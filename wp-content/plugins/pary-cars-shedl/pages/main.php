<?php

if (
    !class_exists( 'woocommerce' )
) {
    echo '
    <div class="tab_container">Enable Woo!</div>
    ';
    return false;
}
?>

<h1>Import Cars XML:</h1>Developed by <a href="https://www.wbd.co.il">WBD.co.il</a>
<div class="tab_container">

    <input id="tab1" type="radio" name="tabs" checked>
    <label for="tab1"><i class="fa fa-server"></i><span>Setup</span></label>

    <input id="tab2" type="radio" name="tabs">
    <label for="tab2"><i class="fa fa-engine"></i><span>API attributes</span></label>

    <input id="tab3" type="radio" name="tabs">
    <label for="tab3"><i class="fa fa-bar-chart-o"></i><span>Queue</span></label>

    <section id="content1" class="tab-content">
      <?php c_render('tabs/tab_1_initial.php'); ?>
    </section>

    <section id="content2" class="tab-content">
        <?php c_render('tabs/tab_2.php'); ?>
    </section>

    <section id="content3" class="tab-content">
        <?php c_render('tabs/tab_3.php'); ?>
    </section>
</div>