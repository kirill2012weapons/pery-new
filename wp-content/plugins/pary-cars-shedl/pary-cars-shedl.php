<?php
/**
 * Plugin Name: Pery Cars Schedule
 * Description: Schedule for API
 * Version: 1.0
 * Author: Alooner
 * Author URI: kirill2012weapons@gmail.com
 **/

/**
 * Defining vars
 */
define('__CARS_DIR__',           __DIR__);
define('__LOGS_DIR__',           __DIR__ . '/logs');
define('__CARS_PAGES_DIR__',     __DIR__ . '/pages');
define('__CARS_VERSION__',       1.0);

define('__CARS_CSS_DIR__',       __DIR__ . '/assets/css');
define('__CARS_JS_DIR__',        __DIR__ . '/assets/js');
define('__CARS_IMF_DIR__',       __DIR__ . '/assets/images');
define('__CARS_CSS_DIR_URL__',   plugin_dir_url(__DIR__) . 'pary-cars-shedl/assets/css');
define('__CARS_JS_DIR_URL__',    plugin_dir_url(__DIR__) . 'pary-cars-shedl/assets/js');
define('__CARS_IMF_DIR_URL__',   plugin_dir_url(__DIR__) . 'pary-cars-shedl/assets/images');

include_once(ABSPATH . 'wp-includes/pluggable.php');
include_once(ABSPATH . 'wp-content/plugins/woocommerce/woocommerce.php');

/**
 * USES
 */
use CarsShedl\Services\Init\CustomOptionPage;
use CarsShedl\Services\Enqueue\EnqCss;
use CarsShedl\Services\Helper\Helpers;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Composer\Autoload\ClassLoader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use CarsShedl\Services\Init\InitDBs;
use CarsShedl\Entity\Options;
use CarsShedl\Services\DataProcessor\CarProcessor;
use CarsShedl\Services\DataProcessor\DataSaverCar;
use CarsShedl\Services\Helper\APIHelper;
use CarsShedl\Services\Queue\QueueDB;
use CarsShedl\Services\DataSingleProcessor\CarSaverSingleWoo;

/**
 * Autoload as PSR-4
 */
/** @var ClassLoader $loader */
$loader = require __DIR__.'/vendor/autoload.php';

include __DIR__ . '/src/parts/cars_cron_schedules.php';
include __DIR__ . '/src/parts/woo_custom_fields.php';
include __DIR__ . '/src/parts/logger.php';

/**
 * Enable action for schedules
 */
add_action('fetching_data_from_api', 'fetchingDataFromApi');
function fetchingDataFromApi() {
    $optionsSetup = new Options();

    if ($optionsSetup->isEnableApiConn() == 'true') {
        $xml          = APIHelper::getRequestXml($optionsSetup->getUrl());
        $dataSaverCar = new CarProcessor( (new DataSaverCar()) );
        $r            = $dataSaverCar->process($xml);
        if ($r) {
            c_log_success()->info('Data From API Fetched.');
        } else {
            c_log_failer()->error('Cannot fetched data from API.');
        }
    }
}

add_action('load_data_from_queue', 'loadDataFromQueue');
function loadDataFromQueue() {

    $optionsSetup = new Options();

    if ($optionsSetup->isEnableQueue() == 'true') {
        $dataQueue = QueueDB::fetch($optionsSetup->getLoadPerFetching());
        foreach ($dataQueue as $entity) {
            $arrData  = \GuzzleHttp\json_decode($entity->car, true);
            $result   = CarSaverSingleWoo::save($entity);

            if ($result) c_log_success()->info(
                'Product created\updated - Product LINK -' .
                '<a href="' . (new WC_Product_Simple($result))->get_permalink() . '">' . (new WC_Product_Simple($result))->get_title() . '</a>' .
                ' (ADMIN LINK - ' . '<a style="color:red;" href="/wp-admin/post.php?post=' . $result . '&action=edit">' . (new WC_Product_Simple($result))->get_title() . '</a>' . ')'
            );
            else c_log_failer()->error('Product failed. - ' . $arrData['CarNumber'] . ' - RESULT - ' . json_encode($result));

            QueueDB::deleteQueueItem($entity->id);
        }
    }
}
//loadDataFromQueue();

/**
 * Init activation hook
 */
function cars_install() {
    if (!get_option('pr_cars_url'))               add_option('pr_cars_url', '');
    if (!get_option('pr_cars_cronInterval'))      add_option('pr_cars_cronInterval', '');
    if (!get_option('pr_cars_cronIntervalQueue')) add_option('pr_cars_cronIntervalQueue', '');
    if (!get_option('pr_cars_loadPerFetching'))   add_option('pr_cars_loadPerFetching', '');
    if (!get_option('pr_cars_enable_api_conn'))   add_option('pr_cars_enable_api_conn', '');
    if (!get_option('pr_cars_enable_queue'))      add_option('pr_cars_enable_queue', '');

    update_option( 'pr_cars_enable_api_conn',   'false' );
    update_option( 'pr_cars_enable_queue',      'false' );

    InitDBs::init();
}
register_activation_hook( __FILE__, 'cars_install' );
/**
 * Init deactivation hook
 */
function cars_remove() {
    if (!get_option('pr_cars_enable_api_conn')) add_option('pr_cars_enable_api_conn', '');
    if (!get_option('pr_cars_enable_queue'))    add_option('pr_cars_enable_queue', '');

    update_option( 'pr_cars_enable_api_conn',   'false' );
    update_option( 'pr_cars_enable_queue',      'false' );

    wp_clear_scheduled_hook('fetching_data_from_api');
    wp_clear_scheduled_hook('load_data_from_queue');
}
register_deactivation_hook( __FILE__, 'cars_remove' );

if (Helpers::isPluginPage()) {
    /**
     * Enable annotation reader
     */
    AnnotationRegistry::registerLoader([$loader, 'loadClass']);

    /* Init request engine. */
    global $request_c;
    $request_c = Request::createFromGlobals();
    /** @return Request */
    function c_request() {
        /** @var $request_c Request */
        global $request_c;
        return $request_c;
    }

    /* Init templating engine. */
    global $templating_c;
    $filesystemLoader = new FilesystemLoader(__DIR__.'/views/%name%');
    $templating_c     = new PhpEngine(new TemplateNameParser(), $filesystemLoader);
    /** @return void */
    function c_render($name, $vars = []) {
        /** @var $templating_c PhpEngine */
        global $templating_c;
        echo $templating_c->render($name, $vars);
    }

    /* Init form factory. */
    /* Init validator engine. */
    $validator_c = Validation::createValidatorBuilder()
        ->enableAnnotationMapping()
        ->getValidator();
    global $formFactory_c;
    $formFactory_c = Forms::createFormFactoryBuilder()
        ->addExtension(new HttpFoundationExtension())
        ->addExtension(new ValidatorExtension($validator_c))
        ->getFormFactory();
    function c_form() {
        /** @var $formFactory_c \Symfony\Component\Form\FormFactory */
        global $formFactory_c;
        return $formFactory_c;
    }

    /* Enq all scripts/styles */
    EnqCss::init();
}

/* Init Option Page (Main page) */
CustomOptionPage::init();
