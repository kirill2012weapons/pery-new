<?php

namespace CarsShedl\CollectionsAttributes;

use CarsShedl\CollectionsAttributes\Interfaces\AttrCollection;

class ColorCollection implements AttrCollection
{
    private $collection = [
      32      => 'לבן',
      23      => 'כחול',
      44      => 'שחור',
      4      => 'אפור מטלי',
      30      => 'כסף מטלי',
      34      => 'לבן פנינה',
      1      => 'אדום',
      42      => 'תכלת',
      19      => 'ירוק',
      15      => 'חום',
      18      => 'תורכיז',
      39      => 'צהוב',
      31      => 'נחושת מטלי',
      14      => 'זהב פנינה',
      46      => 'קרם מטלי',
      5      => 'אפור גג שחור',
      8      => 'בז\'',
      12      => 'ורוד',
      22      => 'ירוק מטלי',
      27      => 'כחול כהה',
      29      => 'כסף',
      24      => 'כחול מטלי',
      41      => 'שמפניה',
      3      => 'אפור',
      2      => 'אדום מטלי',
      50      => 'טוניק',
      38      => 'סהרה',
      6      => 'בורדו',
    ];

    public function getAttrById($id)
    {
        if (isset($this->collection[$id])) return $this->collection[$id];
        return false;
    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function getAttrNameById($id)
    {
        if (isset($this->collection[$id])) return $this->collection[$id];
        return false;
    }

    public function getCollectionName()
    {
        return 'Color';
    }
}