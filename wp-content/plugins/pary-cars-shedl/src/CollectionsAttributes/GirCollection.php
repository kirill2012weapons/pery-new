<?php

namespace CarsShedl\CollectionsAttributes;

use CarsShedl\CollectionsAttributes\Interfaces\AttrCollection;

class GirCollection implements AttrCollection
{
    private $collection = [
        1      => 'אוטומטי',
        2      => 'חברה',
        4      => 'השכרה',
        5      => 'ליסינג',
        9001   => 'מונית',
        9002   => 'נהיגה לימוד',
        9003   => 'ייבוא אישי',
        999999 => 'ממשלתי',
    ];

    public function getAttrById($id)
    {
        if (isset($this->collection[$id])) return $this->collection[$id];
        return false;
    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function getAttrNameById($id)
    {
        if (isset($this->collection[$id])) return $this->collection[$id];
        return false;
    }

    public function getCollectionName()
    {
        return 'Gir';
    }
}