<?php

namespace CarsShedl\CollectionsAttributes;

use CarsShedl\CollectionsAttributes\Interfaces\AttrCollection;

class HandCollection implements AttrCollection
{

    private $collection = [
        1 => 'ראשון',
        2 => 'שנייה',
        3 => 'שלישית',
        4 => 'קדימה',
        5 => 'חמישי',
        6 => 'שישי',
        7 => 'שביעית',
        8 => 'שמונה',
        9 => 'תשיעי',
    ];

    public function getAttrById($id)
    {
        if (isset($this->collection[$id])) return $this->collection[$id];
        return false;
    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function getAttrNameById($id)
    {
        if (isset($this->collection[$id])) return $this->collection[$id];
        return false;
    }

    public function getCollectionName()
    {
        return 'Hand';
    }
}