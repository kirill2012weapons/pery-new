<?php

namespace CarsShedl\CollectionsAttributes\Interfaces;

interface AttrCollection
{
    public function getCollection();

    public function getAttrById($id);

    public function getAttrNameById($id);

    public function getCollectionName();
}