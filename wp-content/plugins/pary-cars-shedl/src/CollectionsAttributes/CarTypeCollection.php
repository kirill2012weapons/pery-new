<?php

namespace CarsShedl\CollectionsAttributes;

use CarsShedl\CollectionsAttributes\Interfaces\AttrCollection;

class CarTypeCollection implements AttrCollection
{

    private $collection = [
        1 => 'פרטית',
        2 => 'חברה',
        3 => 'השכרה',
        5 => 'ליסינג',
        6 => 'מונית',
        8 => 'נהיגה לימוד',
        9 => 'ייבוא אישי',
        10 => 'ממשלתי',
        11 => 'או"ם',
        4 => 'אחר',
    ];

    public function getAttrById($id)
    {
        if (isset($this->collection[$id])) return $this->collection[$id];
        return false;
    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function getAttrNameById($id)
    {
        if (isset($this->collection[$id])) return $this->collection[$id];
        return false;
    }

    public function getCollectionName()
    {
        return 'Car Type';
    }
}