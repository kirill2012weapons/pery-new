<?php

namespace CarsShedl\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use CarsShedl\Validator\Constraints as AcmeAssert;

/**
 * Options
 *
 * Class Options
 * @package CarsShedl\Entity
 */
class Options
{
    /**
     * Api Url.
     *
     * @var string
     * @Assert\Url(message="Set avaliable URL for ur API.")
     * @AcmeAssert\IsSiteAPIAvailable
     */
    private $url;

    /**
     * Cron interval in minutes.
     *
     * @var int
     * @Assert\Regex(pattern="/^[0-9]+$/", message="Must be number.")
     */
    private $cronInterval;

    /**
     * Cron interval in minutes.
     *
     * @var int
     * @Assert\Regex(pattern="/^[0-9]+$/", message="Must be number.")
     */
    private $cronIntervalQueue;

    /**
     * How many items check per one request by cron.
     *
     * @var int
     * @Assert\Regex(pattern="/^[0-9]+$/", message="Must be number.")
     */
    private $loadPerFetching;

    /**
     * Connect to API
     *
     * @var bool
     * @Assert\Regex(pattern="/^(true)|(false)$/", message="True or false.")
     */
    private $enableApiConn;

    /**
     * Do Queue
     *
     * @var bool
     * @Assert\Regex(pattern="/^(true)|(false)$/", message="True or false.")
     */
    private $enableQueue;

    public function __construct()
    {
        $this->url               = get_option('pr_cars_url');
        $this->cronInterval      = get_option('pr_cars_cronInterval');
        $this->cronIntervalQueue = get_option('pr_cars_cronIntervalQueue');
        $this->loadPerFetching   = get_option('pr_cars_loadPerFetching');
        $this->enableApiConn     = get_option('pr_cars_enable_api_conn');
        $this->enableQueue       = get_option('pr_cars_enable_queue');
    }

    /**
     * @return int
     */
    public function getCronIntervalQueue()
    {
        return $this->cronIntervalQueue;
    }

    /**
     * @param int $cronIntervalQueue
     */
    public function setCronIntervalQueue($cronIntervalQueue)
    {
        $this->cronIntervalQueue = $cronIntervalQueue;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getCronInterval()
    {
        return $this->cronInterval;
    }

    /**
     * @param int $cronInterval
     */
    public function setCronInterval($cronInterval)
    {
        $this->cronInterval = $cronInterval;
    }

    /**
     * @return int
     */
    public function getLoadPerFetching()
    {
        return $this->loadPerFetching;
    }

    /**
     * @param int $loadPerFetching
     */
    public function setLoadPerFetching($loadPerFetching)
    {
        $this->loadPerFetching = $loadPerFetching;
    }

    /**
     * @return bool
     */
    public function isEnableApiConn()
    {
        return $this->enableApiConn;
    }

    /**
     * @param bool $enableApiConn
     */
    public function setEnableApiConn($enableApiConn)
    {
        $this->enableApiConn = $enableApiConn;
    }

    /**
     * @return bool
     */
    public function isEnableQueue()
    {
        return $this->enableQueue;
    }

    /**
     * @param bool $enableQueue
     */
    public function setEnableQueue($enableQueue)
    {
        $this->enableQueue = $enableQueue;
    }

    public function save()
    {
        update_option( 'pr_cars_url',               $this->url );
        update_option( 'pr_cars_cronInterval',      $this->cronInterval );
        update_option( 'pr_cars_cronIntervalQueue', $this->cronIntervalQueue );
        update_option( 'pr_cars_loadPerFetching',   $this->loadPerFetching );
        update_option( 'pr_cars_enable_api_conn',   $this->enableApiConn );
        update_option( 'pr_cars_enable_queue',      $this->enableQueue );
    }

}