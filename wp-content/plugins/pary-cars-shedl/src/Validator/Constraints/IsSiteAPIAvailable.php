<?php

namespace CarsShedl\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Check if API URL Available
 *
 * Class IsSiteAPIAvailable
 * @package CarsShedl\Validator\Constraints
 * @Annotation
 */
class IsSiteAPIAvailable extends Constraint
{
    public $message = 'The URL "{{ url }}" not available.';

    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }
}