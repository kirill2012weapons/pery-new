<?php

namespace CarsShedl\Validator\Constraints;

use CarsShedl\Services\Helper\APIHelper;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Validator for check API url
 *
 * Class IsSiteAPIAvailableValidator
 * @package CarsShedl\Validator\Constraints
 */
class IsSiteAPIAvailableValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof IsSiteAPIAvailable) {
            throw new UnexpectedTypeException($constraint, IsSiteAPIAvailable::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        $code = APIHelper::getRequestHeaderCode($value);

        if ($code !== 200) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ url }}', $value)
                ->addViolation();
        }
    }
}