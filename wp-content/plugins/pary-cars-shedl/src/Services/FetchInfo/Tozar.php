<?php

namespace CarsShedl\Services\FetchInfo;

use CarsShedl\Services\Helper\APIHelper;

class Tozar
{
    /**
     * @param $codeDegem CODE-DEGEM
     * @return false|string false - If title not found
     *                      string - If title is found
     */
    static public function getTitle($codeDegem, $mYear)
    {
        $newCodeDegem = str_replace('-', '+', $codeDegem);
        $r            = APIHelper::getRequestToApiInfo($newCodeDegem);

        if (empty($r)) return false;

        $rMass = \GuzzleHttp\json_decode($r, true);

        if (isset($rMass['success']) && $rMass['success'] != true) {
            return false;
        }
        if (!isset($rMass['result']['records'])) {
            return false;
        }
        if (empty($rMass['result']['records'])) {
            return false;
        }

        foreach ($rMass['result']['records'] as $record) {
            if ($record['shnat_yitzur'] == $mYear) {
                $title = $record['tozar'] . ' ' . $record['kinuy_mishari'];
                return $title;
            }
        }
        return false;
    }

    /**
     * @param $carCode CODE NUMBER
     * @return false|string false - If title not found
     *                      string - If title is found
     */
    static public function getTitleFist($carCode)
    {
        $newCodeDegem = str_replace('-', '+', $carCode);

        $r = APIHelper::getRequestToApiInfoTitleFirst($newCodeDegem);

        $rMass = \GuzzleHttp\json_decode($r, true);

        if (isset($rMass['success']) && $rMass['success'] != true) {
            return false;
        }
        if (!isset($rMass['result']['records'])) {
            return false;
        }
        if (empty($rMass['result']['records'])) {
            return false;
        }

        foreach ($rMass['result']['records'] as $record) {
            if (!empty($record['tozeret_nm'])) {
                return $record['tozeret_nm'];
            }
        }
    }
}