<?php

namespace CarsShedl\Services\Queue;

class QueueDB
{
    public static function fetch($count, $order = 'ASC')
    {
        global $wpdb;

        return $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . 'cars_q ORDER BY id ' . $order . ' LIMIT ' .$count );
    }

    public static function fetchAll($order = 'ASC')
    {
        global $wpdb;

        return $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . 'cars_q ORDER BY id ' . $order . ' LIMIT 0,1000');
    }

    public static function deleteQueueItem($id)
    {
        global $wpdb;

        $wpdb->delete( $wpdb->prefix . 'cars_q', ['id' => $id] );
    }

    public static function clearQueue()
    {
        global $wpdb;

        $delete = $wpdb->query('TRUNCATE TABLE `' . $wpdb->prefix . 'cars_q`');
    }
}