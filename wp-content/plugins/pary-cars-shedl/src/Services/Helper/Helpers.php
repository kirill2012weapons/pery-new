<?php

namespace CarsShedl\Services\Helper;

/**
 * Function for helping
 *
 * Class Helpers
 * @package CarsShedl\Services\Helper
 */
class Helpers
{
    /**
     * Return true  - if we are on the plugin option page
     *        false - if not.
     *
     * @return bool
     */
    public static function isPluginPage()
    {
        if (!isset($_REQUEST['page'])) return false;

        if (strpos($_REQUEST['page'], 'cars-shedl/pages')) return true;
        return false;
    }
}