<?php

namespace CarsShedl\Services\Helper;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

/**
 * Helper with guzzle
 *
 * Class APIHelper
 * @package CarsShedl\Services\Helper
 */
class APIHelper
{
    /**
     * Get code response from api url
     *
     * @param $url
     * @return integer
     */
    public static function getRequestHeaderCode($url)
    {
        try {
            $client   = new \GuzzleHttp\Client(['base_uri' => $url]);
            $response = $client->get($url);

            return $response->getStatusCode();
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            return 404;
        } catch (RequestException $exception) {
            return 404;
        }
    }

    /**
     * Get code response from api url
     *
     * @param $url
     * @return string
     */
    public static function getRequestXml($url)
    {
        $client   = new \GuzzleHttp\Client(['base_uri' => $url]);
        $response = $client->get($url);

        try {
            $r = $response->getBody()->getContents();

            return $r;
        } catch (\RuntimeException $exception) {
            c_log_failer()->error('EXCEPTION - ' . $exception->getMessage());

            return '';
        }
    }

    public static function getRequestToApiInfo($code)
    {
        $client   = new Client();
        try {
            $response = $client->request(
                'GET',
                'https://data.gov.il/api/action/datastore_search?resource_id=142afde2-6228-49f9-8a29-9b6c3a0cbe40&q='.$code,
                [
                    'headers'        => [
                        'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                        'accept-encoding' => 'gzip, deflate, br',
                        'accept-language' => 'uk-UA,uk;q=0.9,ru;q=0.8,en-US;q=0.7,en;q=0.6',
                        'cache-control' => 'max-age=0',
                        'sec-fetch-dest' => 'document',
                        'sec-fetch-mode' => 'navigate',
                        'sec-fetch-site' => 'none',
                        'sec-fetch-user' => '?1',
                        'upgrade-insecure-requests' => '1',
                        'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
                    ],
                ]
            );
        } catch (GuzzleException $e) {
            return '';
        }
        try {
            $r = $response->getBody()->getContents();

            return $r;
        } catch (\RuntimeException $exception) {
            c_log_failer()->error('EXCEPTION - Cannot fetch TOZAR from API - ' . $exception->getMessage());

            return '';
        }
    }

    public static function getRequestToApiInfoTitleFirst($caNumber)
    {
        $client   = new Client();
        try {
            $response = $client->request(
                'GET',
                'https://data.gov.il/api/3/action/datastore_search?resource_id=053cea08-09bc-40ec-8f7a-156f0677aff3&q='.$caNumber,
                [
                    'headers'        => [
                        'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                        'accept-encoding' => 'gzip, deflate, br',
                        'accept-language' => 'uk-UA,uk;q=0.9,ru;q=0.8,en-US;q=0.7,en;q=0.6',
                        'cache-control' => 'max-age=0',
                        'sec-fetch-dest' => 'document',
                        'sec-fetch-mode' => 'navigate',
                        'sec-fetch-site' => 'none',
                        'sec-fetch-user' => '?1',
                        'upgrade-insecure-requests' => '1',
                        'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
                    ],
                ]
            );
        } catch (GuzzleException $e) {
            return '';
        }
        try {
            $r = $response->getBody()->getContents();

            return $r;
        } catch (\RuntimeException $exception) {
            return '';
        }
    }
}