<?php

namespace CarsShedl\Services\Helper;

use XMLParser\XMLParser as Parser;

class XMLParser
{
    static public function getArrayFromXml($xml)
    {
        return Parser::objectToArray(Parser::decode($xml));
    }
}