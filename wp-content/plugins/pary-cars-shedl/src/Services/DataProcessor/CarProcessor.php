<?php

namespace CarsShedl\Services\DataProcessor;

use CarsShedl\Services\DataProcessor\Interfaces\CarProcessorInterface;
use CarsShedl\Services\DataProcessor\Interfaces\DataSaverInterface;
use CarsShedl\Services\Helper\XMLParser;

class CarProcessor implements CarProcessorInterface
{
    private $saver;

    public function __construct(DataSaverInterface $saver)
    {
        $this->saver = $saver;
    }

    public function process($xml)
    {
        $xmlArray = XMLParser::getArrayFromXml($xml);

        if (!isset($xmlArray['CARS']))        return false;
        if (!isset($xmlArray['CARS']['CAR'])) return false;
        if (empty($xmlArray['CARS']['CAR']))  return false;

        foreach ($xmlArray['CARS']['CAR'] as $carKey => $carValue) {
            $this->saver->save($carValue);
        }
        return true;
    }
}