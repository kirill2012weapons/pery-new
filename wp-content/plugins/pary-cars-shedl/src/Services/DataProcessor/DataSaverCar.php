<?php

namespace CarsShedl\Services\DataProcessor;

use CarsShedl\Services\DataProcessor\Interfaces\DataSaverInterface;

/**
 * DB Interaction
 *
 * Class DataSaverCar
 * @package CarsShedl\DataProcessor
 */
class DataSaverCar implements DataSaverInterface
{
    private $wpdb;

    /**
     * DataSaverCar constructor.
     */
    public function __construct()
    {
        global $wpdb;

        $this->wpdb = $wpdb;
    }

    public function save($carArray)
    {
        if (is_array($carArray)) {
            $r = $this->wpdb->insert($this->wpdb->get_blog_prefix() . 'cars_q',
              ['car' => \GuzzleHttp\json_encode($carArray)]
            );

            if ($r) return true;

            return false;
        }

        return false;
    }
}