<?php

namespace CarsShedl\Services\DataProcessor\Interfaces;

interface DataSaverInterface
{
    public function save($carArray);
}