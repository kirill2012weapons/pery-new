<?php

namespace CarsShedl\Services\DataProcessor\Interfaces;

interface CarProcessorInterface
{
    public function process($xml);
}