<?php

namespace CarsShedl\Services\Enqueue;

use CarsShedl\Services\Helper\Helpers;

/**
 * Enq Style to admin page
 *
 * Class EnqCss
 * @package CarsShedl\Services\Enqueue
 */
class EnqCss
{
    /**
     * Static Initialisation
     */
    public static function init()
    {
        return new self();
    }

    /**
     * EnqCss constructor.
     */
    public function __construct()
    {
        add_action( 'admin_enqueue_scripts', [$this, 'enqStyle'] );
    }

    /**
     * Enq Styles
     */
    public function enqStyle()
    {
        if (Helpers::isPluginPage())
            wp_enqueue_style( 'cars_admin_tabs_css',    __CARS_CSS_DIR_URL__ . '/tabs.css',    false, '1.0.0' );
            wp_enqueue_style( 'cars_admin_form_ui_css', __CARS_CSS_DIR_URL__ . '/form_ui.css', false, '1.0.0' );
    }
}