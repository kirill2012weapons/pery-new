<?php

namespace CarsShedl\Services\AttachmentSaver;

/**
 * Save image by url
 *
 * Class Saver
 * @package CarsShedl\Services\AttachmentSaver
 */
class Saver
{
    public static function saveThumbnailImage($url, $postID)
    {
        if (!preg_match('/([A-Za-z0-9\_\-]*)\.([a-z]*)$/', $url, $matches)) return false;

        $currentThubnail = get_the_post_thumbnail_url($url);

        if (!$currentThubnail) {
            $attachmentId = self::save($url);
            update_post_meta( $postID, '_thumbnail_id', $attachmentId );
            return true;
        } else {
            $existContent      = file_get_contents( $url );
            $attachmentContent = wp_get_attachment_thumb_file( $postID );
            file_put_contents( $attachmentContent, $existContent );
            return true;
        }
    }

    public static function setDefaultThumbnailImage($postID) {
        update_post_meta( $postID, '_thumbnail_id', 390 );
    }

    public static function isError($ur)
    {
        if (!preg_match('/([A-Za-z0-9\_\-]*)\.([a-z]*)$/', $ur, $matches)) return true;

        $image_data = file_get_contents( 'https://pery.co.il/xml/' . $matches[0] );

        if (!$image_data) return true;

        return false;
    }

    public static function save($ur)
    {
        if (!preg_match('/([A-Za-z0-9\_\-]*)\.([a-z]*)$/', $ur, $matches)) return false;
        global $wp_rewrite;

        $wp_rewrite = new \WP_Rewrite();
        $upload_dir = ABSPATH . 'wp-content/uploads';
        $image_data = file_get_contents( 'https://pery.co.il/xml/' . $matches[0] );
        $filename   = 'xml-' . date('YmnHsi') . basename( $matches[0] );

        $file = $upload_dir . '/' . $filename;

        file_put_contents( $file, $image_data );

        $wp_filetype = wp_check_filetype( $filename, null );

        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name( $filename ),
            'post_content' => '',
            'post_status' => 'inherit',
        );

        $attach_id = wp_insert_attachment( $attachment, $file );

        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        wp_update_attachment_metadata( $attach_id, $attach_data );

        return $attach_id;
    }
}