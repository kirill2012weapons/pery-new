<?php

namespace CarsShedl\Services\Init;

class InitDBs
{
    public static function init()
    {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'cars_q';

        $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );
        if ( is_null($wpdb->get_var( $query )) ) {
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $sql = "
                CREATE TABLE $table_name (
                 id INTEGER NOT NULL AUTO_INCREMENT,
                 car TEXT NOT NULL,
                 PRIMARY KEY (id)) $charset_collate;
             ";
            $result = dbDelta( $sql );
        }
    }
}