<?php

namespace CarsShedl\Services\Init;

/**
 * Init Custom page (option)
 *
 * Class CustomOptionPage
 * @package CarsShedl\Services\Init
 */
class CustomOptionPage
{
    /**
     * Static Init
     *
     * @return CustomOptionPage
     */
    public static function init()
    {
        return new self();
    }

    /**
     * CustomOptionPage constructor.
     *          Init all hooks
     */
    public function __construct()
    {
        add_action( 'admin_menu', [$this, 'registerCustomPage'] );
    }

    /**
     * Register method for custom menu page
     *
     * @return void
     */
    public function registerCustomPage()
    {
        add_menu_page(
          'Cars API',
          'Cars API',
          'manage_options',
          __CARS_PAGES_DIR__ . '/main.php',
          '',
          'dashicons-performance',
          90
        );
    }

}