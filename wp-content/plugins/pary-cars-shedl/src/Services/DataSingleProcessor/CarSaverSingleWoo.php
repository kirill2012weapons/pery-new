<?php

namespace CarsShedl\Services\DataSingleProcessor;

use CarsShedl\CollectionsAttributes\CarTypeCollection;
use CarsShedl\CollectionsAttributes\ColorCollection;
use CarsShedl\CollectionsAttributes\GirCollection;
use CarsShedl\CollectionsAttributes\HandCollection;
use CarsShedl\Services\AttachmentSaver\Saver;
use CarsShedl\Services\DataSingleProcessor\Interfaces\SaverSingleInterface;

class CarSaverSingleWoo implements SaverSingleInterface
{
    public static function save($entity)
    {
        $id       = $entity->id;
        $jsonData = $entity->car;
        $arrData  = \GuzzleHttp\json_decode($jsonData, true);

        if (!isset($arrData['CarNumber'])) return false;

        if (wc_get_product_id_by_sku($arrData['CarNumber'])) {
            return self::update($entity);
        }

        $titleFirst = \CarsShedl\Services\FetchInfo\Tozar::getTitleFist($arrData['CarNumber']);

        if (empty($titleFirst)) {
            $title = $arrData['CarNumber'];
            if (isset($arrData['ManufactureYear'])) {
                $title = \CarsShedl\Services\FetchInfo\Tozar::getTitle($arrData['CodeDegem'] , $arrData['ManufactureYear']);
            }
        } else {
            $title = $titleFirst;
        }
        if (!$title) $title = $arrData['CarNumber'];

        $product = [
            'post_content' => '',
            'post_status' => 'publish',
            'post_title' => $title,
            'post_parent' => '',
            'post_type' => "product",
        ];

        $post_id = wp_insert_post( $product, true );

        if ($post_id instanceof \WP_Error) {
            return false;
        }

        wp_set_object_terms($post_id, 'simple', 'product_type');

        update_post_meta( $post_id, '_visibility', 'visible' );
        update_post_meta( $post_id, 'total_sales', '0');
        update_post_meta( $post_id, '_downloadable', 'no');
        update_post_meta( $post_id, '_virtual', 'yes');
        if (isset($arrData['CarPrice'])) {
            update_post_meta( $post_id, '_regular_price', $arrData['CarPrice'] );
            update_post_meta( $post_id, '_sale_price', $arrData['CarPrice'] );
            update_post_meta( $post_id, '_price', $arrData['CarPrice'] );
            update_post_meta( $post_id, '_stock_status', 'instock');
        } else {
            update_post_meta( $post_id, '_regular_price', "1" );
            update_post_meta( $post_id, '_sale_price', "1" );
            update_post_meta( $post_id, '_price', "1" );
            update_post_meta( $post_id, '_stock_status', 'outofstock');
        }
        update_post_meta( $post_id, '_purchase_note', "" );
        update_post_meta( $post_id, '_featured', "no" );
        update_post_meta( $post_id, '_weight', "" );
        update_post_meta( $post_id, '_length', "" );
        update_post_meta( $post_id, '_width', "" );
        update_post_meta( $post_id, '_height', "" );
        update_post_meta( $post_id, '_sale_price_dates_from', "" );
        update_post_meta( $post_id, '_sale_price_dates_to', "" );
        update_post_meta( $post_id, '_sold_individually', "" );
        update_post_meta( $post_id, '_manage_stock', "no" );
        update_post_meta( $post_id, '_backorders', "no" );
        update_post_meta( $post_id, '_stock', "" );
        if (isset($arrData['CarNumber'])) {
            update_post_meta($post_id, '_sku', $arrData['CarNumber']);
        } else {
            update_post_meta($post_id, '_sku', date('YmdHis'));
        }

        if (isset($arrData['ContactDetails'])) {
            if (isset($arrData['ContactDetails']['ContactDetails'])) {
                if (isset($arrData['ContactDetails']['ContactDetails']['ContactName']))
                    update_post_meta($post_id, '_custom_product_contact_name_field', $arrData['ContactDetails']['ContactDetails']['ContactName']);

                if (isset($arrData['ContactDetails']['ContactDetails']['Phone1']))
                    update_post_meta($post_id, '_custom_product_phone1_field', $arrData['ContactDetails']['ContactDetails']['Phone1']);

                if (isset($arrData['ContactDetails']['ContactDetails']['Email']))
                    update_post_meta($post_id, '_custom_product_Email_field', $arrData['ContactDetails']['ContactDetails']['Email']);
            }
        }

        $thisProduct = new \WC_Product_Simple($post_id);

        $description = '';
        if (isset($arrData['Description'])) {
            $description .= $arrData['Description'] . '<br>';
        }
        if (isset($arrData['Info'])) {
            $description .= $arrData['Info'] . '<br>';
        }
        $thisProduct->set_short_description($description);

        /**
         * ATTRIBUTES
         */

        $att_var = [];
        if (isset($arrData['CodeDegem'])) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Code Degem' );
            $attribute->set_options( [$arrData['CodeDegem']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (isset($arrData['Hand'])) {
            $attribute   = new \WC_Product_Attribute();
            $handTypeAttr = new HandCollection();
            if (!empty($handTypeAttr->getAttrById($arrData['Hand']))) {
                $attribute->set_name( 'Hand' );
                $attribute->set_options([
                    $handTypeAttr->getAttrById($arrData['Hand'])
                ]);
                $attribute->set_position( 1 );
                $attribute->set_visible( 1 );
                $att_var[] = $attribute;
            }
        }

        if (isset($arrData['CarType'])) {
            $attribute   = new \WC_Product_Attribute();
            $carTypeAttr = new CarTypeCollection();
            if (!empty($carTypeAttr->getAttrById($arrData['CarType']))) {
                $attribute->set_name( 'Car Type' );
                $attribute->set_options([
                    $carTypeAttr->getAttrById($arrData['CarType'])
                ]);
                $attribute->set_position( 1 );
                $attribute->set_visible( 1 );
                $att_var[] = $attribute;
            }
        }

        if (isset($arrData['ManufactureYear'])) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Manufacture Year' );
            $attribute->set_options( [$arrData['ManufactureYear']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (isset($arrData['Gir'])) {
            $attribute   = new \WC_Product_Attribute();
            $girAttr     = new GirCollection();

            if (!empty($girAttr->getAttrById($arrData['Gir']))) {
                $attribute->set_name( 'Gir' );
                $attribute->set_options([
                    $girAttr->getAttrById($arrData['CarType'])
                ]);
                $attribute->set_position( 1 );
                $attribute->set_visible( 1 );
                $att_var[] = $attribute;
            }
        }

        if (isset($arrData['EngineType'])) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Engine Type' );
            $attribute->set_options( [$arrData['EngineType']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (isset($arrData['EngineCapacity'])) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Engine Capacity' );
            $attribute->set_options( [$arrData['EngineCapacity']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (isset($arrData['KMS'])) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'KMS' );
            $attribute->set_options( [$arrData['KMS']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (isset($arrData['Color'])) {
            $attribute   = new \WC_Product_Attribute();
            $colorAttr   = new ColorCollection();

            if (!empty($colorAttr->getAttrById($arrData['Color']))) {
                $attribute->set_name( 'Color' );
                $attribute->set_options([
                    $colorAttr->getAttrById($arrData['Color'])
                ]);
                $attribute->set_position( 1 );
                $attribute->set_visible( 1 );
                $att_var[] = $attribute;
            }
        }

        if (isset($arrData['Migrash'])) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Migrash' );
            $attribute->set_options( [$arrData['Migrash']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (isset($arrData['Images'])) {
            if (isset($arrData['Images']['CarImage'])) {
                foreach ($arrData['Images']['CarImage'] as $image) {
                    if (Saver::isError($image['@attributes']['Path'])) continue;
                    Saver::saveThumbnailImage($image['@attributes']['Path'], $post_id);
                }
            } else {
                Saver::setDefaultThumbnailImage($post_id);
            }
        }

        if (isset($arrData['Images'])) {
            if (isset($arrData['Images']['CarImage'])) {
                $images = '';
                foreach ($arrData['Images']['CarImage'] as $image) {
                    if (Saver::isError($image['@attributes']['Path'])) continue;
                    $attId = Saver::save($image['@attributes']['Path']);
                    if ($attId) $images .= Saver::save($image['@attributes']['Path']) . ',';
                }
                update_post_meta($post_id, '_product_image_gallery', $images);
            }
        }

        $thisProduct->set_attributes($att_var);
        $thisProduct->set_category_ids([21]);

        $id = $thisProduct->save();

        if ($id) return $id;
        return false;

    }

    public static function update($entity)
    {
        $id       = $entity->id;
        $jsonData = $entity->car;
        $arrData  = \GuzzleHttp\json_decode($jsonData, true);
        $post_id  = wc_get_product_id_by_sku( $arrData['CarNumber'] );
        $_pf      = new \WC_Product_Factory();
        $product  = $_pf->get_product($post_id);

        if (isset($arrData['CarPrice']) && ($arrData['CarPrice'] != $product->get_price()) ) {
            update_post_meta( $post_id, '_regular_price', $arrData['CarPrice'] );
            update_post_meta( $post_id, '_sale_price', $arrData['CarPrice'] );
            update_post_meta( $post_id, '_price', $arrData['CarPrice'] );
            update_post_meta( $post_id, '_stock_status', 'instock');
        }

        $titleFirst = \CarsShedl\Services\FetchInfo\Tozar::getTitleFist($arrData['CarNumber']);

        if (empty($titleFirst)) {
            $title = $arrData['CarNumber'];
            if (isset($arrData['ManufactureYear'])) {
                $title = \CarsShedl\Services\FetchInfo\Tozar::getTitle($arrData['CodeDegem'] , $arrData['ManufactureYear']);
            }
        } else {
            $title = $titleFirst;
        }
        if (!$title) $title = $arrData['CarNumber'];

        if ($title) wp_update_post([
            'ID' => $post_id,
            'post_title' => $title,
        ]);

        if (isset($arrData['ContactDetails'])) {
            if (isset($arrData['ContactDetails']['ContactDetails'])) {
                if (
                    isset($arrData['ContactDetails']['ContactDetails']['ContactName'])
                    && ($arrData['ContactDetails']['ContactDetails']['ContactName'] != get_post_meta($post_id, '_custom_product_contact_name_field', true))
                )
                    update_post_meta($post_id, '_custom_product_contact_name_field', $arrData['ContactDetails']['ContactDetails']['ContactName']);

                if (
                    isset($arrData['ContactDetails']['ContactDetails']['Phone1'])
                    && ($arrData['ContactDetails']['ContactDetails']['Phone1'] != get_post_meta($post_id, '_custom_product_phone1_field', true))
                )
                    update_post_meta($post_id, '_custom_product_phone1_field', $arrData['ContactDetails']['ContactDetails']['Phone1']);

                if (
                    isset($arrData['ContactDetails']['ContactDetails']['Email'])
                    && ($arrData['ContactDetails']['ContactDetails']['Email'] != get_post_meta($post_id, '_custom_product_Email_field', true))
                )
                    update_post_meta($post_id, '_custom_product_Email_field', $arrData['ContactDetails']['ContactDetails']['Email']);
            }
        }

        $description = '';
        if (isset($arrData['Description'])) {
            $description .= $arrData['Description'] . '<br>';
        }
        if (isset($arrData['Info'])) {
            $description .= $arrData['Info'] . '<br>';
        }
        $product->set_short_description($description);

        /**
         * ATTRIBUTES
         */
        $att_var = [];
        if (
        isset($arrData['CodeDegem'])
        ) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Code Degem' );
            $attribute->set_options( [$arrData['CodeDegem']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (isset($arrData['Hand'])) {
            $attribute   = new \WC_Product_Attribute();
            $handTypeAttr = new HandCollection();
            if (!empty($handTypeAttr->getAttrById($arrData['Hand']))) {
                $attribute->set_name( 'Hand' );
                $attribute->set_options([
                    $handTypeAttr->getAttrById($arrData['Hand'])
                ]);
                $attribute->set_position( 1 );
                $attribute->set_visible( 1 );
                $att_var[] = $attribute;
            }
        }

        if (
        isset($arrData['CarType'])
        ) {
            $attribute   = new \WC_Product_Attribute();
            $carTypeAttr = new CarTypeCollection();
            if (!empty($carTypeAttr->getAttrById($arrData['CarType']))) {
                $attribute->set_name( 'Car Type' );
                $attribute->set_options([
                    $carTypeAttr->getAttrById($arrData['CarType'])
                ]);
                $attribute->set_position( 1 );
                $attribute->set_visible( 1 );
                $att_var[] = $attribute;
            }
        }

        if (
        isset($arrData['ManufactureYear'])
        ) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Manufacture Year' );
            $attribute->set_options( [$arrData['ManufactureYear']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (
        isset($arrData['Gir'])
        ) {
            $attribute   = new \WC_Product_Attribute();
            $girAttr     = new GirCollection();

            if (!empty($girAttr->getAttrById($arrData['Gir']))) {
                $attribute->set_name( 'Gir' );
                $attribute->set_options([
                    $girAttr->getAttrById($arrData['Gir'])
                ]);
                $attribute->set_position( 1 );
                $attribute->set_visible( 1 );
                $att_var[] = $attribute;
            }
        }

        if (
        isset($arrData['EngineType'])
        ) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Engine Type' );
            $attribute->set_options( [$arrData['EngineType']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (
        isset($arrData['EngineCapacity'])
        ) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Engine Capacity' );
            $attribute->set_options( [$arrData['EngineCapacity']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (
        isset($arrData['KMS'])
        ) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'KMS' );
            $attribute->set_options( [$arrData['KMS']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        if (
        isset($arrData['Color'])
        ) {
            $attribute   = new \WC_Product_Attribute();
            $colorAttr   = new ColorCollection();

            if (!empty($colorAttr->getAttrById($arrData['Color']))) {
                $attribute->set_name( 'Color' );
                $attribute->set_options([
                    $colorAttr->getAttrById($arrData['Color'])
                ]);
                $attribute->set_position( 1 );
                $attribute->set_visible( 1 );
                $att_var[] = $attribute;
            }
        }

        if (
        isset($arrData['Migrash'])
        ) {
            $attribute = new \WC_Product_Attribute();

            $attribute->set_name( 'Migrash' );
            $attribute->set_options( [$arrData['Migrash']] );
            $attribute->set_position( 1 );
            $attribute->set_visible( 1 );
            $att_var[] = $attribute;
        }

        $product->set_attributes($att_var);

        if (isset($arrData['Images'])) {
            if (isset($arrData['Images']['CarImage'])) {
                $count = 0;
                foreach ($arrData['Images']['CarImage'] as $image) {
                    if (Saver::isError($image['@attributes']['Path'])) {
                        $count++;
                        continue;
                    }
                    Saver::saveThumbnailImage($image['@attributes']['Path'], $post_id);
                }
                if ($count == count($arrData['Images']['CarImage'])) Saver::setDefaultThumbnailImage($post_id);
            } else {
                Saver::setDefaultThumbnailImage($post_id);
            }
        }
        $cats = $product->get_category_ids();
        $cats[] = 21;
        $product->set_category_ids($cats);

        $id = $product->save();

        if ($id) return $id;
        return false;
    }
}