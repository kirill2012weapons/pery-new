<?php

namespace CarsShedl\Services\DataSingleProcessor\Interfaces;

interface SaverSingleInterface
{
    public static function save($entity);
}