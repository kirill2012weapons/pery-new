<?php

namespace CarsShedl\Services\CommandSchedule\Interfaces;

interface CommandInterface
{
    public static function run();
}