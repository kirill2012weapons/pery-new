<?php

namespace CarsShedl\Services\CommandSchedule;

use CarsShedl\Entity\Options;
use CarsShedl\Services\Helper\APIHelper;
use CarsShedl\Services\CommandSchedule\Interfaces\CommandInterface;
use CarsShedl\Services\DataProcessor\CarProcessor;
use CarsShedl\Services\DataProcessor\DataSaverCar;

class XmlFetcherCommand implements CommandInterface
{
    public static function run()
    {
        $optionSetup = new Options();
        $xml = APIHelper::getRequestXml($optionSetup->getUrl());

        $dataSaverCar = new CarProcessor( (new DataSaverCar()) );
        $dataSaverCar->process($xml);
    }
}