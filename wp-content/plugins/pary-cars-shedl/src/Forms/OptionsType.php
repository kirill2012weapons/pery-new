<?php

namespace CarsShedl\Forms;

use CarsShedl\Entity\Options;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OptionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enableApiConn',     ChoiceType::class, [
                'choices' => [
                    'ON'  => 'true',
                    'OFF' => 'false'
                ]
            ])
            ->add('enableQueue',       ChoiceType::class, [
                'choices' => [
                    'ON'  => 'true',
                    'OFF' => 'false'
                ]
            ])
            ->add('url',               TextType::class)
            ->add('cronInterval',      TextType::class)
            ->add('cronIntervalQueue', TextType::class)
            ->add('loadPerFetching',   TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Options::class,
        ]);
    }
}