<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

global $logSuccess;

$logSuccess = new Logger('success');
try {
    $logSuccess->pushHandler(new StreamHandler(__LOGS_DIR__.'/success.log', Logger::INFO));
} catch (Exception $e) {
}
function c_log_success()
{
    global $logSuccess;
    /** @var $logSuccess Logger */
    return $logSuccess;
}

global $logFailer;

$logFailer = new Logger('failed');
try {
    $logFailer->pushHandler(new StreamHandler(__LOGS_DIR__.'/failed.log', Logger::ERROR));
} catch (Exception $e) {
}
function c_log_failer()
{
    global $logFailer;
    /** @var $logFailer Logger */
    return $logFailer;
}


function tailWithSkip($filepath, $color = '#009568', $lines = 1, $skip = 0, $adaptive = true)
{
    // Open file
    $f = @fopen($filepath, "rb");
    if (@flock($f, LOCK_SH) === false) return false;
    if ($f === false) return false;

    if (!$adaptive) $buffer = 4096;
    else {
        $max=max($lines, $skip);
        $buffer = ($max < 2 ? 64 : ($max < 10 ? 512 : 4096));
    }
    fseek($f, -1, SEEK_END);
    if (fread($f, 1) == "\n") {
        if ($skip > 0) { $skip++; $lines--; }
    } else {
        $lines--;
    }
    $output = '';
    $chunk = '';
    while (ftell($f) > 0 && $lines >= 0) {
        $seek = min(ftell($f), $buffer);
        fseek($f, -$seek, SEEK_CUR);
        $chunk = fread($f, $seek);
        $count = substr_count($chunk, "\n");
        $strlen = mb_strlen($chunk, '8bit');
        fseek($f, -$strlen, SEEK_CUR);

        if ($skip > 0) {
            if ($skip > $count) { $skip -= $count; $chunk=''; }
            else {
                $pos = 0;

                while ($skip > 0) {
                    if ($pos > 0) $offset = $pos - $strlen - 1; // Calculate the offset - NEGATIVE position of last new line symbol
                    else $offset=0; // First search (without offset)

                    $pos = strrpos($chunk, "\n", $offset); // Search for last (including offset) new line symbol

                    if ($pos !== false) $skip--; // Found new line symbol - skip the line
                    else break; // "else break;" - Protection against infinite loop (just in case)
                }
                $chunk=substr($chunk, 0, $pos); // Truncated chunk
                $count=substr_count($chunk, "\n"); // Count new line symbols in truncated chunk
            }
        }

        if (strlen($chunk) > 0) {
            $output = $chunk . $output;
            $output = substr($output, strpos($output, "\n") + 1);
            $output = str_replace('[]', '', $output);
            $output = '<div style="color: ' . $color . ';">' . $output . '</div>';
            $lines -= $count;
        }
    }

    while ($lines++ < 0) {
        $output = substr($output, strpos($output, "\n") + 1);
        $output = str_replace('[]', '', $output);
        $output = '<div style="color: ' . $color . ';">' . $output . '</div>';
    }

    @flock($f, LOCK_UN);
    fclose($f);
    return trim($output);
}