<?php
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields');
add_action('woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save');
function woocommerce_product_custom_fields()
{
    global $woocommerce, $post;

    echo '<div class="product_custom_field">';
    woocommerce_wp_text_input(
        array(
            'id' => '_custom_product_contact_name_field',
            'placeholder' => 'Contact Name',
            'label' => __('ContactName', 'woocommerce'),
            'desc_tip' => 'true',
//            'value' => get_post_meta(get_the_ID(), '_custom_product_contact_name_field', true),
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' => '_custom_product_phone1_field',
            'placeholder' => 'Phone',
            'label' => __('Phone', 'woocommerce'),
            'desc_tip' => 'true'
        )
    );
    woocommerce_wp_text_input(
        array(
            'id' => '_custom_product_Email_field',
            'placeholder' => 'Email',
            'label' => __('Email', 'woocommerce'),
            'desc_tip' => 'true'
        )
    );
    echo '</div>';
}

function woocommerce_product_custom_fields_save()
{
    $_custom_product_contact_name_field = $_POST['_custom_product_contact_name_field'];
    update_post_meta( get_the_ID(), '_custom_product_contact_name_field', esc_attr($_custom_product_contact_name_field));

    $_custom_product_phone1_field = $_POST['_custom_product_phone1_field'];
    update_post_meta( get_the_ID(), '_custom_product_phone1_field', esc_attr($_custom_product_phone1_field));

    $_custom_product_Email_field = $_POST['_custom_product_Email_field'];
    update_post_meta( get_the_ID(), '_custom_product_Email_field', esc_attr($_custom_product_Email_field));
}