<?php

function cars_cron_schedules($schedules) {
    $optionsSetup = new \CarsShedl\Entity\Options();
    if(!isset($schedules["cars_cron_schedule_time"])) {
        $schedules["cars_cron_schedule_time"] = array(
          'interval' => $optionsSetup->getCronInterval()*60,
          'display' => __('Cars schedule'));
    } else if ($schedules["cars_cron_schedule_time"]['interval'] != ($optionsSetup->getCronInterval()*60)) {
        $schedules["cars_cron_schedule_time"] = array(
          'interval' => $optionsSetup->getCronInterval()*60,
          'display' => __('Cars schedule'));
    }

    if(!isset($schedules["cars_cron_schedule_time_qu"])) {
        $schedules["cars_cron_schedule_time_qu"] = array(
          'interval' => $optionsSetup->getCronIntervalQueue()*60,
          'display' => __('Cars schedule Queue'));
    } else if ($schedules["cars_cron_schedule_time_qu"]['interval'] != ($optionsSetup->getCronIntervalQueue()*60)) {
        $schedules["cars_cron_schedule_time_qu"] = array(
          'interval' => $optionsSetup->getCronIntervalQueue()*60,
          'display' => __('Cars schedule Queue'));
    }
    return $schedules;
}
add_filter('cron_schedules','cars_cron_schedules');