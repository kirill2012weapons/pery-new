<?php

use CarsShedl\Services\Queue\QueueDB;

if (isset($_POST['delete'])) {
    QueueDB::clearQueue();
}

if (isset($_GET['del_q'])) {
    QueueDB::deleteQueueItem($_GET['del_q']);
}
$queueArray = QueueDB::fetchAll();

?>

<h3>Queue</h3>
<form action="?page=pary-cars-shedl%2Fpages%2Fmain.php" method="post">
    <input type="submit" name="delete" value="DELETE ALL IN QUEUE" style="display: block;">
</form>
<table>
    <tr>
        <th>ID</th>
        <th>Code Degem</th>
        <th>Car Number</th>
        <th>Delete</th>
    </tr>
    <?php foreach ($queueArray as $item) : ?>
        <?php
        $arr = json_decode($item->car, true);
        ?>
        <tr>
            <td><?= $item->id; ?></td>
            <td><?php if (isset($arr['CodeDegem'])) echo $arr['CodeDegem']; ?></td>
            <td><?php if (isset($arr['CarNumber'])) echo $arr['CarNumber']; ?></td>
            <td><a href="<?php echo $_SERVER['REQUEST_URI'] . '&del_q=' . $item->id; ?>">DELETE</a></td>
        </tr>
    <?php endforeach; ?>
</table>