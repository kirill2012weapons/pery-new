<?php

use CarsShedl\CollectionsAttributes\CarTypeCollection;
use CarsShedl\CollectionsAttributes\ColorCollection;
use CarsShedl\CollectionsAttributes\GirCollection;

$carTypeCollection = new CarTypeCollection();
$colorCollection   = new ColorCollection();
$girCollection     = new GirCollection();

?>
<h2>CarType</h2>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>
  <?php foreach ($carTypeCollection->getCollection() as $key => $value) : ?>
    <tr>
        <td><?= $key; ?></td>
        <td><?= $value; ?></td>
    </tr>
  <?php endforeach; ?>
</table>
<br>
<br>
<h2>Colors</h2>
<table>
  <tr>
    <th>ID</th>
    <th>Name</th>
  </tr>
    <?php foreach ($colorCollection->getCollection() as $key => $value) : ?>
      <tr>
        <td><?= $key; ?></td>
        <td><?= $value; ?></td>
      </tr>
    <?php endforeach; ?>
</table>
<br>
<br>
<h2>Gir</h2>
<table>
  <tr>
    <th>ID</th>
    <th>Name</th>
  </tr>
    <?php foreach ($girCollection->getCollection() as $key => $value) : ?>
      <tr>
        <td><?= $key; ?></td>
        <td><?= $value; ?></td>
      </tr>
    <?php endforeach; ?>
</table>
<br>
<br>