<?php

use CarsShedl\Entity\Options;
use CarsShedl\Forms\OptionsType;

$optionSetup = new Options();

$formSetup = c_form()
    ->createBuilder(OptionsType::class, $optionSetup)
    ->getForm()
    ->handleRequest(c_request());

if ($formSetup->isSubmitted() && $formSetup->isValid()) {
    $optionSetup->save();

    if ($optionSetup->isEnableApiConn() == 'true') {
        if (!wp_next_scheduled ( 'fetching_data_from_api' )) {
            wp_schedule_event(time(), 'cars_cron_schedule_time', 'fetching_data_from_api');
        }
    } else if ($optionSetup->isEnableApiConn() == 'false') {
          wp_clear_scheduled_hook('fetching_data_from_api');
    }

    if ($optionSetup->isEnableQueue() == 'true') {
        if (!wp_next_scheduled ( 'load_data_from_queue' )) {
            wp_schedule_event(time(), 'cars_cron_schedule_time_qu', 'load_data_from_queue');
        }
    } else if ($optionSetup->isEnableApiConn() == 'false') {
          wp_clear_scheduled_hook('load_data_from_queue');
    }
}

$formSetupView = $formSetup->createView();

?>

<h3>Setup</h3>
<form action="<?php echo $formSetupView->vars['action'] ?>" method="<?php echo $formSetupView->vars['method'] ?>">

    <div class="ui-input-wrap radio-group">
        <?php $f_s_enableApiConn = $formSetupView->children['enableApiConn']; ?>
        <label style="text-align: left;">Enable Connect to API URL </br> (Load products from API, Enable "Queue" for creating product)</label>
        <div class="radios">
            <?php $count = 1; ?>
            <?php foreach ($f_s_enableApiConn->vars['choices'] as $choice) : ?>
                <div class="radio">
                    <div class="radio-wrap">
                        <input type="radio"
                               name="<?php echo $f_s_enableApiConn->vars['full_name']; ?>"
                               id="<?php echo $f_s_enableApiConn->vars['id'] . '_' . $count; ?>"
                               value="<?php echo $choice->value; ?>"
                               <?php if ($f_s_enableApiConn->vars['value'] == $choice->value) echo 'checked="checked"' ?>
                        >
                        <label for="<?php echo $f_s_enableApiConn->vars['id'] . '_' . $count; ?>"></label>
                    </div>
                    <label for="<?php echo $f_s_enableApiConn->vars['id'] . '_' . $count; ?>"><?php echo $choice->label; ?></label>
                </div>
            <?php $count += 1; ?>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="ui-input-wrap radio-group">
        <?php $f_s_enableQueue = $formSetupView->children['enableQueue']; ?>
        <label style="text-align: left;">Enable queue (Load products from queue)</label>
        <div class="radios">
            <?php $count = 1; ?>
            <?php foreach ($f_s_enableQueue->vars['choices'] as $choice) : ?>
                <div class="radio">
                    <div class="radio-wrap">
                        <input type="radio"
                               name="<?php echo $f_s_enableQueue->vars['full_name']; ?>"
                               id="<?php echo $f_s_enableQueue->vars['id'] . '_' . $count; ?>"
                               value="<?php echo $choice->value; ?>"
                            <?php if ($f_s_enableQueue->vars['value'] == $choice->value) echo 'checked="checked"' ?>
                        >
                        <label for="<?php echo $f_s_enableQueue->vars['id'] . '_' . $count; ?>"></label>
                    </div>
                    <label for="<?php echo $f_s_enableQueue->vars['id'] . '_' . $count; ?>"><?php echo $choice->label; ?></label>
                </div>
                <?php $count += 1; ?>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="ui-input-wrap">
        <?php $f_s_url = $formSetupView->children['url']; ?>
        <label for="<?php echo $f_s_url->vars['id']; ?>">Url API</label>
        <input type="text"
               value="<?php echo $f_s_url->vars['value']; ?>"
               id="<?php echo $f_s_url->vars['id']; ?>"
               name="<?php echo $f_s_url->vars['full_name']; ?>"
               placeholder="https://example.com"
        />
        <?php if ($f_s_url->vars['errors']->count()) : ?>
            <?php foreach ($f_s_url->vars['errors'] as $error) : ?>
                <p style="color: red;"><?php echo $error->getMessage(); ?></p>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <div class="ui-input-wrap">
        <?php $f_s_cronInterval = $formSetupView->children['cronInterval']; ?>
        <label for="<?php echo $f_s_cronInterval->vars['id']; ?>">Interval request to API (minutes)</label>
        <input type="number"
               style="width: 100px;"
               min="1"
               max="100000"
               id="<?php echo $f_s_cronInterval->vars['id']; ?>"
               name="<?php echo $f_s_cronInterval->vars['full_name']; ?>"
               value="<?php echo $f_s_cronInterval->vars['value']; ?>"
               placeholder="3600"
        />
        <?php if ($f_s_cronInterval->vars['errors']->count()) : ?>
            <?php foreach ($f_s_cronInterval->vars['errors'] as $error) : ?>
                <p style="color: red;"><?php echo $error->getMessage(); ?></p>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <div class="ui-input-wrap">
        <?php $f_s_cronIntervalQueue = $formSetupView->children['cronIntervalQueue']; ?>
        <label style="margin-right: 85px;" for="<?php echo $f_s_cronIntervalQueue->vars['id']; ?>">Interval Queue load</label>
        <input type="number"
               style="width: 100px;"
               min="1"
               max="20000"
               id="<?php echo $f_s_cronIntervalQueue->vars['id']; ?>"
               name="<?php echo $f_s_cronIntervalQueue->vars['full_name']; ?>"
               value="<?php echo $f_s_cronIntervalQueue->vars['value']; ?>"
               placeholder="3600"
        />
        <?php if ($f_s_cronIntervalQueue->vars['errors']->count()) : ?>
            <?php foreach ($f_s_cronIntervalQueue->vars['errors'] as $error) : ?>
                <p style="color: red;"><?php echo $error->getMessage(); ?></p>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <div class="ui-input-wrap">
        <?php $f_s_loadPerFetching = $formSetupView->children['loadPerFetching']; ?>
        <label style="margin-right: 58px;" for="<?php echo $f_s_loadPerFetching->vars['id']; ?>">Fetch items from queue</label>
        <input type="number"
               style="width: 100px;"
               min="1"
               max="30"
               id="<?php echo $f_s_loadPerFetching->vars['id']; ?>"
               name="<?php echo $f_s_loadPerFetching->vars['full_name']; ?>"
               value="<?php echo $f_s_loadPerFetching->vars['value']; ?>"
               placeholder="10"
        />
        <?php if ($f_s_loadPerFetching->vars['errors']->count()) : ?>
            <?php foreach ($f_s_loadPerFetching->vars['errors'] as $error) : ?>
                <p style="color: red;"><?php echo $error->getMessage(); ?></p>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <div class="ui-input-wrap">
        <input type="submit" id="submit_tab_1" name="submit_tab_1" class="btn-cust" value="Save"/>
    </div>
</form>