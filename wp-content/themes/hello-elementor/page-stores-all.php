<?php

/**
 * Template Name: Stores-All
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

?>
<?php get_header(); ?>

<style type="text/css">
    .acf-map {
        width: 100%;
        height: 400px;
        border: #ccc solid 1px;
        margin: 20px 0;
    }


       .acf-map img {
           max-width: inherit !important;
       }
        .mb-0{
            margin-bottom: 0 !important;
        }
    @media (max-width: 767px) {
        .cent-mob{
            display: flex;
            justify-content: center;
        }
    }

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY"></script>
<script type="text/javascript">
    (function( $ ) {

        /**
         * initMap
         *
         * Renders a Google Map onto the selected jQuery element
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   jQuery $el The jQuery element.
         * @return  object The map instance.
         */
        function initMap( $el ) {

            // Find marker elements within map.
            var $markers = $el.find('.marker');

            // Create gerenic map.
            var mapArgs = {
                zoom        : $el.data('zoom') || 16,
                mapTypeId   : google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map( $el[0], mapArgs );

            // Add markers.
            map.markers = [];
            $markers.each(function(){
                initMarker( $(this), map );
            });

            // Center map based on markers.
            centerMap( map );

            // Return map instance.
            return map;
        }

        /**
         * initMarker
         *
         * Creates a marker for the given jQuery element and map.
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   jQuery $el The jQuery element.
         * @param   object The map instance.
         * @return  object The marker instance.
         */
        function initMarker( $marker, map ) {

            // Get position from marker.
            var lat = $marker.data('lat');
            var lng = $marker.data('lng');
            var latLng = {
                lat: parseFloat( lat ),
                lng: parseFloat( lng )
            };

            // Create marker instance.
            var marker = new google.maps.Marker({
                position : latLng,
                map: map
            });

            // Append to reference for later use.
            map.markers.push( marker );

            // If marker contains HTML, add it to an infoWindow.
            if( $marker.html() ){

                // Create info window.
                var infowindow = new google.maps.InfoWindow({
                    content: $marker.html()
                });

                // Show info window when marker is clicked.
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open( map, marker );
                });
            }
        }

        /**
         * centerMap
         *
         * Centers the map showing all markers in view.
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   object The map instance.
         * @return  void
         */
        function centerMap( map ) {

            // Create map boundaries from all map markers.
            var bounds = new google.maps.LatLngBounds();
            map.markers.forEach(function( marker ){
                bounds.extend({
                    lat: marker.position.lat(),
                    lng: marker.position.lng()
                });
            });

            // Case: Single marker.
            if( map.markers.length == 1 ){
                map.setCenter( bounds.getCenter() );

                // Case: Multiple markers.
            } else{
                map.fitBounds( bounds );
            }
        }

// Render maps on page load.
        $(document).ready(function(){
            $('.acf-map').each(function(){
                var map = initMap( $(this) );
            });
        });

    })(jQuery);
</script>

<?php

$elements = get_field('regions');
$elementsFilter = $elements;

if (isset($_GET['s_c_order']) && !empty($_GET['s_c_order'])) {

    if ($_GET['s_c_order'] == 'ASC') {
        foreach ($elements as &$area) {
            uasort($area['area'], function ($a, $b){
                return strcasecmp($a['title'],$b['title']);
            });
        }

    } else if ($_GET['s_c_order'] == 'DESC') {

        foreach ($elements as &$area) {
            uasort($area['area'], function ($a, $b){
                return !strcasecmp($a['title'],$b['title']);
            });
        }

    }
}

if (isset($_GET['s_c_area']) && !empty($_GET['s_c_area'])) {
    $elements = array_filter($elements, function ($ell) {
        return $ell['name'] == $_GET['s_c_area'];
    });
}

?>

<script>
    jQuery(document).ready(function () {
        jQuery('#serch-filter-store').find('select').on('change', function (e) {
            e.preventDefault();
            jQuery('#serch-filter-store').off().submit();
        });
    });
</script>

<div data-elementor-type="wp-page" data-elementor-id="36" class="elementor elementor-36" data-elementor-settings="[]">
    <div class="elementor-inner">
        <div class="elementor-section-wrap">
            <section class="elementor-element elementor-element-71bf663 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="71bf663" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-background-overlay"></div>
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div class="elementor-element elementor-element-f16f121 elementor-column elementor-col-100 elementor-top-column" data-id="f16f121" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-c454581 elementor-widget elementor-widget-heading" data-id="c454581" data-element_type="widget" data-widget_type="heading.default">
                                        <div class="elementor-widget-container">
                                            <h2 class="elementor-heading-title elementor-size-default">סניפים</h2>		</div>
                                    </div>
                                    <div class="elementor-element elementor-element-3d93c29 elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="3d93c29" data-element_type="widget" data-widget_type="button.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-button-wrapper cent-mob">
                                                <a href="<?php echo home_url(); ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-angle-left"></i>			</span>
						<span class="elementor-button-text">קליק לנציג</span>
		</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="elementor-element elementor-element-dbee2cf elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="dbee2cf" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div class="elementor-element elementor-element-6cf6d9d elementor-column elementor-col-100 elementor-top-column" data-id="6cf6d9d" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-c2d5f4b elementor-button-align-stretch elementor-widget elementor-widget-form" data-id="c2d5f4b" data-element_type="widget" data-widget_type="form.default">
                                        <div class="elementor-widget-container">
                                            <form id="serch-filter-store" class="elementor-form" method="get" action="?" name="New Form">

                                                <div class="elementor-form-fields-wrapper elementor-labels-">

                                                    <?php if (wp_is_mobile()) : ?>

                                                        <div class="elementor-field-type-select elementor-field-group elementor-column elementor-field-group-email elementor-col-20 elementor-sm-50 elementor-field-required">
                                                            <label for="form-field-email" class="elementor-field-label elementor-screen-only">Email</label>		<div class="elementor-field elementor-select-wrapper ">
                                                                <select name="s_c_order" id="form-field-email" class="elementor-field-textual elementor-size-sm" required="required" aria-required="true">
                                                                    <option value="">סידור לפי</option>
                                                                    <option
                                                                        <?php
                                                                        if (isset($_GET['s_c_order']) && !empty($_GET['s_c_order']) && ($_GET['s_c_order'] == 'ASC')) {
                                                                            echo 'selected ';
                                                                        }
                                                                        ?>
                                                                            value="ASC">A-Z</option>
                                                                    <option
                                                                        <?php
                                                                        if (isset($_GET['s_c_order']) && !empty($_GET['s_c_order']) && ($_GET['s_c_order'] == 'DESC')) {
                                                                            echo 'selected ';
                                                                        }
                                                                        ?>
                                                                            value="DESC">Z-A</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="elementor-field-type-select elementor-field-group elementor-column elementor-field-group-name elementor-col-20 elementor-sm-50">
                                                            <label for="form-field-name" class="elementor-field-label elementor-screen-only">Name</label>		<div class="elementor-field elementor-select-wrapper ">
                                                                <select name="s_c_area" id="form-field-name" class="elementor-field-textual elementor-size-sm">
                                                                    <option value="">בחירת איזור</option>
                                                                    <?php foreach ( $elementsFilter as $area) : ?>
                                                                        <option
                                                                            <?php
                                                                            if (isset($_GET['s_c_area']) && !empty($_GET['s_c_area']) && ($_GET['s_c_area'] == $area['name'])) {
                                                                                echo 'selected ';
                                                                            }
                                                                            ?>
                                                                                value="<?php echo $area['name'] ?>"><?php echo $area['name'] ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>


                                                    <?php else: ?>
                                                        <div class="elementor-field-type-select elementor-field-group elementor-column elementor-field-group-name elementor-col-20 elementor-sm-50">
                                                            <label for="form-field-name" class="elementor-field-label elementor-screen-only">Name</label>		<div class="elementor-field elementor-select-wrapper ">
                                                                <select name="s_c_area" id="form-field-name" class="elementor-field-textual elementor-size-sm">
                                                                    <option value="">בחירת איזור</option>
                                                                    <?php foreach ( $elementsFilter as $area) : ?>
                                                                        <option
                                                                            <?php
                                                                            if (isset($_GET['s_c_area']) && !empty($_GET['s_c_area']) && ($_GET['s_c_area'] == $area['name'])) {
                                                                                echo 'selected ';
                                                                            }
                                                                            ?>
                                                                                value="<?php echo $area['name'] ?>"><?php echo $area['name'] ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="elementor-field-type-select elementor-field-group elementor-column elementor-field-group-email elementor-col-20 elementor-sm-50 elementor-field-required">
                                                            <label for="form-field-email" class="elementor-field-label elementor-screen-only">Email</label>		<div class="elementor-field elementor-select-wrapper ">
                                                                <select name="s_c_order" id="form-field-email" class="elementor-field-textual elementor-size-sm" required="required" aria-required="true">
                                                                    <option value="">סידור לפי</option>
                                                                    <option
                                                                        <?php
                                                                        if (isset($_GET['s_c_order']) && !empty($_GET['s_c_order']) && ($_GET['s_c_order'] == 'ASC')) {
                                                                            echo 'selected ';
                                                                        }
                                                                        ?>
                                                                            value="ASC">A-Z</option>
                                                                    <option
                                                                        <?php
                                                                        if (isset($_GET['s_c_order']) && !empty($_GET['s_c_order']) && ($_GET['s_c_order'] == 'DESC')) {
                                                                            echo 'selected ';
                                                                        }
                                                                        ?>
                                                                            value="DESC">Z-A</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>


                                                    <div class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-100" >
                                                        <button type="submit" class="elementor-button elementor-size-sm">
						<span>
															<span class=" elementor-button-icon">
																												<span class="elementor-screen-only">Submit</span>
																	</span>
																				</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php foreach ($elements as $areaCC) : ?>

                <section class="elementor-element elementor-element-309e4ae elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="309e4ae" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" style="padding: 15px 0;">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-element elementor-element-d1af31e elementor-column elementor-col-100 elementor-top-column" data-id="d1af31e" data-element_type="column">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-19f94bf elementor-widget elementor-widget-heading" data-id="19f94bf" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default"><?php echo $areaCC['name'] ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-element elementor-element-af3ceee elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="af3ceee" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row" style="flex-wrap: wrap;">
                            <?php foreach ($areaCC['area'] as $elementKey => $elementValue) : ?>
                                <div
                                        <?php if (!wp_is_mobile()) echo 'style="flex: 50%;"'; ?>
                                        class="elementor-element elementor-element-7c511ea elementor-column elementor-col-50 elementor-top-column" data-id="7c511ea" data-element_type="column">
                                    <div class="elementor-column-wrap  elementor-element-populated">
                                        <div class="elementor-widget-wrap c-for-shadow">
                                            <section class="elementor-element elementor-element-ef28c67 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="ef28c67" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-element elementor-element-879e062 elementor-column elementor-col-50 elementor-inner-column" data-id="879e062" data-element_type="column">
                                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-1661f6f elementor-widget elementor-widget-heading" data-id="1661f6f" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default"><?php echo $elementValue['title'] ?></h2>		</div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-fec918d elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list mb-0" data-id="fec918d" data-element_type="widget" data-widget_type="icon-list.default">
                                                                        <div class="elementor-widget-container">
                                                                            <ul class="elementor-icon-list-items">
                                                                                <li class="elementor-icon-list-item">
                                                <span class="elementor-icon-list-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="#D4D4D4" d="M11.916 1.634C10.862.58 9.46 0 7.97 0c-1.49 0-2.89.58-3.945 1.634-1.95 1.95-2.192 5.618-.524 7.84l4.47 6.455 4.462-6.445c1.674-2.232 1.432-5.9-.518-7.85zm-3.893 5.93c-1.123 0-2.037-.914-2.037-2.037 0-1.122.914-2.036 2.037-2.036 1.122 0 2.036.914 2.036 2.036 0 1.123-.914 2.037-2.036 2.037z"></path></svg>						</span>
                                                                                    <span class="elementor-icon-list-text" style="font-weight: bold;"><?php echo $elementValue['place'] ?></span>
                                                                                </li>
                                                                                <li class="elementor-icon-list-item" style="font-weight: bold;">
                                                <span class="elementor-icon-list-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><g fill="none" fill-rule="evenodd"><circle cx="8" cy="8" r="6.5" fill="#D4D4D4"></circle><rect width="2" height="5" x="7" y="4" fill="#FFF" rx="1"></rect><rect width="2" height="4" x="8" y="6.5" fill="#FFF" rx="1" transform="rotate(90 9 8.5)"></rect></g></svg>						</span>
                                                                                    <span class="elementor-icon-list-text" style="font-size: 14px;"><?php echo $elementValue['time'] ?></span>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-55cb897 elementor-widget elementor-widget-text-editor" data-id="55cb897" data-element_type="widget" data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-text-editor elementor-clearfix"><p><?php echo $elementValue['description'] ?></p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-element elementor-element-d2d9caf elementor-column elementor-col-50 elementor-inner-column" data-id="d2d9caf" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">

                                                            <a target="_blank" href="<?php echo $elementValue['map_url'] ?>" class="elementor-column-wrap  elementor-element-populated" style="background-image: url(<?php echo $elementValue['image']['url'] ?>);">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-527be9a elementor-widget elementor-widget-heading" data-id="527be9a" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default"><?php echo $elementValue['image_text'] ?></h2>		</div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </section>

            <?php endforeach; ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>
