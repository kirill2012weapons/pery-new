<?php get_header(); ?>
<script>
    function disabledEventPropagation(event)
    {
        if (event.stopPropagation){
            event.stopPropagation();
        }
        else if(window.event){
            window.event.cancelBubble=true;
        }
    }

    // .loader-custom - LOADER
    function checkVisible(elm) {
        var rect = elm.getBoundingClientRect();
        var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
        return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    }

    jQuery(document).ready(function () {

        is_load_c = false;
        jQuery(window).scroll(function () {
            if (checkVisible(jQuery('[data-id="af3ceee"]')[0]) && !is_load_c) {
                jQuery.ajax({
                    type: 'POST',
                    url: myajaxcustom.url,
                    data: {action: 'pagination_ajax'},
                    beforeSend: function() {
                        if (jQuery('.product-raw-c:hidden:first').length > 0) {
                            jQuery('.loader-custom').show();
                        }
                        is_load_c = true;
                    },
                    success: function(data) {
                        jQuery('.product-raw-c:hidden:first').slideDown();
                    },
                    error: function(xhr) {
                        jQuery('.product-raw-c:hidden:first').slideDown();
                        jQuery('.loader-custom').hide();
                    },
                    complete: function() {
                        jQuery('.loader-custom').hide();
                        is_load_c = false;
                    },
                });
            }
        });
    })

</script>

<style>
    @media (max-width: 767px) {
        .elementor-column {
            width: 100%;
            padding: 12px;
        }
        .whatsapp-img{
             display: inline-flex !important;
         }
    }

    @media (min-width: 100px) and (max-width: 767px) {
        .elementor-element-c15a6a1 {
            display: none;
        }
        .cent-mob{
            display: flex;
            justify-content: center;
        }
    }
    .whatsapp-img{
        display: none;
    }
    .drop-icon {
        position: relative;
    }
    .drop-icon:after{
        position: absolute;
        width: 0;
        left: 30px;
        content: "";
        font-family: "Font Awesome 5 Free";
        color: #b1b1b1;
    }
    .green-badge{
        background-color: #DEFFFC;
        border-style: solid;
        border-width: 1px;
        border-color: #00988D;
        position: absolute;
        padding: 10px !important;
        z-index: 111;
        font-size: 16px;
        left: 0;
        color: #009889;
    }
</style>

<?php

$metaQuery = ['relation' => 'END'];

if($_GET['c_toprice'] == "עד שנה")
	$_GET['c_toprice'] = null;
if($_GET['fromprice'] == "משנה")
	$_GET['fromprice'] = null;
if($_GET['c_gir'] == "יצרן")
	$_GET['c_gir'] = null;

if (isset($_GET['c_toprice']) && !empty($_GET['c_toprice'])) {
    $metaQuery[] = [
        'key' => '_price',
        'value' => $_GET['c_toprice'],
        'compare' => '<',
        'type' => 'NUMERIC'
    ];
}
if (isset($_GET['fromprice']) && !empty($_GET['fromprice'])) {
    $metaQuery[] = [
        'key' => '_price',
        'value' => $_GET['fromprice'],
        'compare' => '>',
        'type' => 'NUMERIC'
    ];
}
if (isset($_GET['c_cartype']) && !empty($_GET['c_cartype'])) {
    $serialized_value =
        serialize( 'name' ) .
        serialize( 'Car Type' ) .
        serialize( 'value' ) .
        serialize( $_GET['c_cartype'] )
    ;
    $metaQuery[] = [
        'key'     => '_product_attributes',
        'value'   => $serialized_value,
        'compare' => 'LIKE',
    ];
}

//if (isset($_GET['c_gir']) && !empty($_GET['c_gir'])) {
//    $serialized_value =
//        serialize( 'name' ) .
//        serialize( 'Gir' ) .
//        serialize( 'value' ) .
//        serialize( $_GET['c_gir'] )
//    ;
//    $metaQuery[] = [
//        'key'     => '_product_attributes',
//        'value'   => $serialized_value,
//        'compare' => 'LIKE',
//    ];
//}

/*if (isset($_GET['c_codedegem']) && !empty($_GET['c_codedegem'])) {
    $serialized_value =
        serialize( 'name' ) .
        serialize( 'Car Type' ) .
        serialize( 'value' ) .
        serialize( $_GET['c_codedegem'] )
    ;
    $metaQuery[] = [
        'key'     => '_product_attributes',
        'value'   => $serialized_value,
        'compare' => 'LIKE',
    ];
}*/

$productsArgs = [
    'status' => 'publish',
    'post_type' => 'product',
    'posts_per_page' => -1,
    'order'          => 'ASC',
    'meta_query' => $metaQuery,
];

if (isset($_GET['c_gir']) && !empty($_GET['c_gir'])) {
    $productsArgs['s'] = $_GET['c_gir'].' '.$_GET['c_codedegem'];
}

if (is_shop()) {
    $productsArgs['tax_query'] = [
        'relation' => 'AND',
        [
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => 'buy',
        ]
    ];
} else {
    $category = get_queried_object();
    if ($category instanceof WP_Term) {
        $productsArgs['tax_query'] = [
            'relation' => 'AND',
            [
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $category->slug,
            ]
        ];
    }
}

$productsPosts = new WP_Query($productsArgs);
foreach ($productsPosts->posts as $post) {
    $products[] = wc_get_product($post->ID);
}


if (isset($_GET['c_toyear']) || isset($_GET['c_fromyear'])) {
    $toYear = 2500;
    $fromYear = 1500;
    if (isset($_GET['c_toyear']) && !empty($_GET['c_toyear']) && ($_GET['c_toyear'] != '') ) $toYear = $_GET['c_toyear'];
    if (isset($_GET['c_fromyear']) && !empty($_GET['c_fromyear']) && ($_GET['c_fromyear'] != '') ) $fromYear = $_GET['c_fromyear'];

    $buff = [];
    foreach ($products as $pr) {
        if (true) {
            $mY = $pr->get_attribute('Manufacture Year');
            if ( ($mY >= $fromYear) && ($mY <= $toYear ) ) {
                $buff[] = $pr;
            }
        }
    }
    $products = $buff;
}

if (isset($_GET['c_tokms']) || isset($_GET['c_fromkms'])) {
    $toKMS = 99999999;
    $fromKMS = 0;
    if (isset($_GET['c_tokms']) && ($_GET['c_tokms'] >= 0) && ($_GET['c_tokms'] != '') ) $toKMS = $_GET['c_tokms'];
    if (isset($_GET['c_fromkms']) && ($_GET['c_fromkms'] >= 0) && ($_GET['c_fromkms'] != '') ) $fromKMS = $_GET['c_fromkms'];
    $buff = [];
    foreach ($products as $pr) {
        if (true) {
            $kms = $pr->get_attribute('KMS');
            if ( ($kms >= $fromKMS) && ($kms <= $toKMS ) ) {
                $buff[] = $pr;
            }
        }
    }
    $products = $buff;
}

/**
 * is_check_c = true
 */
if (isset($_GET['is_check_c'])) {
    $buff = [];
    foreach ($products as $pr) {
        if (true) {
            $check = get_field('promotion', $pr->get_id());
            if ($check) {
                $buff[] = $pr;
            }
        }
    }
    $products = $buff;
}

?>

<div data-elementor-type="wp-page" data-elementor-id="177" class="elementor elementor-177" data-elementor-settings="[]" style="background-color: #F7F7F7;">
    <div class="elementor-inner">
        <div class="elementor-section-wrap">
            <section
                    class="elementor-element elementor-element-71bf663 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                    data-id="71bf663" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-background-overlay"></div>
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div
                                class="elementor-element elementor-element-f16f121 elementor-column elementor-col-100 elementor-top-column"
                                data-id="f16f121" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-c454581 elementor-widget elementor-widget-heading"
                                         data-id="c454581" data-element_type="widget" data-widget_type="heading.default">
                                        <div class="elementor-widget-container">
										<?php if($_GET['c_tokms'] == '0'): ?>
											<h2 class="elementor-heading-title elementor-size-default">רכבי אפס קילומטר</h2></div>
										<?php elseif($category->slug == 'outlet'): ?>
											<h2 class="elementor-heading-title elementor-size-default">קטלוג אאוטלט</h2></div>
										<?php elseif(isset($_GET['is_check_c'])): ?>
											<h2 class="elementor-heading-title elementor-size-default">מבצעי החודש של פרי</h2></div>
										<?php else: ?>
											<h2 class="elementor-heading-title elementor-size-default">רכישת רכב</h2></div>
										<?php endif; ?>
                                    </div>
                                    <div
                                            class="elementor-element elementor-element-3d93c29 elementor-mobile-align-center elementor-widget elementor-widget-button"
                                            data-id="3d93c29" data-element_type="widget" data-widget_type="button.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-button-wrapper cent-mob">
                                                <a href="#" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-angle-left"></i>			</span>
						<span class="elementor-button-text">קליק לנציג</span>
		</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section
                    class="elementor-element elementor-element-15493f4 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                    data-id="15493f4" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div
                                class="elementor-element elementor-element-7410bac elementor-column elementor-col-100 elementor-top-column"
                                data-id="7410bac" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                            class="elementor-element elementor-element-ff21ed3 elementor-button-align-stretch elementor-widget elementor-widget-form"
                                            data-id="ff21ed3" data-element_type="widget" data-widget_type="form.default">
                                        <div class="elementor-widget-container">
                                            <form class="elementor-form" id="serch-form" method="get" action="?">
                                                <div class="elementor-form-fields-wrapper elementor-labels-">

                                                    <?php /*  <div
                              class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-name elementor-col-20 elementor-sm-100">
                            <label for="form-field-name" class="elementor-field-label elementor-screen-only">
                              קטגוריית רכב
                            </label>
                            <input type="text"
                                   name="c_cartype"
                                <?php if (isset($_GET['c_cartype'])) echo ' value="' . $_GET['c_cartype'] . '"' ?>
                                   id="form-field-name"
                                   class="elementor-field elementor-size-sm  elementor-field-textual"
                                   placeholder="קטגוריית רכב">
                          </div> */ ?>

                                                    <div
                                                            id="c_gir"
                                                            class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-email elementor-col-25 elementor-sm-50 elementor-field-required drop-icon">
                                                        <label for="form-field-email"
                                                               class="elementor-field-label elementor-screen-only">
                                                            יצרן
                                                        </label>
                                                        <select name="c_gir"
                                                                id="form-field-email"
                                                                class="elementor-field elementor-size-sm  elementor-field-textual cust-select"
                                                                placeholder="יצרן">
                                                            <option value="">יצרן</option>
                                                            <option value="שברולט" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "שברולט") { echo "selected"; } ?>>שברולט</option>
                                                            <option value="אודי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אודי") { echo "selected"; } ?>>אודי</option>
                                                            <option value="אופל" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אופל") { echo "selected"; } ?>>אופל</option>
                                                            <option value="אלפא רומאו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אלפא רומאו") { echo "selected"; } ?>>אלפא רומאו</option>
                                                            <option value="BMW" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "BMW") { echo "selected"; } ?>>BMW</option>
                                                            <option value="דייהו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "דייהו") { echo "selected"; } ?>>דייהו</option>
                                                            <option value="דייהטסו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "דייהטסו") { echo "selected"; } ?>>דייהטסו</option>
                                                            <option value="הונדה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "הונדה") { echo "selected"; } ?>>הונדה</option>
                                                            <option value="וולוו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "וולוו") { echo "selected"; } ?>>וולוו</option>
                                                            <option value="טויוטה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "טויוטה") { echo "selected"; } ?>>טויוטה</option>
                                                            <option value="יונדאי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "יונדאי") { echo "selected"; } ?>>יונדאי</option>
                                                            <option value="לאנציה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "לאנציה") { echo "selected"; } ?>>לאנציה</option>
                                                            <option value="מאזדה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מאזדה") { echo "selected"; } ?>>מאזדה</option>
                                                            <option value="מיצובישי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מיצובישי") { echo "selected"; } ?>>מיצובישי</option>
                                                            <option value="מרצדס" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מרצדס") { echo "selected"; } ?>>מרצדס</option>
                                                            <option value="מרצדס - לא בשימוש" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מרצדס - לא בשימוש") { echo "selected"; } ?>>מרצדס - לא בשימוש</option>
                                                            <option value="ניסאן" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "ניסאן") { echo "selected"; } ?>>ניסאן</option>
                                                            <option value="סוזוקי - לא בשימוש" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סוזוקי - לא בשימוש") { echo "selected"; } ?>>סוזוקי - לא בשימוש</option>
                                                            <option value="סובארו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סובארו") { echo "selected"; } ?>>סובארו</option>
                                                            <option value="סוזוקי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סוזוקי") { echo "selected"; } ?>>סוזוקי</option>
                                                            <option value="סיאט" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סיאט") { echo "selected"; } ?>>סיאט</option>
                                                            <option value="סיטרואן" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סיטרואן") { echo "selected"; } ?>>סיטרואן</option>
                                                            <option value="פורד" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פורד") { echo "selected"; } ?>>פורד</option>
                                                            <option value="פיאט" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פיאט") { echo "selected"; } ?>>פיאט</option>
                                                            <option value="פיגו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פיגו") { echo "selected"; } ?>>פיגו</option>
                                                            <option value="קאיה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "קאיה") { echo "selected"; } ?>>קאיה</option>
                                                            <option value="רובר" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "רובר") { echo "selected"; } ?>>רובר</option>
                                                            <option value="רנו- לא בשימוש" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "רנו- לא בשימוש") { echo "selected"; } ?>>רנו- לא בשימוש</option>
                                                            <option value="ביואיק" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "ביואיק") { echo "selected"; } ?>>ביואיק</option>
                                                            <option value="דודג" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "דודג") { echo "selected"; } ?>>דודג</option>
                                                            <option value="קדילאק" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "קדילאק") { echo "selected"; } ?>>קדילאק</option>
                                                            <option value="רנו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "רנו") { echo "selected"; } ?>>רנו</option>
                                                            <option value="איסוזו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "איסוזו") { echo "selected"; } ?>>איסוזו</option>
                                                            <option value="G M C" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "G M C") { echo "selected"; } ?>>G M C</option>
                                                            <option value="פונטיאק" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פונטיאק") { echo "selected"; } ?>>פונטיאק</option>
                                                            <option value="אוטוביאנקי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אוטוביאנקי") { echo "selected"; } ?>>אוטוביאנקי</option>
                                                            <option value="מלגזה (אנהוי הלי)" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מלגזה (אנהוי הלי)") { echo "selected"; } ?>>מלגזה (אנהוי הלי)</option>
                                                            <option value="אינטרנאשיונל" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אינטרנאשיונל") { echo "selected"; } ?>>אינטרנאשיונל</option>
                                                            <option value="יגואר" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "יגואר") { echo "selected"; } ?>>יגואר</option>
                                                            <option value="פורשה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פורשה") { echo "selected"; } ?>>פורשה</option>
                                                            <option value="אופנוע" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אופנוע") { echo "selected"; } ?>>אופנוע</option>
                                                            <option value="אוטובאנקי- לא בשימוש" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אוטובאנקי- לא בשימוש") { echo "selected"; } ?>>אוטובאנקי- לא בשימוש</option>
                                                            <option value="פולקסווגן" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פולקסווגן") { echo "selected"; } ?>>פולקסווגן</option>
                                                            <option value="סאנגיונג" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סאנגיונג") { echo "selected"; } ?>>סאנגיונג</option>
                                                            <option value="דאף" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "דאף") { echo "selected"; } ?>>דאף</option>
                                                            <option value="סיטרואן - לא בשימוש" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סיטרואן - לא בשימוש") { echo "selected"; } ?>>סיטרואן - לא בשימוש</option>
                                                            <option value="קרייזלר" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "קרייזלר") { echo "selected"; } ?>>קרייזלר</option>
                                                            <option value="האמר" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "האמר") { echo "selected"; } ?>>האמר</option>
                                                            <option value="משאית - לא בשימוש" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "משאית - לא בשימוש") { echo "selected"; } ?>>משאית - לא בשימוש</option>
                                                            <option value="ג י. או. - GEO" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "ג י. או. - GEO") { echo "selected"; } ?>>ג י. או. - GEO</option>
                                                            <option value="סמארט - SMART" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סמארט - SMART") { echo "selected"; } ?>>סמארט - SMART</option>
                                                            <option value="לאנד רובר" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "לאנד רובר") { echo "selected"; } ?>>לאנד רובר</option>
                                                            <option value="גיפ" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "גיפ") { echo "selected"; } ?>>גיפ</option>
                                                            <option value="סקודה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סקודה") { echo "selected"; } ?>>סקודה</option>
                                                            <option value="פרי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פרי") { echo "selected"; } ?>>פרי</option>
                                                            <option value="פרי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פרי") { echo "selected"; } ?>>פרי</option>
                                                            <option value="פרי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פרי") { echo "selected"; } ?>>פרי</option>
                                                            <option value="פולאריס" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פולאריס") { echo "selected"; } ?>>פולאריס</option>
                                                            <option value="לקסוס" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "לקסוס") { echo "selected"; } ?>>לקסוס</option>
                                                            <option value="דאציה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "דאציה") { echo "selected"; } ?>>דאציה</option>
                                                            <option value="קאן אם (טרקטורון)" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "קאן אם (טרקטורון)") { echo "selected"; } ?>>קאן אם (טרקטורון)</option>
                                                            <option value="איווקו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "איווקו") { echo "selected"; } ?>>איווקו</option>
                                                            <option value="די אס" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "די אס") { echo "selected"; } ?>>די אס</option>
                                                            <option value="שרות טיפולים" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "שרות טיפולים") { echo "selected"; } ?>>שרות טיפולים</option>
                                                            <option value="מ אודי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מ אודי") { echo "selected"; } ?>>מ אודי</option>
                                                            <option value="מ אופל" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מ אופל") { echo "selected"; } ?>>מ אופל</option>
                                                            <option value="אלפא רומאו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אלפא רומאו") { echo "selected"; } ?>>אלפא רומאו</option>
                                                            <option value="איסוזו ארהב" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "איסוזו ארהב") { echo "selected"; } ?>>איסוזו ארהב</option>
                                                            <option value="איווקו איטליה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "איווקו איטליה") { echo "selected"; } ?>>איווקו איטליה</option>
                                                            <option value="איסוזו יפן" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "איסוזו יפן") { echo "selected"; } ?>>איסוזו יפן</option>
                                                            <option value="ב.מ.וו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "ב.מ.וו") { echo "selected"; } ?>>ב.מ.וו</option>
                                                            <option value="ביואיק" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "ביואיק") { echo "selected"; } ?>>ביואיק</option>
                                                            <option value="דודג" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "דודג") { echo "selected"; } ?>>דודג</option>
                                                            <option value="סמארט" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סמארט") { echo "selected"; } ?>>סמארט</option>
                                                            <option value="מרצדס" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מרצדס") { echo "selected"; } ?>>מרצדס</option>
                                                            <option value="דייהטסו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "דייהטסו") { echo "selected"; } ?>>דייהטסו</option>
                                                            <option value="דייהו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "דייהו") { echo "selected"; } ?>>דייהו</option>
                                                            <option value="הונדה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "הונדה") { echo "selected"; } ?>>הונדה</option>
                                                            <option value="אסטון מרטין" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אסטון מרטין") { echo "selected"; } ?>>אסטון מרטין</option>
                                                            <option value="דאציה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "דאציה") { echo "selected"; } ?>>דאציה</option>
                                                            <option value="וולוו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "וולוו") { echo "selected"; } ?>>וולוו</option>
                                                            <option value="טויוטה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "טויוטה") { echo "selected"; } ?>>טויוטה</option>
                                                            <option value="לקסוס" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "לקסוס") { echo "selected"; } ?>>לקסוס</option>
                                                            <option value="גופיל" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "גופיל") { echo "selected"; } ?>>גופיל</option>
                                                            <option value="טזארי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "טזארי") { echo "selected"; } ?>>טזארי</option>
                                                            <option value="ליצי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "ליצי") { echo "selected"; } ?>>ליצי</option>
                                                            <option value="נאנגין" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "נאנגין") { echo "selected"; } ?>>נאנגין</option>
                                                            <option value="יגואר" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "יגואר") { echo "selected"; } ?>>יגואר</option>
                                                            <option value="יונדאי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "יונדאי") { echo "selected"; } ?>>יונדאי</option>
                                                            <option value="לינקולן" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "לינקולן") { echo "selected"; } ?>>לינקולן</option>
                                                            <option value="לנד רובר" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "לנד רובר") { echo "selected"; } ?>>לנד רובר</option>
                                                            <option value="לנציה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "לנציה") { echo "selected"; } ?>>לנציה</option>
                                                            <option value="מזראטי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מזראטי") { echo "selected"; } ?>>מזראטי</option>
                                                            <option value="מאזדה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מאזדה") { echo "selected"; } ?>>מאזדה</option>
                                                            <option value="מיצובישי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מיצובישי") { echo "selected"; } ?>>מיצובישי</option>
                                                            <option value="ניסאן" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "ניסאן") { echo "selected"; } ?>>ניסאן</option>
                                                            <option value="אינפיניטי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אינפיניטי") { echo "selected"; } ?>>אינפיניטי</option>
                                                            <option value="סאאב" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סאאב") { echo "selected"; } ?>>סאאב</option>
                                                            <option value="סובארו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סובארו") { echo "selected"; } ?>>סובארו</option>
                                                            <option value="סקודה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סקודה") { echo "selected"; } ?>>סקודה</option>
                                                            <option value="סוזוקי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סוזוקי") { echo "selected"; } ?>>סוזוקי</option>
                                                            <option value="סאנגיונג" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סאנגיונג") { echo "selected"; } ?>>סאנגיונג</option>
                                                            <option value="פולקסווגן" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פולקסווגן") { echo "selected"; } ?>>פולקסווגן</option>
                                                            <option value="פורד אמריקאי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פורד אמריקאי") { echo "selected"; } ?>>פורד אמריקאי</option>
                                                            <option value="פורד" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פורד") { echo "selected"; } ?>>פורד</option>
                                                            <option value="פורשה" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פורשה") { echo "selected"; } ?>>פורשה</option>
                                                            <option value="פיאט" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פיאט") { echo "selected"; } ?>>פיאט</option>
                                                            <option value="אבארט" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אבארט") { echo "selected"; } ?>>אבארט</option>
                                                            <option value="פיגו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פיגו") { echo "selected"; } ?>>פיגו</option>
                                                            <option value="פרארי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "פרארי") { echo "selected"; } ?>>פרארי</option>
                                                            <option value="סיאט" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סיאט") { echo "selected"; } ?>>סיאט</option>
                                                            <option value="סיטרואן" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "סיטרואן") { echo "selected"; } ?>>סיטרואן</option>
                                                            <option value="אם גי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "אם גי") { echo "selected"; } ?>>אם גי</option>
                                                            <option value="די אס DS" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "די אס DS") { echo "selected"; } ?>>די אס DS</option>
                                                            <option value="קאדילק" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "קאדילק") { echo "selected"; } ?>>קאדילק</option>
                                                            <option value="קרייזלר" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "קרייזלר") { echo "selected"; } ?>>קרייזלר</option>
                                                            <option value="גיפ" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "גיפ") { echo "selected"; } ?>>גיפ</option>
                                                            <option value="קיה מוטורס" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "קיה מוטורס") { echo "selected"; } ?>>קיה מוטורס</option>
                                                            <option value="רנו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "רנו") { echo "selected"; } ?>>רנו</option>
                                                            <option value="רובר" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "רובר") { echo "selected"; } ?>>רובר</option>
                                                            <option value="גרייט וול" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "גרייט וול") { echo "selected"; } ?>>גרייט וול</option>
                                                            <option value="שברולט" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "שברולט") { echo "selected"; } ?>>שברולט</option>
                                                            <option value="איסוזו תאילנד" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "איסוזו תאילנד") { echo "selected"; } ?>>איסוזו תאילנד</option>
                                                            <option value="בנטלי" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "בנטלי") { echo "selected"; } ?>>בנטלי</option>
                                                            <option value=" גיפ-תער" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מ גיפ-תער") { echo "selected"; } ?>>מ גיפ-תער</option>
                                                            <option value="מ טאטא" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מ טאטא") { echo "selected"; } ?>>מ טאטא</option>
                                                            <option value="מ סייק" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מ סייק") { echo "selected"; } ?>>מ סייק</option>
                                                            <option value=" פיאגו" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "מ פיאגו") { echo "selected"; } ?>>מ פיאגו</option>
                                                            <option value="לא ידוע" <?php if(!empty($_GET['c_gir']) && $_GET['c_gir'] == "לא ידוע") { echo "selected"; } ?>>לא ידוע</option>

                                                        </select>
                                                    </div>

                                                    <div
                                                        id="c_codedegem"
                                                            class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-message elementor-col-25 elementor-sm-50">
                                                        <label for="form-field-message"
                                                               class="elementor-field-label elementor-screen-only">
                                                            דגם
                                                        </label>
                                                        <input type="text"
                                                               name="c_codedegem"
                                                            <?php if (isset($_GET['c_codedegem'])) echo ' value="' . $_GET['c_codedegem'] . '"' ?>
                                                               id="form-field-message"
                                                               class="elementor-field elementor-size-sm  elementor-field-textual"
                                                               placeholder="דגם">
                                                    </div>

                                                    <div
                                                        id="c_fromyear"
                                                            class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-field_1 elementor-col-25 elementor-sm-50">
                                                        <label for="form-field-field_1"
                                                               class="elementor-field-label elementor-screen-only">
                                                            משנה
                                                        </label>
                                                        <input type="text"
                                                               name="c_fromyear"
                                                            <?php if (isset($_GET['c_fromyear'])) echo ' value="' . $_GET['c_fromyear'] . '"' ?>
                                                               id="form-field-field_1"
                                                               class="elementor-field elementor-size-sm  elementor-field-textual"
                                                               placeholder="משנה">
                                                    </div>

                                                    <div
                                                        id="c_toyear"
                                                            class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-field_2 elementor-col-25 elementor-sm-50">
                                                        <label for="form-field-field_2" class="elementor-field-label elementor-screen-only">
                                                            עד שנה
                                                        </label>
                                                        <input type="text"
                                                               name="c_toyear"
                                                            <?php if (isset($_GET['c_toyear'])) echo ' value="' . $_GET['c_toyear'] . '"' ?>
                                                               id="form-field-field_2"
                                                               class="elementor-field elementor-size-sm  elementor-field-textual"
                                                               placeholder="עד שנה">
                                                    </div>

                                                    <div
                                                        id="fromprice"
                                                            class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-field_3 elementor-col-20 elementor-sm-50">
                                                        <label for="form-field-field_3"
                                                               class="elementor-field-label elementor-screen-only">
                                                            ממחיר
                                                        </label>
                                                        <input type="text"
                                                               name="fromprice"
                                                            <?php if (isset($_GET['fromprice'])) echo ' value="' . $_GET['fromprice'] . '"' ?>
                                                               id="form-field-field_3"
                                                               class="elementor-field elementor-size-sm  elementor-field-textual"
                                                               placeholder="ממחיר">
                                                    </div>

                                                    <div
                                                        id="c_toprice"
                                                            class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-field_4 elementor-col-20 elementor-sm-50">
                                                        <label for="form-field-field_4" class="elementor-field-label elementor-screen-only">
                                                            עד מחיר
                                                        </label>
                                                        <input type="text"
                                                               name="c_toprice"
                                                            <?php if (isset($_GET['c_toprice'])) echo ' value="' . $_GET['c_toprice'] . '"' ?>
                                                               id="form-field-field_4"
                                                               class="elementor-field elementor-size-sm  elementor-field-textual"
                                                               placeholder="עד מחיר">
                                                    </div>

                                                    <div
                                                        id="btn-sbmit"
                                                            class="elementor-field-group elementor-column elementor-col-20">
                                                        <button style="    width: 100%;" type="submit" onclick="jQuery('#serch-form').off().submit();" class="elementor-button elementor-size-sm">
						<span>

																						<span class="elementor-button-text">לקטלוג הרכבים</span>
													</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php
            $newArrayProduct = [];
            $count = 0;
            $counterr = 0;
            foreach ($products as $product) {
                if (($counterr % 3)!=0) {
                    $newArrayProduct[$count][] = $product;
                } else {
                    $count += 1;
                    $newArrayProduct[$count][] = $product;
                }
                $counterr += 1;
            }
            ?>
            <?php $counterr = 3; ?>
            <?php if (!empty($newArrayProduct)) : ?>
                <?php $prCounter = 0; ?>
                <?php foreach ($newArrayProduct as $row => $products) : ?>
                    <section
                        <?php if ($prCounter > 0) echo 'style="display:none;"'; ?>
                            product-attr="<?php echo $prCounter; $prCounter++; ?>"
                            class="product-raw-c elementor-element elementor-element-af3ceee elementor-section-height-min-height elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                            data-id="af3ceee" data-element_type="section"
                            data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-wider" style="min-height: 572px;">
                            <div class="elementor-row">
                                <?php foreach ($products as $product) : ?>
                                    <?php
                                    /** @var $product WC_Product */
                                    //$product = wc_get_product($product->ID);
                                    ?>
                                    <div
                                            class="elementor-element elementor-element-921d4d6 elementor-column elementor-col-33 elementor-top-column"
                                            data-id="921d4d6" data-element_type="column"
                                            data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <?php if (get_field('promotion', $product->get_id())) : ?>
                                                    <div class="elementor-column-wrap  elementor-element-populated c-promo" style="position: relative;">
                                                        <div class="elementor-widget-wrap">
                                                            <div
                                                                    class="elementor-element elementor-element-df2a39d elementor-widget elementor-widget-heading"
                                                                    data-id="df2a39d" data-element_type="widget" data-widget_type="heading.default">
                                                                <div class="elementor-widget-container">
                                                                    <h2 class="elementor-heading-title elementor-size-default green-badge">משתתף במבצע החודש</h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                                <div
                                                        class="elementor-element elementor-element-887b2e0 elementor-arrows-position-inside elementor-pagination-position-outside elementor-widget elementor-widget-image-carousel"
                                                        data-id="887b2e0" data-element_type="widget"
                                                        data-settings="{&quot;slides_to_show&quot;:&quot;1&quot;,&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;effect&quot;:&quot;slide&quot;,&quot;speed&quot;:500,&quot;direction&quot;:&quot;ltr&quot;}"
                                                        data-widget_type="image-carousel.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-image-carousel-wrapper swiper-container" dir="ltr">
                                                            <div class="elementor-image-carousel swiper-wrapper">
                                                                <div class="swiper-slide">
                                                                    <?php if (wp_is_mobile()) : ?>
                                                                        <figure class="swiper-slide-inner"><a href="<?php echo get_post_permalink($product->get_id()) ?>"><img class="swiper-slide-image"
                                                                                                                                                                               src="<?php echo get_the_post_thumbnail_url( $product->get_id() ) ? get_the_post_thumbnail_url( $product->get_id() ) : wc_placeholder_img_src(); ?>"
                                                                                                                                                                               alt="" border="0"></a></figure>
                                                                    <?php else: ?>
                                                                        <figure class="swiper-slide-inner">

                                                                            <a
                                                                                    href="<?php echo get_post_permalink($product->get_id()) ?>">
                                                                                <div
                                                                                        style="
                                                                                                height: 215px;
                                                                                                width: 100%;
                                                                                                background-image: url(<?php echo get_the_post_thumbnail_url( $product->get_id() ); ?>);
                                                                                                background-repeat: no-repeat;
                                                                                                background-position: center;
                                                                                                background-size: cover;"></div>

                                                                            </a>
                                                                        </figure>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-97d6425 elementor-widget elementor-widget-heading"
                                                     data-id="97d6425" data-element_type="widget" data-widget_type="heading.default">
                                                    <div class="elementor-widget-container">
                                                        <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_title(); ?></h2></div>
                                                </div>
                                                <div class="elementor-element elementor-element-d8aeb32 elementor-widget elementor-widget-heading"
                                                     data-id="d8aeb32" data-element_type="widget" data-widget_type="heading.default">
                                                    <div class="elementor-widget-container">
                                                        <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_attribute('Car Type'); ?></h2></div>
                                                </div>
                                                <section
                                                        class="elementor-element elementor-element-71ef6b6 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                        data-id="71ef6b6" data-element_type="section">
                                                    <div class="elementor-container elementor-column-gap-default">
                                                        <div class="elementor-row">
                                                            <?php if ($product->get_attribute('Manufacture Year')) : ?>
                                                                <div
                                                                        class="elementor-element elementor-element-9946dc9 elementor-column elementor-col-33 elementor-inner-column c-my-k"
                                                                        data-id="9946dc9" data-element_type="column">
                                                                    <div class="elementor-column-wrap  elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                    class="elementor-element elementor-element-da0b0af elementor-widget elementor-widget-image"
                                                                                    data-id="da0b0af" data-element_type="widget" data-widget_type="image.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-image">
                                                                                        <figure class="wp-caption position-relative">
                                                                                            <img width="45" height="45"
                                                                                                 src="/wp-content/uploads/2020/02/BALL.png"
                                                                                                 class="attachment-large size-large" alt="">
                                                                                            <figcaption style="line-height: 4.1em;" class="widget-image-caption wp-caption-text green-round">שנה</figcaption>
                                                                                        </figure>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                    class="elementor-element elementor-element-3266e0f elementor-widget elementor-widget-heading"
                                                                                    data-id="3266e0f" data-element_type="widget" data-widget_type="heading.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_attribute('Manufacture Year') ?></h2></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                            <?php if ($product->get_attribute('KMS')) : ?>
                                                                <div
                                                                        class="elementor-element elementor-element-9946dc9 elementor-column elementor-col-33 elementor-inner-column c-kms-k"
                                                                        data-id="9946dc9" data-element_type="column">
                                                                    <div class="elementor-column-wrap  elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div
                                                                                    class="elementor-element elementor-element-da0b0af elementor-widget elementor-widget-image"
                                                                                    data-id="da0b0af" data-element_type="widget" data-widget_type="image.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-image">
                                                                                        <figure class="wp-caption position-relative">
                                                                                            <img width="38" height="38"
                                                                                                 src="/wp-content/uploads/2020/02/BALL.png"
                                                                                                 class="attachment-large size-large" alt="">
                                                                                            <figcaption class="widget-image-caption wp-caption-text c-km green-round">KM</figcaption>
                                                                                        </figure>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                    class="elementor-element elementor-element-3266e0f elementor-widget elementor-widget-heading"
                                                                                    data-id="3266e0f" data-element_type="widget" data-widget_type="heading.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <h2 class="elementor-heading-title elementor-size-default"><?php echo number_format($product->get_attribute('KMS')) ?></h2></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                            <!--                                                        --><?php //if ($product->get_attribute('Color')) : ?>
                                                            <!--                                                            <div-->
                                                            <!--                                                              class="elementor-element elementor-element-9946dc9 elementor-column elementor-col-33 elementor-inner-column"-->
                                                            <!--                                                              data-id="9946dc9" data-element_type="column">-->
                                                            <!--                                                                <div class="elementor-column-wrap  elementor-element-populated">-->
                                                            <!--                                                                    <div class="elementor-widget-wrap">-->
                                                            <!--                                                                        <div-->
                                                            <!--                                                                          class="elementor-element elementor-element-da0b0af elementor-widget elementor-widget-image"-->
                                                            <!--                                                                          data-id="da0b0af" data-element_type="widget" data-widget_type="image.default">-->
                                                            <!--                                                                            <div class="elementor-widget-container">-->
                                                            <!--                                                                                <div class="elementor-image">-->
                                                            <!--                                                                                    <figure class="wp-caption">-->
                                                            <!--                                                                                        <img width="38" height="38"-->
                                                            <!--                                                                                             src="/wp-content/uploads/2020/02/BALL.png"-->
                                                            <!--                                                                                             class="attachment-large size-large" alt="">-->
                                                            <!--                                                                                        <figcaption class="widget-image-caption wp-caption-text">Color</figcaption>-->
                                                            <!--                                                                                    </figure>-->
                                                            <!--                                                                                </div>-->
                                                            <!--                                                                            </div>-->
                                                            <!--                                                                        </div>-->
                                                            <!--                                                                        <div-->
                                                            <!--                                                                          class="elementor-element elementor-element-3266e0f elementor-widget elementor-widget-heading"-->
                                                            <!--                                                                          data-id="3266e0f" data-element_type="widget" data-widget_type="heading.default">-->
                                                            <!--                                                                            <div class="elementor-widget-container">-->
                                                            <!--                                                                                <h2 class="elementor-heading-title elementor-size-default">--><?php //echo $product->get_attribute('Color') ?><!--</h2></div>-->
                                                            <!--                                                                        </div>-->
                                                            <!--                                                                    </div>-->
                                                            <!--                                                                </div>-->
                                                            <!--                                                            </div>-->
                                                            <!--                                                        --><?php //endif; ?>
                                                            <?php if ($product->get_attribute('Hand')) : ?>
                                                                <div class="elementor-element elementor-element-8fe2c70 elementor-column elementor-col-33 elementor-inner-column c-h-k" data-id="8fe2c70" data-element_type="column">
                                                                    <div class="elementor-column-wrap  elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div class="elementor-element elementor-element-7880ab5 elementor-widget elementor-widget-image" data-id="7880ab5" data-element_type="widget" data-widget_type="image.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-image">
                                                                                        <figure class="wp-caption position-relative">
                                                                                            <img width="38" height="38" src="/wp-content/uploads/2020/02/BALL.png" class="attachment-large size-large" alt="">											<figcaption class="widget-image-caption wp-caption-text green-round">יד</figcaption>
                                                                                        </figure>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-element elementor-element-2d1533b elementor-widget elementor-widget-heading" data-id="2d1533b" data-element_type="widget" data-widget_type="heading.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_attribute('Hand') ?></h2>		</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </section>
                                                <div class="elementor-element elementor-element-3e7edd4 elementor-widget elementor-widget-text-editor"
                                                     data-id="3e7edd4" data-element_type="widget" data-widget_type="text-editor.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-text-editor elementor-clearfix"><p><?php echo get_the_excerpt($product->get_id()); ?></p></div>
                                                    </div>
                                                </div>
                                                <div
                                                    <?php if (!wp_is_mobile()) : ?>
                                                        style="position: absolute; bottom: 0;"
                                                    <?php endif; ?>
                                                        class="elementor-element elementor-element-32bd69a elementor-align-justify elementor-widget elementor-widget-button"
                                                        data-id="32bd69a" data-element_type="widget" data-widget_type="button.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-button-wrapper">
                                                            <?php if (wp_is_mobile()) : ?>
                                                                <a href="#" class="whatsapp-img" style="vertical-align: top;">
                                                                    <img style="height: 42px; position: relative;" src="<?php echo home_url() . '/wp-content/themes/hello-elementor/assets/images/whatsapp.png' ?>" alt="">
                                                                </a>
                                                            <?php endif; ?>
                                                            <a
                                                                    <?php if (wp_is_mobile()) { echo 'style="width: 274px;"'; } ?>
                                                                    href="<?php echo get_post_permalink($product->get_id()) ?>" class="elementor-button-link elementor-button elementor-size-lg" role="button">
                                      <span class="elementor-button-content-wrapper">
                                      <span class="elementor-button-text"><font color="#ffca2e"><?php echo $product->get_price_html() ?></font> בהזמנה אונליין</span>
                              </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </section>
                <?php endforeach; ?>
            <?php else : ?>
                <section
                        class="elementor-element elementor-element-af3ceee elementor-section-height-min-height elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                        data-id="af3ceee" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-wider" style="justify-content: center; min-height: 222px;">
                        <div class="elementor-row" style="justify-content: center;">
                            <div
                                    class="elementor-element elementor-element-921d4d6 elementor-column elementor-col-33 elementor-top-column"
                                    data-id="921d4d6" data-element_type="column"
                                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        לא נמצאו רכבים
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <section
                    class="loader-custom elementor-element elementor-element-af3ceee elementor-section-height-min-height elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                    data-id="af3ceeedd" data-element_type="section"
                    style="display: none;"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-wider" style="justify-content: center; min-height: 222px;">
                    <div class="elementor-row" style="justify-content: center;">
                        <div
                                class="elementor-element elementor-element-921d4d6 elementor-column elementor-col-33 elementor-top-column"
                                data-id="921d4d6" data-element_type="column"
                                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                            <div class="elementor-column-wrap  elementor-element-populated" style="background-color: transparent; box-shadow: unset;">
                                <div class="elementor-widget-wrap">
                                    <img style="width: 80px;" src="<?php echo get_stylesheet_directory_uri() . '/assets/loader/load.svg' ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			<div style="height: 36px;">&nbsp;</div>
            <?php /* <section
                    class="elementor-element elementor-element-65c33fa elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                    data-id="65c33fa" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-no">
                    <div class="elementor-row">
                        <div
                                class="elementor-element elementor-element-f122518 elementor-column elementor-col-100 elementor-top-column"
                                data-id="f122518" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                            class="elementor-element elementor-element-04a94b2 elementor--h-position-center elementor--v-position-middle elementor-pagination-position-inside elementor-widget elementor-widget-slides"
                                            data-id="04a94b2" data-element_type="widget"
                                            data-settings="{&quot;navigation&quot;:&quot;dots&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}"
                                            data-widget_type="slides.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-swiper">
                                                <div
                                                        class="elementor-slides-wrapper elementor-main-swiper swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-rtl"
                                                        dir="rtl" data-animation="fadeInUp" style="cursor: grab;">
                                                    <div class="swiper-wrapper elementor-slides"
                                                         style="transition-duration: 0ms; transform: translate3d(7665px, 0px, 0px);">
                                                        <div class="elementor-repeater-item-5337c20 swiper-slide swiper-slide-duplicate"
                                                             data-swiper-slide-index="0" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                                class="elementor-repeater-item-8119dd9 swiper-slide swiper-slide-duplicate swiper-slide-duplicate-prev"
                                                                data-swiper-slide-index="1" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                                class="elementor-repeater-item-4a99c7d swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active"
                                                                data-swiper-slide-index="2" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                                class="elementor-repeater-item-326bdb7 swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next"
                                                                data-swiper-slide-index="3" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-repeater-item-15495f5 swiper-slide swiper-slide-duplicate"
                                                             data-swiper-slide-index="4" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-repeater-item-5337c20 swiper-slide" data-swiper-slide-index="0"
                                                             style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-repeater-item-8119dd9 swiper-slide swiper-slide-prev"
                                                             data-swiper-slide-index="1" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-repeater-item-4a99c7d swiper-slide swiper-slide-active"
                                                             data-swiper-slide-index="2" style="width: 1095px;">
                                                            <div
                                                                    class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out elementor-ken-burns--active"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-repeater-item-326bdb7 swiper-slide swiper-slide-next"
                                                             data-swiper-slide-index="3" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-repeater-item-15495f5 swiper-slide" data-swiper-slide-index="4"
                                                             style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-repeater-item-5337c20 swiper-slide swiper-slide-duplicate"
                                                             data-swiper-slide-index="0" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                                class="elementor-repeater-item-8119dd9 swiper-slide swiper-slide-duplicate swiper-slide-duplicate-prev"
                                                                data-swiper-slide-index="1" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                                class="elementor-repeater-item-4a99c7d swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active"
                                                                data-swiper-slide-index="2" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                                class="elementor-repeater-item-326bdb7 swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next"
                                                                data-swiper-slide-index="3" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-repeater-item-15495f5 swiper-slide swiper-slide-duplicate"
                                                             data-swiper-slide-index="4" style="width: 1095px;">
                                                            <div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div>
                                                            <div class="elementor-background-overlay"></div>
                                                            <div class="swiper-slide-inner">
                                                                <div class="swiper-slide-contents animated fadeInUp" style="display: block;">
                                                                    <div class="elementor-slide-heading">רכבי אפס קילומטר</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span
                                                                class="swiper-pagination-bullet" tabindex="0" role="button"
                                                                aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0"
                                                                                                        role="button" aria-label="Go to slide 2"></span><span
                                                                class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0"
                                                                role="button" aria-label="Go to slide 3"></span><span class="swiper-pagination-bullet"
                                                                                                                      tabindex="0" role="button"
                                                                                                                      aria-label="Go to slide 4"></span><span
                                                                class="swiper-pagination-bullet" tabindex="0" role="button"
                                                                aria-label="Go to slide 5"></span></div>
                                                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> */ ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
