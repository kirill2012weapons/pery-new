<?php
$product = wc_get_product(get_the_ID());
?>

<style>
    input[disabled] {
        background-color: #dddddd !important;
    }

    .position-relative{
        position: relative;
    }
    .green-round{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        margin-top: 0 !important;
        color: #009889 !important;
        font-weight: 700 !important;
    }

    .mb-10{
        margin-bottom: 10px !important;
    }
    .mb-0{
        margin-bottom: 0 !important;
    }
    .pl-10{
        padding-left: 10px;
    }
    .pr-10{
        padding-right: 10px;
    }
    .pr-3{
        padding-right: 3px;
    }
    .icon-input{
        position: absolute;
        max-width: 15px !important;
        top: 50%;
        transform: translateY(-50%);
        left: 15px;
    }
    .icon-input_full{
        left: 20px;
    }

    .is-right{
        display: flex;
        justify-content: flex-end;
    }
    .cust-checkbox {
        cursor: pointer;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;}
    .cust-checkbox svg {
        border: 1px solid #e0e0e0;
        border-radius: 2px;
        height: 22px;
        min-width: 22px;
        background-color: #ffffff;
        margin-left: 15px; }
    .cust-checkbox svg path {
        fill: #fff;
        -webkit-transition: all 0.2s;
        transition: all 0.2s; }
    .cust-checkbox span {
        margin-bottom: 3px; }
    .cust-checkbox input {
        display: none; }
    .cust-checkbox input:checked + svg path {
        fill: #019687; }



    .checkbox-dropdown {
        width: 350px;
        border: 1px solid #d4d4d4;
        padding: 10px;
        position: relative;
        margin: 0 auto;
        cursor: pointer;

        user-select: none;
    }
    .checkbox-dropdown_form{
        width: 100%;
        height: 40px;
        margin-right: 0 !important;
        color: #818a91;
        font-weight: 300;
        line-height: 1;
    }
    /* Display CSS arrow to the right of the dropdown text */
    .checkbox-dropdown:after {
        content:'';
        height: 0;
        position: absolute;
        width: 0;
        border: 6px solid transparent;
        border-top-color: #000;
        top: 50%;
        left: 10px;
        margin-top: -3px;
    }

    /* Reverse the CSS arrow when the dropdown is active */
    .checkbox-dropdown.is-active:after {
        border-bottom-color: #000;
        border-top-color: #fff;
        margin-top: -9px;
    }

    .checkbox-dropdown-list {
        list-style: none;
        margin: 0;
        padding: 0;
        position: absolute;
        top: 100%; /* align the dropdown right below the dropdown text */
        border: inherit;
        border-top: none;
        left: -1px; /* align the dropdown to the left */
        right: -1px; /* align the dropdown to the right */
        opacity: 0; /* hide the dropdown */
        overflow: hidden;
        overflow-x: hidden;
        pointer-events: none; /* avoid mouse click events inside the dropdown */
        z-index: 11;
        background-color: #fff;
    }
    .is-active .checkbox-dropdown-list {
        opacity: 1; /* display the dropdown */
        pointer-events: auto; /* make sure that the user still can select checkboxes */
    }

    .checkbox-dropdown-list li label {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 10px;

        transition: all 0.2s ease-out;
    }
    .checkbox-dropdown-list li label input {
        margin-left: 10px;
    }
    .checkbox-dropdown-list li label:hover {
        background-color: transparent;
        /*color: white;*/
    }

    .br-hid-p-br {
        margin-top: 35px;
        /*margin-bottom: 25px;*/
    }
    .sl-pad{
        padding: 10px 0 !important;
    }
    .sl-img{
        padding-right: 10px;
    }
    .sl-img:first-child{
        padding-right: 0;
    }
    .pad-0{
        padding: 0 !important;
    }
    .badge-width{
        max-width: 80%;
        margin-right: auto !important;
    }
    .green-txt{
        color: #009889;
        margin-right: 2px;
        font-weight: 700;
    }
    .label-radio {
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        cursor: pointer;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        height: 42px;
    }
    .label-radio input {
        display: none;
    }
    .label-radio .check {
        margin-top: 1px;
        margin-left: 10px;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        height: 16px;
        width: 16px;
        min-width: 16px;
        border: 1px solid #dbdde7;
        border-radius: 50%;
    }
    .label-radio .check:before {
        content: '';
        opacity: 0;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: #019687;
        -webkit-transition: all 0.3s;
        -o-transition: all 0.3s;
        transition: all 0.3s;
    }
    .label-radio input:checked + .check {
        border-color: #019687;
    }
    .label-radio input:checked + .check:before {
        opacity: 1;
    }
    .k-class{
        padding-bottom: 30px;
    }
    @media (min-width: 768px) {
        .elementor-347 .elementor-element.elementor-element-ba48cb8,
        .elementor-347 .elementor-element.elementor-element-d942493{
            width: 50% !important;
        }
    }
    @media (max-width: 767px) {
        .elementor-element-3d93c29{
            text-align: center !important;
        }

        .elementor-column {
            width: 100%;
        }
        .mb-m {
            margin-bottom: 5px !important;
        }
        .p-r-m-0 {
            padding-right: 0 !important;
        }
        .checkbox-dropdown {
            width: 100%
        }
        .elementor-347 .elementor-element.elementor-element-127042c{
            margin-top: -40px;
        }
        .cent-mob{
            display: flex;
            justify-content: center;
        }
        .elementor-16 .elementor-element.elementor-element-229dd11 .elementor-field-group{
            padding-right: 0 !important;
            margin-bottom: 10px !important;
        }

        .d-flex{
            display: flex;
            flex-direction: column;
        }
        .elementor-element-127042c{
            max-height: 100% !important;
        }
        .elementor-72 .elementor-element.elementor-element-c5b634e:not(.elementor-motion-effects-element-type-background), .elementor-72 .elementor-element.elementor-element-c5b634e > .elementor-motion-effects-container > .elementor-motion-effects-layer{
            margin-top: 0 !important;
        }

        .whatsapp-img {
            vertical-align: top !important;
            max-height: 40px !important;
            overflow: auto !important;
            margin-left: 5px !important;
        }

        .elementor-field-group.elementor-column.elementor-field-type-submit.elementor-col-25 {
            padding-left: 0 !important;
            display: flex !important;
            align-items: center !important;
        }

        .elementor-button.elementor-size-sm {
            display: block !important;
            flex: 1 !important;
        }
    }

    [data-id="1043bbb"] {
        display: none;
    }

    .elementor-347 .elementor-element.elementor-element-127042c > .elementor-container {
        box-shadow: 0px 8px 14px 0px rgba(212,212,212,0.3) !important;
    }
</style>

<script>
    jQuery(document).ready(function () {
        jQuery(".checkbox-dropdown").click(function () {
            jQuery(this).toggleClass("is-active");
        });

        jQuery(document).click(function (e) {
            if (!e.target.closest('.checkbox-dropdown')) jQuery(".checkbox-dropdown").removeClass("is-active");
        });

        jQuery(".checkbox-dropdown ul").click(function(e) {
            e.stopPropagation();
        });


        jQuery('#mail-product-form').off();
        jQuery('#mail-product-form').off().on('submit', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            f_drop = [];

            jQuery('input[name="f_drop[]"]:checked').each(function () {
                f_drop.push(jQuery(this).val());
            });

            var data = {
                action: 'send_email',
                product_id: <?php echo $product->get_id(); ?>,
                f_name: jQuery('input[name="f_name"]').val(),
                f_phone: jQuery('input[name="f_phone"]').val(),
                f_email: jQuery('input[name="f_email"]').val(),
                f_part: jQuery('input[name="f_part"]').val(),
                f_age: jQuery('input[name="f_age"]').val(),
                f_d_to: jQuery('input[name="f_d_to"]').val(),
                f_d_from: jQuery('input[name="f_d_from"]').val(),
                f_d_extras: jQuery('input.extra-kk:checked').serializeArray(),
                f_d_period: jQuery('input.period-kk:checked').serializeArray(),
                f_drop: f_drop,
            };
            jQuery.post( myajaxcustom.url, data, function(response) {
                response = JSON.parse(response);
                if (response.success) {
                    jQuery('#mail-product-form').slideUp();
                    jQuery('#mail-product-form-wrap').append('<div style="justify-content: center;\n' +
                        '    display: flex;"><div style="max-width: 95%;" class="elementor-column-wrap  elementor-element-populated">\n' +
                        '                            <div class="elementor-widget-wrap">\n' +
                        '                              <div class="elementor-element elementor-element-df2a39d elementor-widget elementor-widget-heading" data-id="df2a39d" data-element_type="widget" data-widget_type="heading.default">\n' +
                        '                                <div class="elementor-widget-container c-notif">\n' +
                        '                                  <h2 class="elementor-heading-title elementor-size-default c-notif-t">תודה רבה, נציגנו יחזור אליכם בהקדם</h2>\n' +
                        '                                </div>\n' +
                        '                              </div>\n' +
                        '                            </div>\n' +
                        '                          </div></div>');
                } else {
                    jQuery('#mail-product-form').slideUp();
                    jQuery('#mail-product-form-wrap').append('<div style="justify-content: center;\n' +
                        '    display: flex;"><div style="max-width: 95%;" ss="elementor-column-wrap  elementor-element-populated">\n' +
                        '                            <div class="elementor-widget-wrap">\n' +
                        '                              <div class="elementor-element elementor-element-df2a39d elementor-widget elementor-widget-heading" data-id="df2a39d" data-element_type="widget" data-widget_type="heading.default">\n' +
                        '                                <div style="background-color: #ffdbd7;" class="elementor-widget-container c-notif">\n' +
                        '                                  <h2 class="elementor-heading-title elementor-size-default c-notif-t">Error</h2>\n' +
                        '                                </div>\n' +
                        '                              </div>\n' +
                        '                            </div>\n' +
                        '                          </div></div>');
                }
            });
        })
    });
</script>

<div data-elementor-type="wp-page" data-elementor-id="347" class="elementor elementor-347" data-elementor-settings="[]">
    <div class="elementor-inner">
        <div class="elementor-section-wrap">
            <section
                    class="elementor-element elementor-element-71bf663 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                    data-id="71bf663" data-element_type="section"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-background-overlay"></div>
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div
                                class="elementor-element elementor-element-f16f121 elementor-column elementor-col-100 elementor-top-column"
                                data-id="f16f121" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-c454581 elementor-widget elementor-widget-heading"
                                         data-id="c454581" data-element_type="widget" data-widget_type="heading.default">
                                        <div class="elementor-widget-container">
                                            <h2 class="elementor-heading-title elementor-size-default"><?php echo get_the_title(); ?></h2></div>
                                    </div>
                                    <div
                                            class="elementor-element elementor-element-3d93c29 elementor-mobile-align-center elementor-widget elementor-widget-button"
                                            data-id="3d93c29" data-element_type="widget" data-widget_type="button.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-button-wrapper">
                                                <a href="<?php echo home_url(); ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-angle-left"></i>			</span>
						<span class="elementor-button-text">קליק לנציג</span>
		</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="br-hid-p-br">
                <section
                        class="elementor-element elementor-element-127042c elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                        data-id="127042c" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default" style=" background-color: white; min-height: 400px !important;box-shadow: 0px 5px 19px #818891;!important;">
                        <div class="elementor-row" style="padding: 25px 25px 10px 25px;">
                            <div
                                    class="elementor-element elementor-element-ba48cb8 elementor-column elementor-col-50 elementor-top-column"
                                    data-id="ba48cb8" data-element_type="column"
                                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                    >
                                <div class="elementor-column-wrap  elementor-element-populated pad-0">
                                    <div class="elementor-widget-wrap">
                                        <div
                                                class="elementor-element elementor-element-8d49987 elementor-hidden-phone elementor-widget elementor-widget-image"
                                                data-id="8d49987" data-element_type="widget" data-widget_type="image.default" style="margin-bottom:0;">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img style="width:100% !important;height:auto !important;"
                                                         src="<?php echo get_the_post_thumbnail_url(); ?>"
                                                         class="attachment-large size-large" alt=""
                                                    ></div>
                                            </div>
                                        </div>
                                        <section
                                                class="elementor-element elementor-element-c3dd8cb elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                data-id="c3dd8cb" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <?php $attachment_ids = $product->get_gallery_image_ids(); ?>
                                                    <?php foreach ($attachment_ids as $attachment_id) : ?>
                                                        <div
                                                                class="elementor-element elementor-element-975465f elementor-column elementor-col-33 elementor-inner-column sl-img"
                                                                data-id="975465f" data-element_type="column">
                                                            <div class="elementor-column-wrap  elementor-element-populated sl-pad">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                            class="elementor-element elementor-element-0020f9e elementor-hidden-phone elementor-widget elementor-widget-image"
                                                                            data-id="0020f9e" data-element_type="widget" data-widget_type="image.default">
                                                                        <div class="elementor-widget-container ">
                                                                            <div
                                                                                    style="
                                                                                    height: 137px;
                                                                                    width: 100%;
                                                                                    background-image: url(<?php echo wp_get_attachment_url( $attachment_id ); ?>);
                                                                                    background-repeat: no-repeat;
                                                                                    background-size: cover;
                                                                                    background-position: center;"
                                                                                    class="elementor-image ">
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div
                                    class="elementor-element elementor-element-d942493 elementor-column elementor-col-50 elementor-top-column"
                                    data-id="d942493" data-element_type="column"
                                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                <div class="elementor-column-wrap  elementor-element-populated pad-0">
                                    <div class="elementor-widget-wrap">
                                        <section
                                                class="elementor-element elementor-element-739ab7c elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                data-id="739ab7c" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div
                                                            class="elementor-element elementor-element-95a4888 elementor-column elementor-col-50 elementor-inner-column"
                                                            data-id="95a4888" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                            class="elementor-element elementor-element-a5bbfc4 elementor-column elementor-col-50 elementor-inner-column"
                                                            data-id="a5bbfc4" data-element_type="column">

                                                        <?php if (get_field('promotion', get_the_ID())) : ?>
                                                            <div class="elementor-column-wrap  elementor-element-populated c-promo pad-0">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                            class="elementor-element elementor-element-df2a39d elementor-widget elementor-widget-heading"
                                                                            data-id="df2a39d" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container badge-width">
                                                                            <h2 class="elementor-heading-title elementor-size-default">משתתף במבצע החודש</h2>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div
                                                class="elementor-element elementor-element-9cf08c8 elementor-hidden-desktop elementor-hidden-tablet elementor-skin-carousel elementor-pagination-type-bullets elementor-pagination-position-outside elementor-widget elementor-widget-media-carousel"
                                                data-id="9cf08c8" data-element_type="widget"
                                                data-settings="{&quot;space_between_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;skin&quot;:&quot;carousel&quot;,&quot;effect&quot;:&quot;slide&quot;,&quot;pagination&quot;:&quot;bullets&quot;,&quot;speed&quot;:500,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;loop&quot;:&quot;yes&quot;,&quot;space_between&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;space_between_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]}}"
                                                data-widget_type="media-carousel.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-swiper">
                                                    <div
                                                            class="elementor-main-swiper swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-rtl"
                                                            style="cursor: grab;">
                                                        <div class="swiper-wrapper" style="transition-duration: 500ms;">
                                                            <?php $count = 0; ?>
                                                            <div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="<?php echo $count; $count++; ?>">
                                                                <div class="elementor-carousel-image"
                                                                     style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)">
                                                                </div>
                                                            </div>
                                                            <?php $attachment_ids = $product->get_gallery_image_ids();?>
                                                            <?php foreach ($attachment_ids as $attachment_id) : ?>
                                                                <div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="<?php echo $count; ?>">
                                                                    <div class="elementor-carousel-image"
                                                                         style="background-image: url(<?php echo wp_get_attachment_url( $attachment_id ); ?>)">
                                                                    </div>
                                                                </div>
                                                                <?php $count++; ?>
                                                            <?php endforeach; ?>
                                                            <?php $attachment_ids = $product->get_gallery_image_ids(); $count = 0;?>
                                                            <?php foreach ($attachment_ids as $attachment_id) : ?>
                                                                <div class="swiper-slide" data-swiper-slide-index="<?php echo $count; ?>">
                                                                    <div class="elementor-carousel-image"
                                                                         style="background-image: url(<?php echo wp_get_attachment_url( $attachment_id ); ?>)">
                                                                    </div>
                                                                </div>
                                                                <?php $count++; ?>
                                                            <?php endforeach; ?>
                                                            <?php $attachment_ids = $product->get_gallery_image_ids(); $count = 0;?>
                                                            <?php foreach ($attachment_ids as $attachment_id) : ?>
                                                                <div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="<?php echo $count; ?>">
                                                                    <div class="elementor-carousel-image"
                                                                         style="background-image: url(<?php echo wp_get_attachment_url( $attachment_id ); ?>)">
                                                                    </div>
                                                                </div>
                                                                <?php $count++; ?>
                                                            <?php endforeach; ?>
                                                        </div>
                                                        <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"></div>
                                                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-cf807a4 elementor-widget elementor-widget-heading"
                                             data-id="cf807a4" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default"><?php echo get_the_title(); ?></h2></div>
                                        </div>
                                        <?php /* <div class="elementor-element elementor-element-cdd102c elementor-widget elementor-widget-heading"
                                             data-id="cdd102c" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default"><?php //echo $product->get_attribute('Car Type'); ?></h2></div>
                                        </div> */ ?>

                                        <section
                                                class="elementor-element elementor-element-71ef6b6 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                data-id="71ef6b6" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row" style="flex-wrap: wrap;">
                                                    <?php
                                                    $attrs = $product->get_attributes();
                                                    ?>
                                                    <?php foreach ($attrs as $attr) : ?>
                                                        <?php
                                                        if ($attr['data']['name'] == 'Code Degem') continue;
                                                        if ($attr['data']['name'] == 'Migrash') continue;
                                                        if ($attr['data']['name'] == 'Engine Capacity') continue;
                                                        if ($attr['data']['name'] == 'Gir') continue;
                                                        if ($attr['data']['name'] == 'Engine Type') continue;
                                                        if ($attr['data']['name'] == 'Color') continue;
                                                        if ($attr['data']['name'] == 'Car Type') continue;
                                                        ?>
                                                        <div
                                                                class="elementor-element elementor-element-9946dc9 elementor-column elementor-col-25 elementor-inner-column c-align-option"
                                                                data-id="9946dc9" data-element_type="column">
                                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div
                                                                            class="elementor-element elementor-element-da0b0af elementor-widget elementor-widget-image"
                                                                            data-id="da0b0af" data-element_type="widget" data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <figure class="wp-caption position-relative">
                                                                                    <img width="38" height="38"
                                                                                         src="/wp-content/uploads/2020/02/BALL.png"
                                                                                         class="attachment-large size-large" alt="">
                                                                                    <figcaption class="widget-image-caption wp-caption-text green-round">
                                                                                        <?php
                                                                                        if ($attr['data']['name'] == 'Manufacture Year') echo 'שנה';
                                                                                        else if ($attr['data']['name'] == 'KMS') echo 'Km';
                                                                                        else if ($attr['data']['name'] == 'Hand') echo 'יד';
                                                                                        else echo $attr['data']['name'];
                                                                                        ?>
                                                                                    </figcaption>
                                                                                </figure>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                            class="elementor-element elementor-element-3266e0f elementor-widget elementor-widget-heading"
                                                                            data-id="3266e0f" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">
                                                                                <?php
                                                                                if ($attr['data']['name'] == 'Manufacture Year') echo 'שנה';
                                                                                else if ($attr['data']['name'] == 'Hand') echo $attr['data']['options'][0];
                                                                                else echo number_format($attr['data']['options'][0]);
                                                                                ?>
                                                                            </h2></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </section>
                                        <div class="elementor-element elementor-element-bb5670e elementor-widget elementor-widget-text-editor"
                                             data-id="bb5670e" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix"><p><?php echo get_the_excerpt(get_the_ID()); ?></p></div>
                                            </div>
                                        </div>
                                        <div
                                                class="elementor-element elementor-element-8442c56 elementor-align-justify elementor-hidden-phone elementor-widget elementor-widget-button"
                                                data-id="8442c56" data-element_type="widget" data-widget_type="button.default">
                                            <div class="elementor-widget-container">
                                                <?php /*
                      <div class="elementor-button-wrapper">
                        <a href="javascript:void(0);" class="elementor-button-link elementor-button elementor-size-lg" role="button">
                          <span class="elementor-button-content-wrapper">
                            <span class="elementor-button-text">
                              <font color="#ffca2e"><?php echo $product->get_price_html() ?></font> בהזמנה אונליין
                            </span>
                          </span>
                        </a>
                      </div>
					  */ ?>
                                            </div>
                                        </div>
                                        <section
                                                class="elementor-element elementor-element-1043bbb elementor-hidden-desktop elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                data-id="1043bbb" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div
                                                            class="elementor-element elementor-element-7fd90dd elementor-hidden-desktop elementor-hidden-tablet elementor-column elementor-col-50 elementor-inner-column"
                                                            data-id="7fd90dd" data-element_type="column"
                                                            data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div
                                                                        class="elementor-element elementor-element-f01caa1 elementor-view-default elementor-widget elementor-widget-icon"
                                                                        data-id="f01caa1" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fab fa-whatsapp"></i></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                            class="elementor-element elementor-element-d9a8e61 elementor-column elementor-col-50 elementor-inner-column"
                                                            data-id="d9a8e61" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div
                                                                        class="elementor-element elementor-element-3206667 elementor-align-justify elementor-widget elementor-widget-button"
                                                                        data-id="3206667" data-element_type="widget" data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <?php /*
                                  <div class="elementor-button-wrapper">
                                    <a href="#" class="elementor-button-link elementor-button elementor-size-lg"
                                       role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text"><font color="#ffca2e">₪34,000</font> בהזמנה אונליין</span>
		</span>
                                    </a>
                                  </div>
								  */ ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="elementor-16 elementor-element elementor-element-127042c elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="127042c" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default" style="background-color: white; min-height: 90px; box-shadow: 0px 5px 19px #818891;!important;">
                        <div class="elementor-row">

                            <div class="elementor-widget-wrap">
                                <div class="elementor-element elementor-element-229dd11 elementor-button-align-stretch elementor-widget elementor-widget-form" data-id="229dd11" data-element_type="widget" data-widget_type="form.default">
                                    <div class="elementor-widget-container" id="mail-product-form-wrap">

                                        <form style="padding: 0 32px 5px 32px;" class="elementor-form" id="mail-product-form" method="post" name="New Form">

                                            <div <?php if (get_field('is_show_addition_form', get_the_ID())) : ?>class="k-class"<?php endif; ?> >

                                                <?php if (get_field('is_show_addition_form', get_the_ID())) : ?>

                                                    <div class="mb-10 elementor-form-fields-wrapper">

                                                        <div style="padding-left: 0;padding-right: 0" class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-name elementor-col-25">
                                                            <div style="padding-left: 0;" class="elementor-field-type-text elementor-column elementor-field-group-name elementor-col-50 mb-0 pl-10 mb-m">
                                                                <img src="<?php echo get_template_directory_uri() . '/assets/gvn/calendar.svg'; ?>" class="icon-input" alt="">
                                                                <label for="f_d_from" class="elementor-field-label elementor-screen-only">שם מלא</label>
                                                                <input autocomplete="off"
																		type="text"
                                                                       name="f_d_from"
                                                                       id="f_d_from"
                                                                       data-language='en'
                                                                       class="datepicker-here-from elementor-field elementor-size-sm  elementor-field-textual"
                                                                       required="required"
                                                                       data-container="body"
                                                                       style="    border: 1px solid #818a91;"
                                                                       placeholder="צ’ק-אין">
                                                            </div>

                                                            <div style="padding-left: 0;" class="elementor-field-type-text elementor-column elementor-field-group-name elementor-col-50 mb-0 pr-3 mb-m p-r-m-0">
                                                                <img src="<?php echo get_template_directory_uri() . '/assets/gvn/calendar.svg'; ?>" class="icon-input" alt="">
                                                                <label for="f_d_to" class="elementor-field-label elementor-screen-only">שם מלא</label>
                                                                <input autocomplete="off"
																		type="text"
                                                                       disabled="disabled"
                                                                       name="f_d_to"
                                                                       id="f_d_to"
                                                                       data-language='en'
                                                                       class="datepicker-here-to elementor-field elementor-size-sm  elementor-field-textual"
                                                                       required="required"
                                                                       data-container="body"
                                                                       style="    border: 1px solid #818a91;"
                                                                       placeholder="צ’ק-אאט">
                                                            </div>
                                                        </div>

                                                        <div style="padding-left: 0;" class="elementor-field-type-email elementor-field-group elementor-column elementor-field-group-email elementor-col-25 elementor-field-required mb-m">
                                                            <img src="<?php echo get_template_directory_uri() . '/assets/gvn/marker.svg'; ?>" class="icon-input icon-input_full" alt="">
                                                            <label for="form-field-email" class="elementor-field-label elementor-screen-only">טלפון</label>
                                                            <input type="text"
                                                                   name="f_part"
                                                                   id="form-field-email"
                                                                   class="elementor-field elementor-size-sm  elementor-field-textual"
                                                                   placeholder="סניף לאיסוף הרכב"
                                                                   required="required"
                                                                   style="    border: 1px solid #818a91;"
                                                            >
                                                        </div>

                                                        <div style="padding-left: 0;" class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-message elementor-col-25">
                                                            <img src="<?php echo get_template_directory_uri() . '/assets/gvn/rule.svg'; ?>" class="icon-input icon-input_full" alt="">
                                                            <label for="form-field-message-f_age" class="elementor-field-label elementor-screen-only">דואר אלקטרוני</label>
                                                            <select name="f_age"
                                                                   id="form-field-message-f_age"
                                                                   data-language='en'
                                                                   class="elementor-field elementor-size-sm  elementor-field-textual"
                                                                   required="required"
                                                                   data-container="body"
                                                                   style="    border: 1px solid #818a91;">
																   <option value="" disabled selected>גיל נהג הרכב</option>
																   <?php for($i=17;$i<=120;$i++): ?>
																	<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
																	<?php endfor; ?>
															</select>
                                                        </div>

                                                    </div>

                                                <?php endif; ?>
                                                <div class="elementor-form-fields-wrapper elementor-labels- d-flex">
                                                    <?php if (have_rows('f_g_copy_extras_g')) : ?>
                                                        <div style="padding-left: 0;flex:1;" class="cust-pad elementor-field-type-text elementor-field-group elementor-column elementor-field-group-name ">
                                                            <div class="checkbox-dropdown checkbox-dropdown_form">
                                                                תוספות
                                                                <ul class="checkbox-dropdown-list">
                                                                    <?php while (have_rows('f_g_copy_extras_g')) : the_row(); ?>
                                                                    <?php $period = get_sub_field('period'); ?>
                                                                    <?php if (get_sub_field('is_default')) $default = true; ?>
                                                                        <li>
                                                                            <label class="cust-checkbox" for="extras-<?php echo get_row_index(); ?>">
                                                                                <input type="checkbox"
                                                                                       class="extra-kk"
                                                                                       <?php if (get_sub_field('is_default')) : ?>checked="checked" disabled="disabled"<?php endif; ?>
                                                                                       name="<?php echo get_sub_field('key'); ?>"
                                                                                       value="<?php echo get_sub_field('key'); ?>"
                                                                                       data-extra-daily="<?php echo $period['daily']; ?>"
                                                                                       data-extra-weekly="<?php echo $period['weekly']; ?>"
                                                                                       data-extra-monthly="<?php echo $period['monthly']; ?>"
                                                                                       data-extra-id="<?php echo get_row_index(); ?>"
                                                                                       id="extras-<?php echo get_row_index(); ?>"/>
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13">
                                                                                    <g fill="none" fill-rule="evenodd">
                                                                                        <rect width="13" height="13" fill="#FFF" rx="1"/>
                                                                                        <path fill="#1C1D22" fill-rule="nonzero" d="M9.36 3.67l1.046 1.027L5.6 9.505 2.593 6.5 3.67 5.41 5.6 7.32z"/>
                                                                                    </g>
                                                                                </svg><span><?php echo get_sub_field('key'); ?></span>
                                                                            </label>
                                                                        </li>
                                                                    <?php endwhile; ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>



                                                    <div style="padding-left: 0;flex:1; padding-right: 0;" class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-name ">
                                                        <img src="<?php echo get_template_directory_uri() . '/assets/gvn/user.svg'; ?>" class="icon-input icon-input_full" alt="">
                                                        <label for="f_name" class="elementor-field-label elementor-screen-only">שם מלא</label>
                                                        <input type="text"
                                                               name="f_name"
                                                               id="f_name"
                                                               class="elementor-field elementor-size-sm  elementor-field-textual"
                                                               required="required"
                                                               style="    border: 1px solid #818a91;"
                                                               placeholder="שם מלא">
                                                    </div>

                                                    <div style="padding-left: 0;flex:1;" class="elementor-field-type-email elementor-field-group elementor-column elementor-field-group-email elementor-field-required">
                                                        <img src="<?php echo get_template_directory_uri() . '/assets/gvn/phone.svg'; ?>" class="icon-input icon-input_full" alt="">
                                                        <label for="form-field-email" class="elementor-field-label elementor-screen-only">טלפון</label>
                                                        <input type="text"
                                                               name="f_phone"
                                                               id="form-field-email"
                                                               class="elementor-field elementor-size-sm  elementor-field-textual"
                                                               placeholder="טלפון ליצירת קשר"
                                                               required="required"
                                                               style="    border: 1px solid #818a91;"
                                                        >
                                                    </div>

                                                    <div style="padding-left: 0;flex:1;" class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-message">
                                                        <img src="<?php echo get_template_directory_uri() . '/assets/gvn/mail.svg'; ?>" class="icon-input icon-input_full" alt="">
                                                        <label for="form-field-message" class="elementor-field-label elementor-screen-only">דואר אלקטרוני</label>
                                                        <input type="email"
                                                               name="f_email"
                                                               id="form-field-message"
                                                               class="elementor-field elementor-size-sm  elementor-field-textual"
                                                               required="required"
                                                               style="    border: 1px solid #818a91;"
                                                               placeholder="דואר אלקטרוני">
                                                    </div>

                                                    <div style="padding-left: 0;" class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-25">
                                                        <?php if (wp_is_mobile()) : ?>
                                                            <a href="#" class="whatsapp-img" style="vertical-align: top;">
                                                                <img style="height: 42px; position: relative;" src="<?php echo home_url() . '/wp-content/themes/hello-elementor/assets/images/whatsapp.png' ?>" alt="">
                                                            </a>
                                                        <?php endif; ?>
                                                        <button style="display: block;" type="submit" class="elementor-button elementor-size-sm">
						                            <span>
															<span class=" elementor-button-icon">
															</span>
															<span style="margin-right: 3px;" class="elementor-button-text">ליום בהזמנה אונליין</span>
                                                            <?php if (true == $default) : ?>
                                                                <span style="color: #f6ca2d;">
                                                                    <?php echo (wc_get_product(get_the_ID()))->get_price_html() ?>
                                                                </span>
                                                            <?php else: ?>
                                                                <span style="color: #f6ca2d;"> <?php echo (wc_get_product(get_the_ID()))->get_price_html() ?></span>
                                                            <?php endif; ?>
													</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>