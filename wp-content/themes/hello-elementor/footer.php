<?php
/**
 * The template for displaying the footer.
 *
 * Contains the body & html closing tags.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) {
	get_template_part( 'template-parts/footer' );
}
?>
<?php wp_footer(); ?>
    <?php if (is_single()) : ?>
    <script>
        $ = jQuery;
            jQuery(document).ready(function () {
                $ = jQuery;
                var DateDiff = {

                    inDays: function(d1, d2) {
                        var t2 = d2.getTime();
                        var t1 = d1.getTime();

                        return parseInt((t2-t1)/(24*3600*1000));
                    },

                    inWeeks: function(d1, d2) {
                        var t2 = d2.getTime();
                        var t1 = d1.getTime();

                        return parseInt((t2-t1)/(24*3600*1000*7));
                    },

                    inMonths: function(d1, d2) {
                        var d1Y = d1.getFullYear();
                        var d2Y = d2.getFullYear();
                        var d1M = d1.getMonth();
                        var d2M = d2.getMonth();

                        return (d2M+12*d2Y)-(d1M+12*d1Y);
                    },

                    inYears: function(d1, d2) {
                        return d2.getFullYear()-d1.getFullYear();
                    }
                }

                jQuery('[data-id="a2ceeeb"]').on('click', function(e){
                    e.preventDefault();
                    location.href = '/#';
                });
                jQuery('[data-id="01d1298"]').on('click', function(e){
                    e.preventDefault();
                    location.href = '/#';
                });

                var formatter = new Intl.NumberFormat('en-US', {
                    style: 'currency',
                    currency: 'ILS',
                    minimumFractionDigits: 0
                })
                var startPrice = +jQuery('.woocommerce-Price-amount.amount').text().substr(1).replace(',', '');

                function calculatePrice()
                {
                    if ($('[name="f_d_from"]').length > 0) {

                        var startDate = $('[name="f_d_from"]').val(),
                            endDate = $('[name="f_d_to"]').val();

                        if (startDate.length == 0 || endDate.length ==0) {
                            jQuery('.woocommerce-Price-amount.amount').html('<span class="woocommerce-Price-currencySymbol">₪</span>' + formatter.format(startPrice).substr(1))
                            return;
                        }

                        var startDateObj = new Date(startDate),
                            endDateObj   = new Date(endDate);
                        var diffDays = DateDiff.inDays(startDateObj, endDateObj);

                        let sum = 0;
                        
                        if (diffDays < 7) {
                            jQuery('.extra-kk:checked').each(function (index) {
                                sum += ((+jQuery( this ).attr('data-extra-daily'))/1)*(+diffDays);
                            });
                        } else if (diffDays < 30) {
                            jQuery('.extra-kk:checked').each(function (index) {
                                sum += ((+jQuery( this ).attr('data-extra-weekly'))/7)*(+diffDays);
                            });
                        } else {
                            jQuery('.extra-kk:checked').each(function (index) {
                                sum += ((+jQuery( this ).attr('data-extra-monthly'))/30)*(+diffDays);
                            });
                        }

                        jQuery('.woocommerce-Price-amount.amount').html('<span class="woocommerce-Price-currencySymbol">₪</span>' + formatter.format(startPrice + Math.round(sum)).substr(1))
                        return;

                    } else {

                    }
                }

                jQuery('.extra-kk,[name="f_d_from"],[name="f_d_to"]').on('change', function (e) {
                    calculatePrice();
                });
                calculatePrice();

                $('#f_d_from').datepicker({
                    startDate: new Date(),
                    pick: function (selectedDate, inst) {
                        $('#f_d_to').val('');
                        $('#f_d_to').datepicker('setStartDate', selectedDate.date).removeAttr('disabled');
                    }
                });
                $('#f_d_to').datepicker();
            })
        </script>
    <?php endif; ?>
</body>
</html>
