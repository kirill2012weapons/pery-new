<?php

/**
 * Template Name: Rental
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

?>


<?php get_header(); ?>

<script>
  function disabledEventPropagation(event)
  {
    if (event.stopPropagation){
      event.stopPropagation();
    }
    else if(window.event){
      window.event.cancelBubble=true;
    }
  }

  jQuery(document).ready(function () {
    jQuery('#serch-filter-rental').find('select').on('change', function (e) {
      e.preventDefault();
      jQuery('#serch-filter-rental').off().submit();
    });
  });

  // .loader-custom - LOADER
  function checkVisible(elm) {
    var rect = elm.getBoundingClientRect();
    var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
  }

  jQuery(document).ready(function () {

    is_load_c = false;
    jQuery(window).scroll(function () {
      if (checkVisible(jQuery('[data-id="c5b634e"]')[0]) && !is_load_c) {
        jQuery.ajax({
          type: 'POST',
          url: myajaxcustom.url,
          data: {action: 'pagination_ajax'},
          beforeSend: function() {
            if (jQuery('.product-raw-c:hidden:first').length > 0) {
              jQuery('.loader-custom').show();
            }
            is_load_c = true;
          },
          success: function(data) {
            jQuery('.product-raw-c:hidden:first').slideDown();
          },
          error: function(xhr) {
            jQuery('.product-raw-c:hidden:first').slideDown();
            jQuery('.loader-custom').hide();
          },
          complete: function() {
            jQuery('.loader-custom').hide();
            is_load_c = false;
          },
        });
      }
    });
  })

</script>

<style>
  @media (max-width: 767px) {
    .elementor-column {
      width: 100%;
      padding: 12px;
    }
    .whatsapp-img{
          display: inline-flex !important;
    }
  }
  @media (min-width: 100px) and (max-width: 767px) {
      .elementor-element-c15a6a1 {
          display: none;
      }
      #serch-filter-rental .c-btn-sbm-w {
        padding: 0 !important;
      }
      .cent-mob{
          display: flex;
          justify-content: center;
      }

  }
  .elementor-72 .elementor-element.elementor-element-c5b634e:not(.elementor-motion-effects-element-type-background), .elementor-72 .elementor-element.elementor-element-c5b634e > .elementor-motion-effects-container > .elementor-motion-effects-layer {
      margin-top: 20px !important;
  }
  .whatsapp-img{
      display: none;
  }
    .green-badge{
        background-color: #DEFFFC;
        border-style: solid;
        border-width: 1px;
        border-color: #00988D;
        position: absolute;
        padding: 10px !important;
        z-index: 111;
        font-size: 16px;
        left: 0;
        color: #009889;
    }
</style>

<?php

$metaQuery = ['relation' => 'END'];

if (isset($_GET['car_type_f']) && !empty($_GET['car_type_f'])) {
    $serialized_value =
        serialize( 'name' ) .
        serialize( 'Car Type' ) .
        serialize( 'value' ) .
        serialize( $_GET['car_type_f'] )
    ;
    $metaQuery[] = [
        'key'     => '_product_attributes',
        'value'   => $serialized_value,
        'compare' => 'LIKE',
    ];
}

$productsArgs = [
    'status' => 'publish',
    'post_type' => 'product',
    'posts_per_page' => -1,
    'order'          => 'ASC',
    'meta_query' => $metaQuery,
];

$productsArgs['tax_query'] = [
    'relation' => 'AND',
    [
        'taxonomy' => 'product_cat',
        'field' => 'slug',
        'terms' => 'renting',
    ]
];

if (isset($_GET['car_price_f']) && !empty($_GET['car_price_f'])) {
    $productsArgs['orderby'] = 'meta_value_num';
    $productsArgs['meta_key'] = '_price';
    $productsArgs['order'] = $_GET['car_price_f'];
}

$productsPosts = new WP_Query($productsArgs);

foreach ($productsPosts->posts as $post) {
    $products[] = wc_get_product($post->ID);
}

?>

<div data-elementor-type="wp-page" data-elementor-id="110" class="elementor elementor-177" data-elementor-settings="[]">
  <div class="elementor-inner">
    <div class="elementor-section-wrap">
      <section class="elementor-element elementor-element-71bf663 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="71bf663" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-row">
            <div class="elementor-element elementor-element-f16f121 elementor-column elementor-col-100 elementor-top-column" data-id="f16f121" data-element_type="column">
              <div class="elementor-column-wrap  elementor-element-populated">
                <div class="elementor-widget-wrap">
                  <div class="elementor-element elementor-element-c454581 elementor-widget elementor-widget-heading" data-id="c454581" data-element_type="widget" data-widget_type="heading.default">
                    <div class="elementor-widget-container">
                      <h2 class="elementor-heading-title elementor-size-default">השכרת רכב</h2>		</div>
                  </div>
                  <div class="elementor-element elementor-element-3d93c29 elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="3d93c29" data-element_type="widget" data-widget_type="button.default">
                    <div class="elementor-widget-container">
                      <div class="elementor-button-wrapper cent-mob">
                        <a href="#" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-angle-left"></i>			</span>
						<span class="elementor-button-text">קליק לנציג</span>
		</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--            <section class="elementor-element elementor-element-d7b4344 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="d7b4344" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">-->
      <!--                <div class="elementor-container elementor-column-gap-default">-->
      <!--                    <div class="elementor-row">-->
      <!--                        <div class="elementor-element elementor-element-f6d5c69 elementor-column elementor-col-100 elementor-top-column" data-id="f6d5c69" data-element_type="column">-->
      <!--                            <div class="elementor-column-wrap  elementor-element-populated">-->
      <!--                                <div class="elementor-widget-wrap">-->
      <!--                                    <div class="elementor-element elementor-element-accd773 elementor-button-align-stretch elementor-widget elementor-widget-form" data-id="accd773" data-element_type="widget" data-widget_type="form.default">-->
      <!--                                        <div class="elementor-widget-container">-->
      <!---->
      <!--                                            <form class="elementor-form" method="post" name="New Form">-->
      <!--                                                <input type="hidden" name="post_id" value="110">-->
      <!--                                                <input type="hidden" name="form_id" value="accd773">-->
      <!---->
      <!--                                                <div class="elementor-form-fields-wrapper elementor-labels-">-->
      <!--                                                    <div class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-name elementor-col-20 elementor-sm-50">-->
      <!--                                                        <label for="form-field-name" class="elementor-field-label elementor-screen-only">צ'ק אין</label><input size="1" type="text" name="form_fields[name]" id="form-field-name" class="elementor-field elementor-size-md  elementor-field-textual" placeholder="צ'ק אין">				</div>-->
      <!--                                                    <div class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-email elementor-col-20 elementor-sm-50 elementor-field-required">-->
      <!--                                                        <label for="form-field-email" class="elementor-field-label elementor-screen-only">צ'ק אאוט</label><input size="1" type="text" name="form_fields[email]" id="form-field-email" class="elementor-field elementor-size-md  elementor-field-textual" placeholder="צ'ק אאוט" required="required" aria-required="true">				</div>-->
      <!--                                                    <div class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-field_1 elementor-col-20 elementor-field-required">-->
      <!--                                                        <label for="form-field-field_1" class="elementor-field-label elementor-screen-only">סניף לאיסוף הרכב</label><input size="1" type="text" name="form_fields[field_1]" id="form-field-field_1" class="elementor-field elementor-size-md  elementor-field-textual" placeholder="סניף לאיסוף הרכב" required="required" aria-required="true">				</div>-->
      <!--                                                    <div class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-field_2 elementor-col-20 elementor-field-required">-->
      <!--                                                        <label for="form-field-field_2" class="elementor-field-label elementor-screen-only">גיל נהג הרכב</label><input size="1" type="text" name="form_fields[field_2]" id="form-field-field_2" class="elementor-field elementor-size-md  elementor-field-textual" placeholder="גיל נהג הרכב" required="required" aria-required="true">				</div>-->
      <!--                                                    <div class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-20">-->
      <!--                                                        <button type="submit" class="elementor-button elementor-size-md">-->
      <!--						<span>-->
      <!--															<span class="elementor-align-icon-left elementor-button-icon">-->
      <!--									<i aria-hidden="true" class="fas fa-long-arrow-alt-left"></i>																	</span>-->
      <!--																						<span class="elementor-button-text">מצאו לי רכב</span>-->
      <!--													</span>-->
      <!--                                                        </button>-->
      <!--                                                    </div>-->
      <!--                                                </div>-->
      <!--                                            </form>-->
      <!---->
      <!---->
      <!--                                        </div>-->
      <!--                                    </div>-->
      <!--                                </div>-->
      <!--                            </div>-->
      <!--                        </div>-->
      <!--                    </div>-->
      <!--                </div>-->
      <!--            </section>-->
      <section class="elementor-element elementor-element-dbee2cf elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="dbee2cf" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-row">
            <div class="elementor-element elementor-element-6cf6d9d elementor-column elementor-col-100 elementor-top-column" data-id="6cf6d9d" data-element_type="column">
              <div class="elementor-column-wrap  elementor-element-populated">
                <div class="elementor-widget-wrap">
                  <div class="elementor-element elementor-element-c2d5f4b elementor-button-align-stretch elementor-widget elementor-widget-form" data-id="c2d5f4b" data-element_type="widget" data-widget_type="form.default">
                    <div class="elementor-widget-container">
                      <form id="serch-filter-rental" class="elementor-form" method="get" action="?" name="New Form">

                        <div class="elementor-form-fields-wrapper elementor-labels-">
                          <div class="elementor-field-type-select elementor-field-group elementor-column elementor-field-group-name elementor-col-20 elementor-sm-50">
                            <label for="form-field-name" class="elementor-field-label elementor-screen-only">Name</label>		<div class="elementor-field elementor-select-wrapper ">
                              <select name="car_type_f" id="form-field-name" class="elementor-field-textual elementor-size-sm">
                                <option value="<?php if (isset($_GET['car_type_f']) && !empty($_GET['car_type_f'])) echo $_GET['car_type_f']; ?>">סוג רכב</option>
                                <option value="פרטית">פרטית</option>
                                <option value="חברה">חברה</option>
                                <option value="השכרה">השכרה</option>
                                <option value="ליסינג">ליסינג</option>
                                <option value="מונית">מונית</option>
                                <option value="נהיגה לימוד">נהיגה לימוד</option>
                                <option value="ייבוא אישי">ייבוא אישי</option>
                                <option value="ממשלתי">ממשלתי</option>
                                <option value='או"ם'>או"ם</option>
                                <option value='אחר'>או"ם</option>
                              </select>
                            </div>
                          </div>

                          <div class="elementor-field-type-select elementor-field-group elementor-column elementor-field-group-email elementor-col-20 elementor-sm-50 elementor-field-required">
                            <label for="form-field-email" class="elementor-field-label elementor-screen-only">Email</label>		<div class="elementor-field elementor-select-wrapper ">
                              <select name="car_price_f" id="form-field-email" class="elementor-field-textual elementor-size-sm" required="required" aria-required="true">
                                <option value="<?php if (isset($_GET['car_price_f']) && !empty($_GET['car_price_f'])) echo $_GET['car_price_f']; ?>">סדר לפי מחיר</option>
                                <option value="DESC">מהגבוה לנמוך</option>
                                <option value="ASC">מהנמוך לגבוה</option>
                              </select>
                            </div>
                          </div>

                          <div class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-100 c-btn-sbm-w">
                            <button type="submit" class="elementor-button elementor-size-sm">
						<span>
															<span class=" elementor-button-icon">
																												<span class="elementor-screen-only">Submit</span>
																	</span>
																				</span>
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


        <?php
        $newArrayProduct = [];
        $count = 0;
        $counterr = 0;
        foreach ($products as $product) {
            if (($counterr % 3)!=0) {
                $newArrayProduct[$count][] = $product;
            } else {
                $count += 1;
                $newArrayProduct[$count][] = $product;
            }
            $counterr += 1;
        }
        ?>
        <?php $counterr = 3; ?>
        <?php if (!empty($newArrayProduct)) : ?>
            <?php $prCounter = 0; ?>
            <?php foreach ($newArrayProduct as $row => $products) : ?>
            <section
                <?php if ($prCounter > 0) echo 'style="display:none;"'; ?>
                product-attr="<?php echo $prCounter; $prCounter++; ?>"
                class="product-raw-c elementor-element elementor-element-af3ceee elementor-section-height-min-height elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                data-id="af3ceee" data-element_type="section"
                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-container elementor-column-gap-wider" style="min-height: 572px; ">
                <div class="elementor-row">
                    <?php foreach ($products as $product) : ?>
                        <?php
                        /** @var $product WC_Product */
                        //$product = wc_get_product($product->ID);
                        ?>
                      <div
                          class="elementor-element elementor-element-921d4d6 elementor-column elementor-col-33 elementor-top-column"
                          data-id="921d4d6" data-element_type="column"
                          data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-column-wrap  elementor-element-populated">
                          <div class="elementor-widget-wrap">
                              <?php if (get_field('promotion', $product->get_id())) : ?>
                                  <div class="elementor-column-wrap  elementor-element-populated c-promo" style="position: relative;">
                                      <div class="elementor-widget-wrap">
                                          <div
                                                  class="elementor-element elementor-element-df2a39d elementor-widget elementor-widget-heading"
                                                  data-id="df2a39d" data-element_type="widget" data-widget_type="heading.default">
                                              <div class="elementor-widget-container">
                                                  <h2 class="elementor-heading-title elementor-size-default green-badge">משתתף במבצע החודש</h2>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              <?php endif; ?>
                            <div
                                class="elementor-element elementor-element-887b2e0 elementor-arrows-position-inside elementor-pagination-position-outside elementor-widget elementor-widget-image-carousel"
                                data-id="887b2e0" data-element_type="widget"
                                data-settings="{&quot;slides_to_show&quot;:&quot;1&quot;,&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;effect&quot;:&quot;slide&quot;,&quot;speed&quot;:500,&quot;direction&quot;:&quot;ltr&quot;}"
                                data-widget_type="image-carousel.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-image-carousel-wrapper swiper-container" dir="ltr">
                                  <div class="elementor-image-carousel swiper-wrapper">
                                    <div class="swiper-slide">
                                        <?php if (wp_is_mobile()) : ?>
                                        <figure class="swiper-slide-inner"><a href="<?php echo get_post_permalink($product->get_id()) ?>"><img class="swiper-slide-image"
                                                                              src="<?php echo get_the_post_thumbnail_url( $product->get_id() ) ? get_the_post_thumbnail_url( $product->get_id() ) : wc_placeholder_img_src(); ?>"
                                                                              alt="" border="0"></a></figure>
                                        <?php else: ?>
                                            <figure class="swiper-slide-inner">

                                                <a
                                                        href="<?php echo get_post_permalink($product->get_id()) ?>">
                                                    <div
                                                            style="
                                                                    height: 215px;
                                                                    width: 100%;
                                                                    background-image: url(<?php echo get_the_post_thumbnail_url( $product->get_id() ); ?>);
                                                                    background-repeat: no-repeat;
                                                                    background-position: center;
                                                                    background-size: cover;"></div>

                                                </a>
                                            </figure>
                                        <?php endif; ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-97d6425 elementor-widget elementor-widget-heading"
                                 data-id="97d6425" data-element_type="widget" data-widget_type="heading.default">
                              <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_title(); ?></h2></div>
                            </div>
                            <div class="elementor-element elementor-element-d8aeb32 elementor-widget elementor-widget-heading"
                                 data-id="d8aeb32" data-element_type="widget" data-widget_type="heading.default">
                              <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_attribute('Car Type'); ?></h2></div>
                            </div>
                            <section
                                class="elementor-element elementor-element-71ef6b6 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                data-id="71ef6b6" data-element_type="section">
                              <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <?php if ($product->get_attribute('Manufacture Year')) : ?>
                                      <div
                                          class="elementor-element elementor-element-9946dc9 elementor-column elementor-col-33 elementor-inner-column c-my-k"
                                          data-id="9946dc9" data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                          <div class="elementor-widget-wrap">
                                            <div
                                                class="elementor-element elementor-element-da0b0af elementor-widget elementor-widget-image"
                                                data-id="da0b0af" data-element_type="widget" data-widget_type="image.default">
                                              <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                  <figure class="wp-caption position-relative">
                                                    <img width="45" height="45"
                                                         src="/wp-content/uploads/2020/02/BALL.png"
                                                         class="attachment-large size-large" alt="">
                                                    <figcaption style="line-height: 4.1em;" class="widget-image-caption wp-caption-text green-round">שנה</figcaption>
                                                  </figure>
                                                </div>
                                              </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-3266e0f elementor-widget elementor-widget-heading"
                                                data-id="3266e0f" data-element_type="widget" data-widget_type="heading.default">
                                              <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_attribute('Manufacture Year') ?></h2></div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    <?php endif; ?>
                                    <?php if ($product->get_attribute('KMS')) : ?>
                                      <div
                                          class="elementor-element elementor-element-9946dc9 elementor-column elementor-col-33 elementor-inner-column c-kms-k"
                                          data-id="9946dc9" data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                          <div class="elementor-widget-wrap">
                                            <div
                                                class="elementor-element elementor-element-da0b0af elementor-widget elementor-widget-image"
                                                data-id="da0b0af" data-element_type="widget" data-widget_type="image.default">
                                              <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                  <figure class="wp-caption position-relative">
                                                    <img width="38" height="38"
                                                         src="/wp-content/uploads/2020/02/BALL.png"
                                                         class="attachment-large size-large" alt="">
                                                    <figcaption class="widget-image-caption wp-caption-text c-km green-round">KM</figcaption>
                                                  </figure>
                                                </div>
                                              </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-3266e0f elementor-widget elementor-widget-heading"
                                                data-id="3266e0f" data-element_type="widget" data-widget_type="heading.default">
                                              <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default"><?php echo number_format($product->get_attribute('KMS')) ?></h2></div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    <?php endif; ?>
                                    <?php if ($product->get_attribute('Hand')) : ?>
                                      <div class="elementor-element elementor-element-8fe2c70 elementor-column elementor-col-33 elementor-inner-column c-h-k" data-id="8fe2c70" data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                          <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-7880ab5 elementor-widget elementor-widget-image" data-id="7880ab5" data-element_type="widget" data-widget_type="image.default">
                                              <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                  <figure class="wp-caption position-relative">
                                                    <img width="38" height="38" src="/wp-content/uploads/2020/02/BALL.png" class="attachment-large size-large" alt="">											<figcaption class="widget-image-caption wp-caption-text green-round">יד</figcaption>
                                                  </figure>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="elementor-element elementor-element-2d1533b elementor-widget elementor-widget-heading" data-id="2d1533b" data-element_type="widget" data-widget_type="heading.default">
                                              <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_attribute('Hand') ?></h2>		</div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    <?php endif; ?>
                                </div>
                              </div>
                            </section>
                            <div class="elementor-element elementor-element-3e7edd4 elementor-widget elementor-widget-text-editor"
                                 data-id="3e7edd4" data-element_type="widget" data-widget_type="text-editor.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-text-editor elementor-clearfix"><p><?php echo get_the_excerpt($product->get_id()); ?></p></div>
                              </div>
                            </div>
                            <div
                                class="elementor-element elementor-element-32bd69a elementor-align-justify elementor-widget elementor-widget-button"
                                <?php if (!wp_is_mobile()) : ?>
                                style="position: absolute; bottom: 0;"
                                <?php endif; ?>
                                data-id="32bd69a" data-element_type="widget" data-widget_type="button.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-button-wrapper">
                                        <?php if (wp_is_mobile()) : ?>
                                            <a href="#" class="whatsapp-img" style="vertical-align: top;">
                                                <img style="height: 42px; position: relative;" src="<?php echo home_url() . '/wp-content/themes/hello-elementor/assets/images/whatsapp.png' ?>" alt="">
                                            </a>
                                        <?php endif; ?>
                                        <a
                                            <?php if (wp_is_mobile()) { echo 'style="width: 274px;"'; } ?>
                                                href="<?php echo get_post_permalink($product->get_id()) ?>" class="elementor-button-link elementor-button elementor-size-lg" role="button">
                                      <span class="elementor-button-content-wrapper">
                                      <span class="elementor-button-text"><font color="#ffca2e"><?php echo $product->get_price_html() ?></font> בהזמנה אונליין</span>
                              </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                </div>
              </div>
            </section>
            <?php endforeach; ?>
        <?php else : ?>
          <section
              class="elementor-element elementor-element-af3ceee elementor-section-height-min-height elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
              data-id="af3ceee" data-element_type="section"
              data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-container elementor-column-gap-wider" style="justify-content: center; min-height: 222px;">
              <div class="elementor-row" style="justify-content: center;">
                <div
                    class="elementor-element elementor-element-921d4d6 elementor-column elementor-col-33 elementor-top-column"
                    data-id="921d4d6" data-element_type="column"
                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                      No products
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        <?php endif; ?>
      <section
          class="loader-custom elementor-element elementor-element-af3ceee elementor-section-height-min-height elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
          data-id="af3ceeedd" data-element_type="section"
          style="display: none;"
          data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-wider" style="justify-content: center; min-height: 222px;">
          <div class="elementor-row" style="justify-content: center;">
            <div
                class="elementor-element elementor-element-921d4d6 elementor-column elementor-col-33 elementor-top-column"
                data-id="921d4d6" data-element_type="column"
                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-column-wrap  elementor-element-populated" style="background-color: transparent; box-shadow: unset;">
                <div class="elementor-widget-wrap">
                  <img style="width: 80px;" src="<?php echo get_stylesheet_directory_uri() . '/assets/loader/load.svg' ?>" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>


<?php get_footer(); ?>
