<?php

/**
 * Template Name: Luxury
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

?>


<?php get_header(); ?>

<?php

$productsPosts = get_field('luxury', 'options');

foreach ($productsPosts as $post) {
    $products[] = wc_get_product($post->ID);
}

?>

<style>

    .elementor-element-b57f61a .elementor-element-populated {
        padding: 0px !important;
    }
</style>

<div data-elementor-type="wp-page" data-elementor-id="134" class="elementor elementor-134" data-elementor-settings="[]">
    <div class="elementor-inner">
        <div class="elementor-section-wrap">
            <section class="elementor-element elementor-element-71bf663 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="71bf663" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-background-overlay"></div>
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div class="elementor-element elementor-element-f16f121 elementor-column elementor-col-100 elementor-top-column" data-id="f16f121" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-c454581 elementor-widget elementor-widget-heading" data-id="c454581" data-element_type="widget" data-widget_type="heading.default">
                                        <div class="elementor-widget-container">
                                            <h2 class="elementor-heading-title elementor-size-default">רכבי יוקרה</h2>		</div>
                                    </div>
                                    <div class="elementor-element elementor-element-3d93c29 elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="3d93c29" data-element_type="widget" data-widget_type="button.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-button-wrapper">
                                                <a href="<?php echo home_url(); ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-angle-left"></i>			</span>
						<span class="elementor-button-text">קליק לנציג</span>
		</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="elementor-element elementor-element-8eef801 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="8eef801" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">

                        <?php foreach ($products as $product) : ?>

                            <div class="elementor-element elementor-element-82e1d8b elementor-hidden-phone elementor-column elementor-col-25 elementor-top-column" data-id="82e1d8b" data-element_type="column">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-element elementor-element-635b673 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="635b673" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-element elementor-element-86ef60c elementor-column elementor-col-100 elementor-inner-column" data-id="86ef60c" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-1140ffe elementor-widget elementor-widget-image" data-id="1140ffe" data-element_type="widget" data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img style="height: 167px;" width="640" height="480" src="<?php echo get_the_post_thumbnail_url( $product->get_id() ); ?>" class="attachment-large size-large" alt="" >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="margin-top: 25px;" class="elementor-element elementor-element-3ec9135 elementor-widget elementor-widget-heading" data-id="3ec9135" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_title(); ?></h2>		</div>
                                                                </div>
                                                                <?php if ($product->get_attribute('Manufacture Year')) : ?>
                                                                <div class="elementor-element elementor-element-0ca9884 elementor-widget elementor-widget-heading" data-id="0ca9884" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default">מודל <?php echo $product->get_attribute('Manufacture Year') ?></h2>
                                                                    </div>
                                                                </div>
                                                                <?php endif; ?>
                                                                <div class="elementor-element elementor-element-113b8e4 elementor-align-justify elementor-widget elementor-widget-button" data-id="113b8e4" data-element_type="widget" data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="<?php echo get_post_permalink($product->get_id()) ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                            <span class="elementor-button-content-wrapper">
                            <span class="elementor-button-text">לפרטים נוספים</span>
            </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; ?>

                    </div>
                </div>
            </section>
            <section class="elementor-element elementor-element-5a20a16 elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="5a20a16" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div class="elementor-element elementor-element-7dcfc90 elementor-column elementor-col-100 elementor-top-column" data-id="7dcfc90" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-ea7e823 elementor-button-align-stretch elementor-widget elementor-widget-form" data-id="ea7e823" data-element_type="widget" data-widget_type="form.default">
                                        <div class="elementor-widget-container">
                                            <form class="elementor-form" method="post" name="New Form">
                                                <input type="hidden" name="post_id" value="134">
                                                <input type="hidden" name="form_id" value="ea7e823">

                                                <div class="elementor-form-fields-wrapper elementor-labels-">
                                                    <div class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-field_1 elementor-col-100 elementor-sm-100">
                                                        <label for="form-field-field_1" class="elementor-field-label elementor-screen-only">שם מלא</label><input size="1" type="text" name="form_fields[field_1]" id="form-field-field_1" class="elementor-field elementor-size-sm  elementor-field-textual" placeholder="שם מלא">				</div>
                                                    <div class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-name elementor-col-100 elementor-sm-100">
                                                        <label for="form-field-name" class="elementor-field-label elementor-screen-only">טלפון ליצירת קשר</label><input size="1" type="text" name="form_fields[name]" id="form-field-name" class="elementor-field elementor-size-sm  elementor-field-textual" placeholder="טלפון ליצירת קשר">				</div>
                                                    <div class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-email elementor-col-100 elementor-sm-100 elementor-field-required">
                                                        <label for="form-field-email" class="elementor-field-label elementor-screen-only">דואר אלקטרוני</label><input size="1" type="text" name="form_fields[email]" id="form-field-email" class="elementor-field elementor-size-sm  elementor-field-textual" placeholder="דואר אלקטרוני" required="required" aria-required="true">				</div>
                                                    <div class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-100">
                                                        <button type="submit" class="elementor-button elementor-size-sm">
						<span>
															<span class=" elementor-button-icon">
																										</span>
																						<span class="elementor-button-text">קבל הצעה</span>
													</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="elementor-element elementor-element-d1644fd elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="d1644fd" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div class="elementor-element elementor-element-b57f61a elementor-column elementor-col-100 elementor-top-column" data-id="b57f61a" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-e0478a0 elementor--h-position-center elementor--v-position-middle elementor-pagination-position-inside elementor-widget elementor-widget-slides" data-id="e0478a0" data-element_type="widget" data-settings="{&quot;navigation&quot;:&quot;dots&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}" data-widget_type="slides.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-swiper">
                                                <div class="elementor-slides-wrapper elementor-main-swiper swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-rtl" dir="rtl" data-animation="fadeInUp" style="cursor: grab;">
                                                    <div class="swiper-wrapper elementor-slides" style="transition-duration: 0ms; transform: translate3d(4400px, 0px, 0px);"><div class="elementor-repeater-item-5337c20 swiper-slide swiper-slide-duplicate swiper-slide-duplicate-prev" data-swiper-slide-index="0" style="width: 1100px;"><div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div><div class="elementor-background-overlay"></div><div class="swiper-slide-inner"><div class="swiper-slide-contents animated fadeInUp" style="display: block;"><div class="elementor-slide-heading">הרכב המיוחד ליום המיוחד</div></div></div></div><div class="elementor-repeater-item-edb402e swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="1" style="width: 1100px;"><div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div><div class="elementor-background-overlay"></div><div class="swiper-slide-inner"><div class="swiper-slide-contents animated fadeInUp" style="display: block;"><div class="elementor-slide-heading">הרכב המיוחד ליום המיוחד</div></div></div></div><div class="elementor-repeater-item-63e0562 swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="2" style="width: 1100px;"><div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div><div class="elementor-background-overlay"></div><div class="swiper-slide-inner"><div class="swiper-slide-contents animated fadeInUp" style="display: block;"><div class="elementor-slide-heading">הרכב המיוחד ליום המיוחד</div></div></div></div>
                                                        <div class="elementor-repeater-item-5337c20 swiper-slide swiper-slide-prev" data-swiper-slide-index="0" style="width: 1100px;"><div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div><div class="elementor-background-overlay"></div><div class="swiper-slide-inner"><div class="swiper-slide-contents animated fadeInUp" style="display: block;"><div class="elementor-slide-heading">הרכב המיוחד ליום המיוחד</div></div></div></div><div class="elementor-repeater-item-edb402e swiper-slide swiper-slide-active" data-swiper-slide-index="1" style="width: 1100px;"><div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out elementor-ken-burns--active"></div><div class="elementor-background-overlay"></div><div class="swiper-slide-inner"><div class="swiper-slide-contents animated fadeInUp" style="display: block;"><div class="elementor-slide-heading">הרכב המיוחד ליום המיוחד</div></div></div></div><div class="elementor-repeater-item-63e0562 swiper-slide swiper-slide-next" data-swiper-slide-index="2" style="width: 1100px;"><div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div><div class="elementor-background-overlay"></div><div class="swiper-slide-inner"><div class="swiper-slide-contents animated fadeInUp" style="display: block;"><div class="elementor-slide-heading">הרכב המיוחד ליום המיוחד</div></div></div></div>				<div class="elementor-repeater-item-5337c20 swiper-slide swiper-slide-duplicate swiper-slide-duplicate-prev" data-swiper-slide-index="0" style="width: 1100px;"><div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div><div class="elementor-background-overlay"></div><div class="swiper-slide-inner"><div class="swiper-slide-contents animated fadeInUp" style="display: block;"><div class="elementor-slide-heading">הרכב המיוחד ליום המיוחד</div></div></div></div><div class="elementor-repeater-item-edb402e swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="1" style="width: 1100px;"><div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div><div class="elementor-background-overlay"></div><div class="swiper-slide-inner"><div class="swiper-slide-contents animated fadeInUp" style="display: block;"><div class="elementor-slide-heading">הרכב המיוחד ליום המיוחד</div></div></div></div><div class="elementor-repeater-item-63e0562 swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="2" style="width: 1100px;"><div class="swiper-slide-bg elementor-ken-burns elementor-ken-burns--out"></div><div class="elementor-background-overlay"></div><div class="swiper-slide-inner"><div class="swiper-slide-contents animated fadeInUp" style="display: block;"><div class="elementor-slide-heading">הרכב המיוחד ליום המיוחד</div></div></div></div></div>
                                                    <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span></div>
                                                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php get_footer(); ?>
