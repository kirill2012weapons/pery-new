<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section, opens the <body> tag and adds the site's header.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
?>
    <!doctype html>
<html <?php language_attributes(); ?> style="position: relative;">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <?php $viewport_content = apply_filters( 'hello_elementor_viewport_content', 'width=device-width, initial-scale=1' ); ?>
        <meta name="viewport" content="<?php echo esc_attr( $viewport_content ); ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <style>
            body {
                position: relative!important;
            }
          .menu-item-276 a span {
            padding-left: 95px !important;
          }
          .menu-item-269 a span {
            padding-left: 95px !important;
          }
          .menu-item-271 a span {
            padding-left: 95px !important;
          }
            .elementor-347 .elementor-element.elementor-element-3d93c29 .elementor-button {
                border-width: 2px !important;
            }
            .elementor-177 .elementor-element.elementor-element-3d93c29 .elementor-button {
                border-width: 2px !important;
            }
            .elementor-134 .elementor-element.elementor-element-3d93c29 .elementor-button {
                border-width: 2px !important;
            }
            .elementor-36 .elementor-element.elementor-element-3d93c29 .elementor-button {
                border-width: 2px !important;
            }
            .elementor-252 .elementor-element.elementor-element-3d93c29 .elementor-button {
                border-width: 2px !important;
            }
            .elementor-16 .elementor-element.elementor-element-2a501a4 > .elementor-element-populated{
                padding: 10px !important;
            }
            .elementor-9 .elementor-element.elementor-element-b4cb193 {
                margin-bottom: -163px !important;
            }
            .elementor.elementor-9.elementor-location-header {
                background-color: #000 !important;
            }

            .page-id-190 .elementor-element-01d1298,
            .page-id-190 .elementor-element-a2ceeeb{
                cursor: pointer;
            }

            .page-id-190 .elementor-element-01d1298:hover .elementor-element-populated,
            .page-id-190 .elementor-element-a2ceeeb:hover .elementor-element-populated{
                cursor: pointer;
                background-color: #191919 !important;
            }
            .elementor-16 .elementor-element.elementor-element-229dd11 .elementor-field-group{
                margin-bottom: 0 !important;
            }
            @media (min-width: 1500px) {
                .page-id-190 .elementor-190 .elementor-element.elementor-element-a236151 > .elementor-container{
                    max-width: 1149px!important;
                }
            }
            @media (max-width: 767px) {
                .elementor-16 .elementor-element.elementor-element-321bbf2 {
                    margin-top: -210px !important;
                }
            }

        </style>
        <?php wp_head(); ?>

    </head>
<body <?php body_class(); ?>>

<?php
hello_elementor_body_open();

if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'header' ) ) {
    get_template_part( 'template-parts/header' );
}
