<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

define( 'HELLO_ELEMENTOR_VERSION', '2.2.0' );

if ( ! isset( $content_width ) ) {
    $content_width = 800; // Pixels.
}

if ( ! function_exists( 'hello_elementor_setup' ) ) {
    /**
     * Set up theme support.
     *
     * @return void
     */
    function hello_elementor_setup() {
        $hook_result = apply_filters_deprecated( 'elementor_hello_theme_load_textdomain', [ true ], '2.0', 'hello_elementor_load_textdomain' );
        if ( apply_filters( 'hello_elementor_load_textdomain', $hook_result ) ) {
            load_theme_textdomain( 'hello-elementor', get_template_directory() . '/languages' );
        }

        $hook_result = apply_filters_deprecated( 'elementor_hello_theme_register_menus', [ true ], '2.0', 'hello_elementor_register_menus' );
        if ( apply_filters( 'hello_elementor_register_menus', $hook_result ) ) {
            register_nav_menus( array( 'menu-1' => __( 'Primary', 'hello-elementor' ) ) );
        }

        $hook_result = apply_filters_deprecated( 'elementor_hello_theme_add_theme_support', [ true ], '2.0', 'hello_elementor_add_theme_support' );
        if ( apply_filters( 'hello_elementor_add_theme_support', $hook_result ) ) {
            add_theme_support( 'post-thumbnails' );
            add_theme_support( 'automatic-feed-links' );
            add_theme_support( 'title-tag' );
            add_theme_support(
                'html5',
                array(
                    'search-form',
                    'comment-form',
                    'comment-list',
                    'gallery',
                    'caption',
                )
            );
            add_theme_support(
                'custom-logo',
                array(
                    'height'      => 100,
                    'width'       => 350,
                    'flex-height' => true,
                    'flex-width'  => true,
                )
            );

            /*
             * Editor Style.
             */
            add_editor_style( 'editor-style.css' );

            /*
             * WooCommerce.
             */
            $hook_result = apply_filters_deprecated( 'elementor_hello_theme_add_woocommerce_support', [ true ], '2.0', 'hello_elementor_add_woocommerce_support' );
            if ( apply_filters( 'hello_elementor_add_woocommerce_support', $hook_result ) ) {
                // WooCommerce in general.
                add_theme_support( 'woocommerce' );
                // Enabling WooCommerce product gallery features (are off by default since WC 3.0.0).
                // zoom.
                add_theme_support( 'wc-product-gallery-zoom' );
                // lightbox.
                add_theme_support( 'wc-product-gallery-lightbox' );
                // swipe.
                add_theme_support( 'wc-product-gallery-slider' );
            }
        }
    }
}
add_action( 'after_setup_theme', 'hello_elementor_setup' );

if ( ! function_exists( 'hello_elementor_scripts_styles' ) ) {
    /**
     * Theme Scripts & Styles.
     *
     * @return void
     */
    function hello_elementor_scripts_styles() {
        $enqueue_basic_style = apply_filters_deprecated( 'elementor_hello_theme_enqueue_style', [ true ], '2.0', 'hello_elementor_enqueue_style' );
        $min_suffix          = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

        if ( apply_filters( 'hello_elementor_enqueue_style', $enqueue_basic_style ) ) {
            wp_enqueue_style(
                'hello-elementor',
                get_template_directory_uri() . '/style' . $min_suffix . '.css',
                [],
                HELLO_ELEMENTOR_VERSION
            );
        }

        if ( apply_filters( 'hello_elementor_enqueue_theme_style', true ) ) {
            wp_enqueue_style(
                'hello-elementor-theme-style',
                get_template_directory_uri() . '/theme' . $min_suffix . '.css',
                [],
                HELLO_ELEMENTOR_VERSION
            );
        }
    }
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_scripts_styles' );

if ( ! function_exists( 'hello_elementor_register_elementor_locations' ) ) {
    /**
     * Register Elementor Locations.
     *
     * @param ElementorPro\Modules\ThemeBuilder\Classes\Locations_Manager $elementor_theme_manager theme manager.
     *
     * @return void
     */
    function hello_elementor_register_elementor_locations( $elementor_theme_manager ) {
        $hook_result = apply_filters_deprecated( 'elementor_hello_theme_register_elementor_locations', [ true ], '2.0', 'hello_elementor_register_elementor_locations' );
        if ( apply_filters( 'hello_elementor_register_elementor_locations', $hook_result ) ) {
            $elementor_theme_manager->register_all_core_location();
        }
    }
}
add_action( 'elementor/theme/register_locations', 'hello_elementor_register_elementor_locations' );

if ( ! function_exists( 'hello_elementor_content_width' ) ) {
    /**
     * Set default content width.
     *
     * @return void
     */
    function hello_elementor_content_width() {
        $GLOBALS['content_width'] = apply_filters( 'hello_elementor_content_width', 800 );
    }
}
add_action( 'after_setup_theme', 'hello_elementor_content_width', 0 );

if ( is_admin() ) {
    require get_template_directory() . '/includes/admin-functions.php';
}

if ( ! function_exists( 'hello_elementor_check_hide_title' ) ) {
    /**
     * Check hide title.
     *
     * @param bool $val default value.
     *
     * @return bool
     */
    function hello_elementor_check_hide_title( $val ) {
        if ( defined( 'ELEMENTOR_VERSION' ) ) {
            $current_doc = \Elementor\Plugin::instance()->documents->get( get_the_ID() );
            if ( $current_doc && 'yes' === $current_doc->get_settings( 'hide_title' ) ) {
                $val = false;
            }
        }
        return $val;
    }
}
add_filter( 'hello_elementor_page_title', 'hello_elementor_check_hide_title' );

/**
 * Wrapper function to deal with backwards compatibility.
 */
if ( ! function_exists( 'hello_elementor_body_open' ) ) {
    function hello_elementor_body_open() {
        if ( function_exists( 'wp_body_open' ) ) {
            wp_body_open();
        } else {
            do_action( 'wp_body_open' );
        }
    }
}

/**
 * CUSTOM START
 */
function wpdocs_theme_name_scripts() {
    if (is_product()) {
        wp_enqueue_style( 'post_347_css', home_url() . '/wp-content/uploads/elementor/css/post-347.css' );
        wp_enqueue_style( 'post_16_css', home_url() . '/wp-content/uploads/elementor/css/post-16.css' );

        wp_enqueue_style( 'date_picker_css', 'https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.9/datepicker.min.css' );
        wp_enqueue_script( 'date_picker_js', 'https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.9/datepicker.min.js');
//        wp_enqueue_script( 'date_picker_js', 'https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/datepicker.min.js');
//        wp_enqueue_script( 'date_picker_js_i', 'https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.9/i18n/datepicker.en-US.min.js');
        wp_enqueue_style( 'custom_css', home_url() . '/wp-content/themes/hello-elementor/assets/css/custom_product_single.css' );
    }
    if (is_shop() || is_product_category() || is_page('rental')) {
        wp_enqueue_style( 'post_177_css', home_url() . '/wp-content/uploads/elementor/css/post-177.css' );
        wp_enqueue_style( 'post_190_css', home_url() . '/wp-content/uploads/elementor/css/post-190.css' );
        wp_enqueue_style( 'custom_archive_css', home_url() . '/wp-content/themes/hello-elementor/assets/css/custom_archive.css' );
    }

    if (is_page('rental')) {
        wp_enqueue_style( 'post_110_css', home_url() . '/wp-content/uploads/elementor/css/post-110.css' );
        wp_enqueue_style( 'post_177_css', home_url() . '/wp-content/uploads/elementor/css/post-177.css' );
        wp_enqueue_style( 'custom_archive_css', home_url() . '/wp-content/themes/hello-elementor/assets/css/custom_archive.css' );
    }

    if (is_page('luxury')) {
        wp_enqueue_style( 'post_134_css', home_url() . '/wp-content/uploads/elementor/css/post-134.css' );
    }

    if (is_page(14061)) {
        wp_enqueue_style( 'post_16_css', home_url() . '/wp-content/uploads/elementor/css/post-16.css' );
    }


    if (is_page('stores-all')) {
        wp_enqueue_style( 'custom_stores_all_css', home_url() . '/wp-content/themes/hello-elementor/assets/css/custom_stores_all.css' );
    }
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );


add_filter( 'body_class','my_class_names' );
function my_class_names( $classes ) {
    if( is_product() )
        $classes[] = 'rtl page-template page-template-elementor_header_footer page page-id-347 logged-in admin-bar theme-hello-elementor woocommerce-js elementor-default elementor-template-full-width elementor-kit-10 elementor-page elementor-page-347 customize-support';

    if( is_shop() )
        $classes[] = 'rtl page-template page-template-elementor_header_footer page page-id-177 theme-hello-elementor woocommerce-js elementor-default elementor-template-full-width elementor-kit-10 elementor-page elementor-page-177';

    if(is_page(14061))
        $classes[] = 'a rtl home page-template page-template-elementor_header_footer page page-id-16 logged-in admin-bar theme-hello-elementor woocommerce-js elementor-default elementor-template-full-width elementor-kit-10 elementor-page elementor-page-16 customize-support';

    return $classes;
}

add_action('template_redirect', 'remove_shop_breadcrumbs' );
function remove_shop_breadcrumbs(){
    if (is_product())
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
}
/**
 * CUSTOM END
 */

add_action( 'pre_get_posts', function( $q )
{
    // Check the meta query:
    $mq = $q->get( 'meta_query' );

    if( empty( $mq ) )
        return;

    // Init:
    $marker = '___tmp_marker___';
    $rx     = [];

    // Collect all the sub meta queries, that use REGEXP, RLIKE or LIKE:
    foreach( $mq as $k => $m )
    {
        if(    isset( $m['_key_compare'] )
            && in_array( strtoupper( $m['_key_compare'] ), [ 'REGEXP', 'RLIKE', 'LIKE' ] )
            && isset( $m['key'] )
        ) {
            // Mark the key with a unique string to secure the later replacements:
            $m['key'] .= $marker . $k; // Make the appended tmp marker unique

            // Modify the corresponding original query variable:
            $q->query_vars['meta_query'][$k]['key'] = $m['key'];

            // Collect it:
            $rx[$k] = $m;
        }
    }

    // Nothing to do:
    if( empty( $rx ) )
        return;

    // Get access the generated SQL of the meta query:
    add_filter( 'get_meta_sql', function( $sql ) use ( $rx, $marker )
    {
        // Only run once:
        static $nr = 0;
        if( 0 != $nr++ )
            return $sql;

        // Modify WHERE part where we replace the temporary markers:
        foreach( $rx as $k => $r )
        {
            $sql['where'] = str_replace(
                sprintf(
                    ".meta_key = '%s' ",
                    $r['key']
                ),
                sprintf(
                    ".meta_key %s '%s' ",
                    $r['_key_compare'],
                    str_replace(
                        $marker . $k,
                        '',
                        $r['key']
                    )
                ),
                $sql['where']
            );
        }
        return $sql;
    });

});

/**
 * AJAX START
 */
function hm_get_template_part( $file, $template_args = array(), $cache_args = array() ) {
    $template_args = wp_parse_args( $template_args );
    $cache_args = wp_parse_args( $cache_args );
    if ( $cache_args ) {
        foreach ( $template_args as $key => $value ) {
            if ( is_scalar( $value ) || is_array( $value ) ) {
                $cache_args[$key] = $value;
            } else if ( is_object( $value ) && method_exists( $value, 'get_id' ) ) {
                $cache_args[$key] = call_user_method( 'get_id', $value );
            }
        }
        if ( ( $cache = wp_cache_get( $file, serialize( $cache_args ) ) ) !== false ) {
            if ( ! empty( $template_args['return'] ) )
                return $cache;
            echo $cache;
            return;
        }
    }
    $file_handle = $file;
    do_action( 'start_operation', 'hm_template_part::' . $file_handle );
    if ( file_exists( get_stylesheet_directory() . '/' . $file . '.php' ) )
        $file = get_stylesheet_directory() . '/' . $file . '.php';
    elseif ( file_exists( get_template_directory() . '/' . $file . '.php' ) )
        $file = get_template_directory() . '/' . $file . '.php';
    ob_start();
    $return = require( $file );
    $data = ob_get_clean();
    do_action( 'end_operation', 'hm_template_part::' . $file_handle );
    if ( $cache_args ) {
        wp_cache_set( $file, $data, serialize( $cache_args ), 3600 );
    }
    if ( ! empty( $template_args['return'] ) )
        if ( $return === false )
            return false;
        else
            return $data;
    echo $data;
}

add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data(){
    wp_localize_script( 'jquery', 'myajaxcustom',
        array(
            'url' => admin_url('admin-ajax.php')
        )
    );
}
add_action('wp_ajax_send_email', 'send_email_purchase');
add_action('wp_ajax_nopriv_send_email', 'send_email_purchase');
function send_email_purchase() {
    if (!isset($_POST['product_id']) || empty($_POST['product_id'])) {
        echo json_encode(['success'=> false]);
        die;
    }
    if (!isset($_POST['f_name']) || empty($_POST['f_name']))  {
        echo json_encode(['success'=> false]);
        die;
    }
    if (!isset($_POST['f_phone']) || empty($_POST['f_phone']))  {
        echo json_encode(['success'=> false]);
        die;
    }
    if (!isset($_POST['f_email']) || empty($_POST['f_email']))  {
        echo json_encode(['success'=> false]);
        die;
    }
    if (!get_field('m_g_mail_to_send', 'options')) {
        echo json_encode(['success'=> false]);
        die;
    }

    $mailer = WC()->mailer();
    $product = wc_get_product($_POST['product_id']);

    if (!isset($product)) echo json_encode(['success'=> false]);

    $recipient = get_field('m_g_mail_to_send', 'options');
    $subject = __("New order Pery", 'theme_name');
    $content = hm_get_template_part('woocommerce/email-custom.php', [
        'product' => $product,
        'name' => $_POST['f_name'],
        'phone' => $_POST['f_phone'],
        'email' => $_POST['f_email'],
        'f_age' => isset($_POST['f_age']) ? $_POST['f_age'] : '',
        'f_part' => isset($_POST['f_part']) ? $_POST['f_part'] : '',
        'f_d_to' => isset($_POST['f_d_to']) ? $_POST['f_d_to'] : '',
        'f_d_from' => isset($_POST['f_d_from']) ? $_POST['f_d_from'] : '',
        'f_drop' => isset($_POST['f_drop']) ? implode(',', $_POST['f_drop']) : '',
        'period' => isset($_POST['f_d_period']) ? $_POST['f_d_period'] : [],
        'extras' => isset($_POST['f_d_extras']) ? $_POST['f_d_extras'] : [],
        'return' => true,
    ]);
    $headers = "Content-Type: text/html\r\n";

    $result = $mailer->send( $recipient, $subject, $content, $headers );
    if ($result) {
        echo json_encode(['success'=> true]);
    } else {
        echo json_encode(['success'=> false]);
    }
    wp_die();
}


add_action('wp_ajax_send_email_from_home', 'send_email_send_email_from_home');
add_action('wp_ajax_nopriv_send_email_from_home', 'send_email_send_email_from_home');
function send_email_send_email_from_home() {
    if (!isset($_POST['ff_name']) || empty($_POST['ff_name'])) {
        echo json_encode(['success'=> false]);
        die;
    }
    if (!isset($_POST['ff_email']) || empty($_POST['ff_email']))  {
        echo json_encode(['success'=> false]);
        die;
    }
    if (!isset($_POST['ff_phone']) || empty($_POST['ff_phone']))  {
        echo json_encode(['success'=> false]);
        die;
    }

    $mailer = WC()->mailer();

    $recipient = get_field('m_g_mail_to_send', 'options');
    $subject = __("Pery", 'theme_name');
    $content = hm_get_template_part('woocommerce/email-custom-from-home.php', [
        'ff_phone' => $_POST['ff_phone'],
        'ff_email' => $_POST['ff_email'],
        'ff_name' => $_POST['ff_name'],
        'return' => true,
    ]);
    $headers = "Content-Type: text/html\r\n";

    $result = $mailer->send( $recipient, $subject, $content, $headers );
    if ($result) {
        echo json_encode(['success'=> true]);
    } else {
        echo json_encode(['success'=> false]);
    }
    wp_die();
}


add_action('wp_ajax_pagination_ajax', 'pagination_ajax');
add_action('wp_ajax_nopriv_pagination_ajax', 'pagination_ajax');
function pagination_ajax() {
    usleep(30000);
    wp_die();
}
/**
 * AJAX END
 */

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'post_id' => 'options',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

}