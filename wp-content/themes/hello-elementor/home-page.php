<?php

/**
 * Template Name: Home-Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

?>

<?php get_header(); ?>
    <style>
        @media (min-width: 767px) {
            .elementor-16 .elementor-element.elementor-element-89d0b49 > .elementor-element-populated,
            .elementor-16 .elementor-element.elementor-element-79e78d4 > .elementor-element-populated,
            .elementor-16 .elementor-element.elementor-element-62fd950 > .elementor-element-populated,
            .elementor-190 .elementor-element.elementor-element-01d1298 > .elementor-element-populated{
                border: none !important;
                position: relative;
            }
            .elementor-16 .elementor-element.elementor-element-89d0b49 > .elementor-element-populated:before,
            .elementor-16 .elementor-element.elementor-element-79e78d4 > .elementor-element-populated:before,
            .elementor-16 .elementor-element.elementor-element-62fd950 > .elementor-element-populated:before{
                content: '';
                position: absolute;
                top: 25%;
                right: 0;
                height: 50%;
                width: 1px;
                background-color: #000;
            }
            .elementor-190 .elementor-element.elementor-element-01d1298 > .elementor-element-populated:before{
                content: '';
                position: absolute;
                top: 25%;
                left: 0;
                height: 50%;
                width: 1px;
                background-color: #000;
            }
        }
        .elementor-element-2a501a4,
        .elementor-element-62fd950,
        .elementor-element-79e78d4,
        .elementor-element-89d0b49{
            cursor: pointer;
        }
        .elementor-element-2a501a4:hover .elementor-element-populated,
        .elementor-element-62fd950:hover .elementor-element-populated,
        .elementor-element-79e78d4:hover .elementor-element-populated,
        .elementor-element-89d0b49:hover .elementor-element-populated{
            cursor: pointer;
            background-color: #191919 !important;
        }

        .elementor-16 .elementor-element.elementor-element-e842f4d > .elementor-column-wrap > .elementor-widget-wrap > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
            margin-bottom: -30px !important;
        }
    </style>
    <script>
        jQuery(document).ready(function () {
            jQuery('#new-url-form').off().on('submit', function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();
                if (jQuery(this).find('select').val().length > 0) {
                    window.location.replace(jQuery(this).find('select').val());
                }
            })

            jQuery('[data-url-custom]').on('click', function(e){
                e.preventDefault();console.dir('new');
                var url = jQuery(this).attr('href');
                location.href = url;
            });

            jQuery('#send-email-form').off().on('submit', function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                var data = {
                    action: 'send_email_from_home',
                    ff_name: jQuery('input[name="ff_name"]').val(),
                    ff_email: jQuery('input[name="ff_email"]').val(),
                    ff_phone: jQuery('input[name="ff_phone"]').val(),
                };
                jQuery.post( myajaxcustom.url, data, function(response) {
                    response = JSON.parse(response);
                    if (response.success) {
                        jQuery('#send-email-form').slideUp();
                        jQuery('#send-email-form-wrap').append('<div style="justify-content: center;\n' +
                            '    display: flex;"><div style="" class="elementor-column-wrap  elementor-element-populated">\n' +
                            '                            <div class="elementor-widget-wrap">\n' +
                            '                              <div class="elementor-element elementor-element-df2a39d elementor-widget elementor-widget-heading" data-id="df2a39d" data-element_type="widget" data-widget_type="heading.default">\n' +
                            '                                <div class="elementor-widget-container c-notif">\n' +
                            '                                  <h2 class="elementor-heading-title elementor-size-default c-notif-t">תודה רבה, נציגנו יחזור אליכם בהקדם</h2>\n' +
                            '                                </div>\n' +
                            '                              </div>\n' +
                            '                            </div>\n' +
                            '                          </div></div>');
                    } else {
                        jQuery('#send-email-form').slideUp();
                        jQuery('#send-email-form-wrap').append('<div style="justify-content: center;\n' +
                            '    display: flex;"><div style="max-width: 95%;" ss="elementor-column-wrap  elementor-element-populated">\n' +
                            '                            <div class="elementor-widget-wrap">\n' +
                            '                              <div class="elementor-element elementor-element-df2a39d elementor-widget elementor-widget-heading" data-id="df2a39d" data-element_type="widget" data-widget_type="heading.default">\n' +
                            '                                <div style="background-color: #ffdbd7;" class="elementor-widget-container c-notif">\n' +
                            '                                  <h2 class="elementor-heading-title elementor-size-default c-notif-t">Error</h2>\n' +
                            '                                </div>\n' +
                            '                              </div>\n' +
                            '                            </div>\n' +
                            '                          </div></div>');
                    }
                });

            })


        })

        function nextSlider()
        {
            swiperss[0].slideNext();
        }
        setInterval(function() {
            nextSlider();
        }, 4000);
    </script>

    <div data-elementor-type="wp-page" data-elementor-id="16" class="elementor elementor-16" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-element elementor-element-77a00e4 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="77a00e4" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                            <div class="elementor-element elementor-element-325461e elementor-column elementor-col-100 elementor-top-column" data-id="325461e" data-element_type="column">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-e6ac44a elementor--h-position-right elementor--v-position-middle elementor-pagination-position-inside elementor-widget elementor-widget-slides" data-id="e6ac44a" data-element_type="widget" data-settings="{&quot;navigation&quot;:&quot;dots&quot;,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}" data-widget_type="slides.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-swiper cust-margin-slider">
                                                    <div class="elementor-slides-wrapper elementor-main-swiper swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-rtl" dir="rtl" data-animation="fadeInUp" style="cursor: grab;">
                                                        <div class="swiper-wrapper elementor-slides" style="transition-duration: 100ms; transform: translate3d(2336px, 0px, 0px);">

                                                            <?php while (have_rows('slider_r')) : the_row(); ?>

                                                                <div class="elementor-repeater-item-67c497b swiper-slide" data-swiper-slide-index="<?php echo get_row_index() ?>" style="width: 1168px;">
                                                                    <div class="swiper-slide-bg" style="background-image: url(<?php echo get_sub_field('image')['url']; ?>)"></div>
                                                                    <div class="elementor-background-overlay"></div>
                                                                    <div class="swiper-slide-inner"><div class="swiper-slide-contents"><div class="elementor-slide-heading"><?php echo get_sub_field('test'); ?></div>
                                                                            <?php
                                                                            $link = get_sub_field('link');
                                                                            if( $link ):
                                                                            $link_url = $link['url'];
                                                                            $link_title = $link['title'];
                                                                            $link_target = $link['target'] ? $link['target'] : '_self';
                                                                            ?>
                                                                            <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" class="elementor-button elementor-slide-button elementor-size-sm"><?php echo esc_html( $link_title ); ?></a>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            <?php endwhile; ?>

                                                        </div>
                                                        <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets">
                                                            <span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span>
                                                            <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span></div>
                                                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-element elementor-element-37b3ef1 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="37b3ef1" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-element elementor-element-a78e8c8 elementor-column elementor-col-100 elementor-top-column" data-id="a78e8c8" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-element elementor-element-2a9be10 elementor-section-height-min-height art-h-t elementor-section-boxed elementor-section-height-default elementor-section elementor-inner-section" data-id="2a9be10" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div data-url-custom href="rental/" class="elementor-element elementor-element-2a501a4 elementor-column elementor-col-25 elementor-inner-column" data-id="2a501a4" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-d96a5af elementor-view-default elementor-widget elementor-widget-icon" data-id="d96a5af" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><g fill="none" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"><path d="M13.44 14.496L8.726 9.782c-.959-.944-1.103-2.439-.343-3.549l3.291-4.8C12.251.628 13.147.111 14.133.016c.985-.095 1.964.24 2.684.92l5.417 5.417c.68.72 1.016 1.7.92 2.685-.095.986-.611 1.882-1.417 2.458l-4.8 3.292c-1.087.745-2.548.623-3.497-.292zM14.863 3.885L19.337 8.359M12.926 13.999L11.143 15.782 10.766 17.685 8.949 17.976 8.691 19.759 7.08 19.845 3.651 23.273.206 23.068 0 19.622 2.126 17.496 2.983 16.639 3.874 15.748 9.274 10.348" transform="translate(12 12)"></path></g></svg>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-154b63c elementor-widget elementor-widget-heading" data-id="154b63c" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default"><a href="/rental/">השכרת רכב</a></h2>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-url-custom href="shop/" class="elementor-element elementor-element-62fd950 elementor-column elementor-col-25 elementor-inner-column" data-id="62fd950" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-eb662c2 elementor-view-default elementor-widget elementor-widget-icon" data-id="eb662c2" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><g fill="none" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" transform="translate(8 16)"><circle cx="25.293" cy="12.093" r="2.973"></circle><circle cx="6.68" cy="12.093" r="2.973"></circle><path d="M30.507 12.093h.746c.413 0 .747-.334.747-.746V6.133c-.007-.407-.34-.733-.747-.733h0C29.761 2.676 26.92.965 23.813.92h-6.666c-2.863.273-5.52 1.604-7.454 3.733 0 0-6.666.747-8.186 2.227C.514 8.14-.018 9.702 0 11.307c-.011.205.063.405.204.554.141.148.338.233.543.232h2.96M11.893 12.093L22.32 12.093M29.027 5.4L11.893 5.4M19.347 5.4L19.347 1.667M27.533 5.4L27.533 2.413M17.853 7.627L17.107 7.627"></path></g></svg>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-824b981 elementor-widget elementor-widget-heading" data-id="824b981" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default"><a href="/shop/">רכישת רכב</a></h2>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-url-custom href="lease-catalog/" class="elementor-element elementor-element-79e78d4 elementor-column elementor-col-25 elementor-inner-column" data-id="79e78d4" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-0868629 elementor-view-default elementor-widget elementor-widget-icon" data-id="0868629" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><g fill="none" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"><path d="M.347 3.111H22.666V18.389H.347zM7.292.333H15.722999999999999V3.111H7.292z" transform="translate(13 15)"></path><path d="M15.667 11.444H18.445V14.222000000000001H15.667zM4.556 11.444H7.334V14.222000000000001H4.556zM.347 11.444L22.653 11.444" transform="translate(13 15)"></path></g></svg>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-1414b0e elementor-widget elementor-widget-heading" data-id="1414b0e" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default"><a href="/lease-catalog/">סוגי ליסינג</a></h2>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-url-custom href="luxury/" class="elementor-element elementor-element-89d0b49 elementor-column elementor-col-25 elementor-inner-column" data-id="89d0b49" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                        <div class="elementor-widget-wrap">
                                                            <div class="elementor-element elementor-element-5c99376 elementor-view-default elementor-widget elementor-widget-icon" data-id="5c99376" data-element_type="widget" data-widget_type="icon.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-icon-wrapper">
                                                                        <div class="elementor-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><g fill="none" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" transform="translate(7 17)"><circle cx="25.382" cy="10.038" r="2.982"></circle><circle cx="5.222" cy="10.038" r="2.982"></circle><path d="M29.862 11.536l2.996-.742L33.6 5.6l-.742-.742v-1.54H29.12S23.898.336 19.418.336h-2.24c-2.766.079-5.415 1.135-7.476 2.982 0 0-8.204.756-9.702 2.282v5.236l.742.742H2.24M22.4 11.536L9.702 11.536"></path><path d="M11.942 4.074L23.142 4.074 25.382 3.318M20.16 7.056L18.662 9.296 10.458 9.296"></path></g></svg>			</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-7b11755 elementor-widget elementor-widget-heading" data-id="7b11755" data-element_type="widget" data-widget_type="heading.default">
                                                                <div class="elementor-widget-container">
                                                                    <h2 class="elementor-heading-title elementor-size-default"><a href="/luxury/">רכבי יוקרה</a></h2>		</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php if (have_rows('cir')) : ?>
                    <section class="elementor-element elementor-element-02e1c85 elementor-section-height-min-height elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="02e1c85" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-container elementor-column-gap-no">
                            <div class="elementor-row">
                                <?php
                                $products = get_field('cir');
                                ?>
                                <?php foreach ($products as $product) : ?>
                                    <?php
                                    $product = new WC_Product($product->ID);
                                    ?>
                                    <div class="elementor-element elementor-element-e842f4d elementor-column elementor-col-25 elementor-top-column" data-id="e842f4d" data-element_type="column">
                                    <div class="elementor-column-wrap  elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-5e6c26e elementor-widget elementor-widget-image" data-id="5e6c26e" data-element_type="widget" data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="600" height="300" src="<?php echo get_the_post_thumbnail_url( $product->get_id() ); ?>" class="attachment-large size-large" sizes="(max-width: 600px) 100vw, 600px" style="width: 100% !important; margin-bottom: 10px !important;">											</div>
                                                </div>
                                            </div>
                                            <section class="elementor-element elementor-element-b740280 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="b740280" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-element elementor-element-bd6024a elementor-column elementor-col-100 elementor-inner-column" data-id="bd6024a" data-element_type="column">
                                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-ce0e978 elementor-widget elementor-widget-heading" data-id="ce0e978" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_title(); ?></h2>		</div>
                                                                    </div>
                                                                    <?php if ($product->get_attribute('Manufacture Year')) : ?>
                                                                        <div class="elementor-element elementor-element-abc1f1b elementor-widget elementor-widget-heading" data-id="abc1f1b" data-element_type="widget" data-widget_type="heading.default">
                                                                            <div class="elementor-widget-container">
                                                                                <h2 class="elementor-heading-title elementor-size-default">מודל <?php echo $product->get_attribute('Manufacture Year') ?></h2>		</div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                    <div class="elementor-element elementor-element-0bc332d elementor-align-justify elementor-widget elementor-widget-button" data-id="0bc332d" data-element_type="widget" data-widget_type="button.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-button-wrapper">
                                                                                <a href="<?php echo get_post_permalink($product->get_id()) ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                                                                    <span class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">לפרטים נוספים</span>
                                                                                    </span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </section>
                <?php endif; ?>
                <section class="elementor-element elementor-element-321bbf2 elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="321bbf2" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-background-overlay"></div>
                    <div class="elementor-container elementor-column-gap-default" style="margin-top: auto">
                        <div class="elementor-row">
                            <div class="elementor-element elementor-element-d727051 elementor-column elementor-col-100 elementor-top-column" data-id="d727051" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-1868815 elementor--h-position-center elementor--v-position-middle elementor-arrows-position-inside elementor-pagination-position-inside elementor-widget elementor-widget-slides" data-id="1868815" data-element_type="widget" id="test" data-settings="{&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}" data-widget_type="slides.default">
                                            <div class="elementor-widget-container">
                                                <?php if (wp_is_mobile()) : ?>
                                                <div class="elementor-swiper">
                                                    <div class="elementor-slides-wrapper elementor-main-swiper swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-rtl" dir="rtl" data-animation="fadeInUp" style="cursor: grab;">
                                                        <div class="swiper-wrapper elementor-slides" style="transition-duration: 500ms;">
                                                            <?php foreach ($products as $product) : ?>
                                                                <?php
                                                                $product = new WC_Product($product->ID);
                                                                ?>

                                                            <div class="elementor-repeater-item-b61d9f8 swiper-slide pos-relative" data-swiper-slide-index="<?php echo get_row_index(); ?>">
                                                                    <div class="swiper-slide-bg" style="background-image: none">
                                                                        <img src="<?php echo get_the_post_thumbnail_url( $product->get_id() ); ?>" alt="img" style="max-height: 195px; height: 100%; margin: 0 auto; display: block;position: relative;top: 20px;">
                                                                    </div>
                                                                    <div class="swiper-slide-wrap"></div>
                                                                    <div class="swiper-slide-inner" style="display: flex;flex-direction: column;position: absolute;top: 63%;margin: 0 20px">
                                                                        <div class="swiper-slide-contents" style="display: none;">

                                                                        </div>
                                                                        <div class="elementor-element elementor-element-852a939 elementor-widget elementor-widget-heading" data-id="852a939" data-element_type="widget" data-widget_type="heading.default">
                                                                            <div class="elementor-widget-container">
                                                                                <h2 class="elementor-heading-title elementor-size-default"><?php echo $product->get_title(); ?></h2>		</div>
                                                                        </div>
                                                                        <?php if ($product->get_attribute('Manufacture Year')) : ?>
                                                                            <div class="elementor-element elementor-element-d68fe72 elementor-widget elementor-widget-heading" data-id="d68fe72" data-element_type="widget" data-widget_type="heading.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <h2 class="elementor-heading-title elementor-size-default">מודל <?php echo $product->get_attribute('Manufacture Year') ?></h2>		</div>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                        <div class="elementor-element elementor-element-a6a64d0 elementor-align-justify elementor-widget elementor-widget-button" data-id="a6a64d0" data-element_type="widget" data-widget_type="button.default" style="width: 100%">
                                                                            <div class="elementor-widget-container" style="width: 100%">
                                                                                <div class="elementor-button-wrapper" style="width: 100%">
                                                                                    <a href="<?php echo get_post_permalink($product->get_id()) ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button" style="width: 100%">
						                                                            <span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">לפרטים נוספים</span>
		</span>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                </div>
                                            </div>
                                                <?php endif; ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-element elementor-element-5fb09d7 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="5fb09d7" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-element elementor-element-ac70b2a elementor-column elementor-col-50 elementor-top-column" data-id="ac70b2a" data-element_type="column">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-3269da0 elementor-widget elementor-widget-heading" data-id="3269da0" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">מבצעי החודש</h2>		</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-d474608 elementor-column elementor-col-50 elementor-top-column" data-id="d474608" data-element_type="column">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-e9bb89d elementor-widget elementor-widget-heading" data-id="e9bb89d" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">רכבי 0 ק״מ ישירות מהיבואן עכשיו במבצע</h2>		</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-element elementor-element-6c94624 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="6c94624" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-element elementor-element-bbc9fd2 elementor-column elementor-col-50 elementor-top-column" data-id="bbc9fd2" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-element elementor-element-446f84b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="446f84b" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-element elementor-element-7ca91d3 elementor-column elementor-col-50 elementor-inner-column" data-id="7ca91d3" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-86ba0c7 elementor-view-default elementor-widget elementor-widget-icon" data-id="86ba0c7" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon" style="vertical-align: middle;">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="86" height="86" viewBox="0 0 86 86"><g fill="none" fill-rule="evenodd" stroke="#FFF" stroke-width="3.2"><path stroke-linecap="round" stroke-linejoin="round" d="M51.006 64.173L62.561 52.62c1.62-1.623 4.228-1.69 5.929-.152h0c1.538 1.7 1.471 4.31-.152 5.93l-17.301 17.3H27.929l-2.889 2.889-11.524-11.524 11.555-11.554h11.554l3.04 3.04h7.176c1.146-.004 2.247.449 3.058 1.26.81.81 1.264 1.911 1.26 3.058h0c.004 1.146-.45 2.247-1.26 3.058-.81.81-1.912 1.264-3.058 1.26l-10.125-.03"></path><g transform="translate(30.1 7.525)"><path d="M0 12.9c0 2.85 5.776 5.16 12.9 5.16 7.124 0 12.9-2.31 12.9-5.16M0 20.64c0 2.85 5.776 5.16 12.9 5.16 7.124 0 12.9-2.31 12.9-5.16M0 28.38c0 2.85 5.776 5.16 12.9 5.16 7.124 0 12.9-2.31 12.9-5.16"></path><path d="M0 5.16v30.96c0 2.85 5.776 5.16 12.9 5.16 7.124 0 12.9-2.31 12.9-5.16V5.16"></path><ellipse cx="12.9" cy="5.16" rx="12.9" ry="5.16"></ellipse></g></g></svg>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-579d235 elementor-column elementor-col-50 elementor-inner-column" data-id="579d235" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-d903f58 elementor-widget elementor-widget-heading" data-id="d903f58" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default">פתרונות מימון</h2>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-49f085f elementor-widget elementor-widget-heading" data-id="49f085f" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default">לפרטים נוספים &gt; </h2>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-6a6872a elementor-column elementor-col-50 elementor-top-column" data-id="6a6872a" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-element elementor-element-5da908f elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="5da908f" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-element elementor-element-66fcec2 elementor-column elementor-col-33 elementor-inner-column" data-id="66fcec2" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-9b2b565 elementor-view-default elementor-widget elementor-widget-icon" data-id="9b2b565" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon" style="vertical-align: middle">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="86" height="86" viewBox="0 0 86 86"><path fill="#FFF" d="M69.685 11.442c-.609 0-1.189.122-1.718.342-.438-2.015-2.239-3.528-4.388-3.528-.635 0-1.24.132-1.788.371-.536-1.875-2.268-3.252-4.318-3.252s-3.781 1.377-4.317 3.252c-.549-.239-1.153-.371-1.788-.371-2.476 0-4.49 2.008-4.49 4.477V23.95c-1.023-1.081-2.553-1.64-4.111-1.367-1.216.214-2.27.891-2.968 1.907-.075.109-.144.22-.208.335-.607-.327-1.3-.512-2.037-.512-.548 0-1.072.105-1.556.291-.456-1.884-2.16-3.289-4.188-3.289-.575 0-1.125.114-1.627.32-.543-1.753-2.184-3.03-4.117-3.03-1.934 0-3.574 1.277-4.117 3.03-.503-.206-1.052-.32-1.627-.32-2.377 0-4.31 1.928-4.31 4.297v10.348c-.977-.939-2.374-1.412-3.797-1.162-1.167.205-2.178.855-2.848 1.829-.67.974-.912 2.15-.683 3.308l1.507 7.62c.6 3.031 2.076 5.788 4.27 7.97l2.69 2.674v4.568h-1.515c-.791 0-1.432.639-1.432 1.427v12.852c0 .789.64 1.428 1.432 1.428.79 0 1.431-.64 1.431-1.428V65.623h18.689v11.424c0 .789.64 1.428 1.431 1.428s1.432-.64 1.432-1.428V64.195c0-.788-.64-1.427-1.432-1.427H35.71V58.2l1.59-1.58c2.944-2.927 4.564-6.822 4.564-10.967v-5.978c.811 1.77 1.935 3.391 3.34 4.788l2.886 2.869v5.587h-1.702c-.79 0-1.431.639-1.431 1.427v22.701c0 .789.64 1.428 1.431 1.428.79 0 1.432-.64 1.432-1.428V55.774h20.044v21.273c0 .789.641 1.428 1.432 1.428.79 0 1.432-.64 1.432-1.428v-22.7c0-.79-.642-1.428-1.432-1.428h-1.662v-5.587l1.717-1.706c3.11-3.094 4.825-7.212 4.825-11.593V32.55c0-.789-.641-1.428-1.432-1.428-.79 0-1.432.64-1.432 1.428v1.483c0 3.617-1.414 7.016-3.983 9.57L65.19 45.73c-.27.267-.421.631-.421 1.01v6.18H50.952v-6.18c0-.38-.152-.743-.421-1.01l-3.306-3.288c-1.915-1.905-3.204-4.31-3.727-6.956l-1.602-8.098c-.09-.45.005-.904.264-1.282.26-.377.652-.63 1.104-.708.876-.155 1.727.4 1.939 1.26l.828 3.366c.23.937 1.093 1.533 2.053 1.417.96-.115 1.657-.9 1.657-1.864V12.733c0-.895.73-1.622 1.627-1.622.835 0 1.525.631 1.615 1.441v9.386c0 .057.004.112.01.168v.917c0 .788.642 1.427 1.433 1.427.79 0 1.431-.639 1.431-1.427v-10.29c0-.099-.004-.197-.01-.295V9.852c0-.894.73-1.622 1.626-1.622.897 0 1.626.728 1.626 1.622v2.586c-.006.098-.01.196-.01.295v9.205c0 .057.004.112.01.168v.917c0 .788.642 1.427 1.432 1.427.791 0 1.432-.639 1.432-1.427v-10.47c.09-.81.78-1.442 1.616-1.442.897 0 1.626.727 1.626 1.622v2.892c-.006.098-.01.195-.01.295v6.018c0 .057.004.112.01.168v.917c0 .788.641 1.427 1.432 1.427.79 0 1.431-.639 1.431-1.427v-7.285c.091-.809.781-1.44 1.616-1.44.897 0 1.627.727 1.627 1.622l-.163 21.499c0 .788.219-5.007 1.01-5.007.79 0 2.017 1.79 2.017 1.001V15.92c0-2.47-2.014-4.478-4.49-4.478zm-30.82 34c0 3.36-1.307 6.517-3.682 8.89l-1.989 1.987c-.267.266-.417.628-.417 1.005v5.13H20.085v-5.13c0-.377-.15-.739-.417-1.005l-3.076-3.074c-1.771-1.77-2.963-4.004-3.446-6.462l-1.49-7.573c-.079-.397.004-.8.233-1.135.228-.334.573-.557.971-.627.773-.136 1.523.355 1.71 1.117l.77 3.147c.224.918 1.065 1.501 2.003 1.388.936-.113 1.615-.88 1.615-1.826V25.523c0-.79.642-1.433 1.43-1.433.733 0 1.339.556 1.42 1.27v8.77c0 .056.004.11.01.163v.853c0 .784.634 1.419 1.416 1.419.782 0 1.416-.635 1.416-1.42v-9.622c0-.093-.004-.185-.01-.277v-2.417c0-.79.642-1.432 1.43-1.432.79 0 1.43.642 1.43 1.432v2.415c-.005.093-.01.185-.01.279v8.608c0 .055.005.11.01.163v.852c0 .783.635 1.419 1.417 1.419s1.416-.636 1.416-1.42V25.36c.082-.713.687-1.27 1.42-1.27.789 0 1.43.644 1.43 1.434v2.701c-.006.092-.01.185-.01.279v5.628c0 .055.004.11.01.163v.852c0 .783.635 1.419 1.416 1.419.783 0 1.417-.636 1.417-1.42V28.34c.082-.713.687-1.269 1.42-1.269.788 0 1.43.643 1.43 1.433v16.938z"></path></svg>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-2ae97aa elementor-hidden-phone elementor-hidden-tablet elementor-column elementor-col-33 elementor-inner-column" data-id="2ae97aa" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-e1f089f elementor-widget elementor-widget-heading" data-id="e1f089f" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default">אני צריך</h2>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-d45193d elementor-column elementor-col-33 elementor-inner-column" data-id="d45193d" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-7f2a92b elementor-button-align-stretch elementor-widget elementor-widget-form" data-id="7f2a92b" data-element_type="widget" data-widget_type="form.default">
                                                                    <div class="elementor-widget-container">
                                                                        <form class="elementor-form" method="post" id="new-url-form" name="אני צריך...">

                                                                            <div class="elementor-form-fields-wrapper elementor-labels-">
                                                                                <div class="elementor-field-type-select elementor-field-group elementor-column elementor-field-group-name elementor-col-100">
                                                                                    <label for="form-field-name" class="elementor-field-label elementor-screen-only">אני צריך</label>		<div class="elementor-field elementor-select-wrapper ">
                                                                                        <select name="form_fields[name]" id="form-field-name-new-url" class="elementor-field-textual elementor-size-sm">
                                                                                            <option value="">קניית רכב</option>
                                                                                            <option value="http://perydev.wbd.co.il/rental/">RENTAL</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
<!--                                                                                <div class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-100">-->
<!--                                                                                    <button type="submit" id="new-url-btn" class="elementor-button elementor-size-sm">-->
<!--                                                                                            <span>-->
<!--															                                    <span class=" elementor-button-icon">-->
<!--																										</span>-->
<!--																						<span class="elementor-button-text">שליחה</span>-->
<!--													                                                                            </span>-->
<!--                                                                                    </button>-->
<!--                                                                                </div>-->
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-7642b53 elementor-icon-list--layout-inline elementor-align-center elementor-widget elementor-widget-icon-list" data-id="7642b53" data-element_type="widget" data-widget_type="icon-list.default">
                                                                    <div class="elementor-widget-container">
                                                                        <ul class="elementor-icon-list-items elementor-inline-items">
                                                                            <li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#FFF" d="M23.4 18.917l-3.697-3.697c-.736-.733-1.955-.711-2.717.051l-1.863 1.862-.367-.204c-1.176-.652-2.786-1.545-4.48-3.24C8.577 11.99 7.683 10.378 7.03 9.2c-.07-.125-.135-.245-.2-.359l1.25-1.248.614-.615c.763-.764.784-1.983.05-2.718L5.046.563C4.312-.17 3.092-.149 2.329.615L1.287 1.662l.029.029c-.35.445-.641.96-.859 1.514-.2.528-.324 1.031-.381 1.536-.489 4.047 1.36 7.745 6.379 12.763 6.937 6.937 12.527 6.413 12.768 6.387.526-.063 1.029-.188 1.54-.387.55-.215 1.064-.506 1.51-.855l.022.02 1.056-1.033c.761-.763.783-1.982.049-2.72z"></path></svg>						</span>
                                                                                <span class="elementor-icon-list-text"></span>
                                                                            </li>
                                                                            <li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><g fill="#FFF" fill-rule="evenodd"><path d="M.825 2.704l10.931 9.333c.188.16.465.16.652-.003l10.765-9.318c.209-.18.524-.158.705.05.079.092.122.208.122.328V15.5c0 .276-.224.5-.5.5H.5c-.276 0-.5-.224-.5-.5V3.084c0-.276.224-.5.5-.5.119 0 .234.043.325.12z" transform="translate(0 4)"></path><path d="M22.953.882l-10.504 8.85c-.184.155-.453.156-.64.003L1.075.885C.86.71.83.396 1.007.183 1.102.067 1.243 0 1.393 0H22.63c.276 0 .5.224.5.5 0 .147-.066.287-.178.382z" transform="translate(0 4)"></path></g></svg>						</span>
                                                                                <span class="elementor-icon-list-text"></span>
                                                                            </li>
                                                                            <li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#FFF" fill-rule="evenodd" d="M20.444 3.535c-2.22-2.234-5.24-3.487-8.39-3.478C5.517.057.197 5.377.195 11.917c-.004 2.081.543 4.127 1.583 5.93L.094 23.992l6.288-1.65c1.738.948 3.687 1.445 5.667 1.445h.006c6.536 0 11.858-5.321 11.86-11.86.01-3.15-1.24-6.171-3.47-8.393zm-8.39 18.247h-.003c-1.766 0-3.5-.473-5.019-1.372l-.36-.214-3.73.98.996-3.64-.235-.374c-.986-1.571-1.508-3.39-1.505-5.244 0-5.435 4.422-9.858 9.86-9.858 2.615.001 5.123 1.042 6.97 2.892 1.849 1.85 2.886 4.358 2.885 6.973-.003 5.436-4.425 9.857-9.858 9.857zm5.408-7.384c-.296-.148-1.753-.865-2.025-.967-.272-.101-.47-.148-.667.149-.197.297-.765.964-.938 1.162-.173.198-.346.222-.642.074-.297-.149-1.252-.461-2.384-1.471-.88-.786-1.479-1.756-1.65-2.053-.17-.297-.017-.455.131-.605.133-.133.296-.346.444-.52.148-.173.198-.296.297-.494.098-.198.05-.37-.025-.519-.074-.148-.667-1.607-.914-2.2-.24-.578-.484-.5-.666-.512-.171-.009-.371-.01-.57-.01-.303.008-.59.142-.79.37-.27.297-1.036 1.014-1.036 2.473 0 1.458 1.061 2.868 1.21 3.066.147.198 2.09 3.19 5.06 4.475.553.238 1.117.447 1.69.626.71.227 1.356.193 1.867.117.569-.085 1.753-.717 2-1.409.248-.692.248-1.286.172-1.41-.077-.123-.268-.191-.564-.34v-.002z"></path></svg>						</span>
                                                                                <span class="elementor-icon-list-text"></span>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-element elementor-element-3241ec2 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="3241ec2" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                            <div class="elementor-element elementor-element-015db8f elementor-column elementor-col-100 elementor-top-column" data-id="015db8f" data-element_type="column">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-c90d4df elementor-widget elementor-widget-heading" data-id="c90d4df" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">רוצים שנציג יחזור אליכם?</h2>		</div>
                                        </div>
                                        <div class="elementor-element elementor-element-229dd11 elementor-button-align-stretch elementor-widget elementor-widget-form" data-id="229dd11" data-element_type="widget" data-widget_type="form.default">
                                            <div class="elementor-widget-container">
                                                <div id="send-email-form-wrap"></div>
                                                <form class="elementor-form" id="send-email-form" method="post" name="טופס עמוד ראשי">

                                                    <div class="elementor-form-fields-wrapper elementor-labels-">
                                                        <div class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-name elementor-col-25">
                                                            <img src="<?php echo get_template_directory_uri() . '/assets/gvn/user.svg'; ?>" class="icon-input icon-input_full" alt="">
                                                            <label for="ff_name" class="elementor-field-label elementor-screen-only">שם מלא</label>
                                                            <input size="1" type="text" name="ff_name" id="ff_name" class="elementor-field elementor-size-sm  elementor-field-textual" required="required" placeholder="שם מלא">
                                                        </div>
                                                        <div class="elementor-field-type-email elementor-field-group elementor-column elementor-field-group-email elementor-col-25 elementor-field-required">
                                                            <img src="<?php echo get_template_directory_uri() . '/assets/gvn/phone.svg'; ?>" class="icon-input icon-input_full" alt="">
                                                            <label for="ff_email" class="elementor-field-label elementor-screen-only">דואר אלקטרוני</label>
                                                            <input size="1" type="email" name="ff_email" id="ff_email" class="elementor-field elementor-size-sm  elementor-field-textual" placeholder="דואר אלקטרוני" required="required" aria-required="true">
                                                        </div>
                                                        <div class="elementor-field-type-tel elementor-field-group elementor-column elementor-field-group-message elementor-col-25">
                                                            <img src="<?php echo get_template_directory_uri() . '/assets/gvn/mail.svg'; ?>" class="icon-input icon-input_full" alt="">
                                                            <label for="ff_phone" class="elementor-field-label elementor-screen-only">טלפון ליצירת קשר</label>
                                                            <input size="1" type="tel" name="ff_phone" id="ff_phone" class="elementor-field elementor-size-sm  elementor-field-textual" placeholder="טלפון ליצירת קשר" required="required" pattern="[0-9()#&amp;+*-=.]+" title="Only numbers and phone characters (#, -, *, etc) are accepted.">
                                                        </div>

                                                        <div class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-25">
                                                            <button type="submit" class="elementor-button elementor-size-sm"><span
															    <span class=" elementor-button-icon"></span><span class="elementor-button-text">צור קשר</span></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-element elementor-element-f3a02f8 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="f3a02f8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-element elementor-element-6cd79de elementor-column elementor-col-100 elementor-top-column" data-id="6cd79de" data-element_type="column">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-131db31 elementor-widget elementor-widget-heading" data-id="131db31" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">מי אנחנו?</h2>		</div>
                                        </div>
                                        <div class="elementor-element elementor-element-804e8d8 elementor-widget elementor-widget-text-editor" data-id="804e8d8" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix"><p>חברת "פרי" הינה מהחברות המובילות בתחום הרכב.<br>אנו מתמחים במכירה, ליסינג והשכרה. לחברה סניפים בפריסה ארצית המאפשרים נוחות מרבית ושירות אישי.</p></div>
                                            </div>
                                        </div>
                                        <div style="margin-bottom:-25px" class="elementor-element elementor-element-51db2a3 elementor-testimonial--layout-image_stacked elementor-hidden-desktop elementor-hidden-tablet elementor-testimonial--skin-default elementor-testimonial--align-center elementor-arrows-yes elementor-pagination-type-bullets elementor-widget elementor-widget-testimonial-carousel" data-id="51db2a3" data-element_type="widget" id="test2" data-settings="{&quot;slides_per_view_mobile&quot;:&quot;1&quot;,&quot;space_between_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:13,&quot;sizes&quot;:[]},&quot;show_arrows&quot;:&quot;yes&quot;,&quot;pagination&quot;:&quot;bullets&quot;,&quot;speed&quot;:500,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;loop&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;space_between&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;space_between_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]}}" data-widget_type="testimonial-carousel.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-swiper">
                                                    <div class="elementor-main-swiper swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-rtl" style="cursor: grab;">
                                                        <div class="swiper-wrapper" style="transition-duration: 500ms;">

                                                            <?php while (have_rows('people')) : the_row() ?>
                                                                <?php
                                                                $image = get_sub_field('image');
                                                                ?>
                                                                <div class="swiper-slide" data-swiper-slide-index="<?php echo get_row_index(); ?>">
                                                                    <div class="elementor-testimonial">
                                                                        <div class="elementor-testimonial__footer">
                                                                            <div class="elementor-testimonial__image">
                                                                                <img src="<?php echo esc_url($image['url']); ?>" alt="ישראל ישראלי">
                                                                            </div>
                                                                            <cite class="elementor-testimonial__cite"><span class="elementor-testimonial__name"><?php echo get_sub_field('title') ?></span>
                                                                                <span class="elementor-testimonial__title"><?php echo get_sub_field('description') ?></span></cite>			</div>
                                                                    </div>
                                                                </div>
                                                            <?php endwhile; ?>

                                                        </div>
                                                        <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"></div>
                                                        <div class="elementor-swiper-button elementor-swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide">
                                                            <i class="eicon-chevron-left" aria-hidden="true"></i>
                                                            <span class="elementor-screen-only">Previous</span>
                                                        </div>
                                                        <div class="elementor-swiper-button elementor-swiper-button-next" tabindex="0" role="button" aria-label="Next slide">
                                                            <i class="eicon-chevron-right" aria-hidden="true"></i>
                                                            <span class="elementor-screen-only">Next</span>
                                                        </div>
                                                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (have_rows('people')) : ?>
                                            <section class="elementor-element elementor-element-a0df0fe elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="a0df0fe" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <?php while (have_rows('people')) : the_row() ?>
                                                            <div class="elementor-element elementor-element-d6793be elementor-hidden-tablet elementor-column elementor-col-25 elementor-inner-column" data-id="d6793be" data-element_type="column">
                                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-334cca2 elementor-widget elementor-widget-image" data-id="334cca2" data-element_type="widget" data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <?php
                                                                                $image = get_sub_field('image');
                                                                                ?>
                                                                                <img width="328" height="300" src="<?php echo esc_url($image['url']); ?>" class="attachment-large size-large" alt="" sizes="(max-width: 328px) 100vw, 328px">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-bdca8a4 elementor-widget elementor-widget-heading" data-id="bdca8a4" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default"><?php echo get_sub_field('title') ?></h2>		</div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-1cdd953 elementor-widget elementor-widget-heading" data-id="1cdd953" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default"><?php echo get_sub_field('description') ?></h2>		</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php endwhile; ?>
                                                    </div>
                                                </div>
                                            </section>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

<?php get_footer(); ?>